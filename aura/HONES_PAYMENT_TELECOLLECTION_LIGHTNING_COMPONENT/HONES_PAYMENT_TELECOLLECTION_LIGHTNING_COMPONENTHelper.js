({
    getPicklistStatus : function(component) {
        var action = component.get("c.getPickListValuesStatus");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.callStatus", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getPicklistPaymentPromise : function(component) {
        var action = component.get("c.getPickListValuesPaymentPromise");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.PaymentPromise", conts);
                console.log("Valores PaymentPromise" + JSON.stringify(conts));
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getPicklistIncompleteStatus : function(component) {
        var action = component.get("c.getPickListValuesIncompleteStatus");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.incompleteStatusPicklistValues", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getPicklistClarificationRequest : function(component) {
        var action = component.get("c.getPickListValuesClarificationRequest");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.ClarificationRequest", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getPicklistClarificationType : function(component) {
        var action = component.get("c.getPickListValuesClarificationType");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.ClarificationType", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getPicklistPaymentDelayReason : function(component) {
        var action = component.get("c.getPickListValuesPaymentDelayReason");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.ReasonDelay", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getInsertValues : function(component,recordId) {
        var action = component.get("c.insertTelecobranza");
        var recordId = component.get("v.recordId");
        var idCuenta = component.get("v.idCuenta");
        var PayPromise = component.get("v.selectedValuePaymentPromise");
        var ClarificationRequest = component.get("v.selectedValueClarificationRequest");
        var ClarificationType = component.get("v.selectedValueClarificationType");
        var compromisedAmount = component.get("v.compromisedAmount");
        var todayValue = component.get("v.today");
        var ReasonDelay = component.get("v.selectedValueReasonDelay");
        var callStatus = component.get("v.selectedValueCallStatus");
        var otro = component.get("v.Otro");
        var incompleteStatus = component.get("v.selectedValueIncompleteStatusPicklistValues");
       
        action.setParams({recordId : recordId,
            			  idCuenta : idCuenta,
            			  PayPromise:PayPromise,
                          ClarificationRequest:ClarificationRequest,
                          ClarificationType:ClarificationType,
                          compromisedAmount:compromisedAmount,
                          todayValue:todayValue,
                          ReasonDelay:ReasonDelay,
                          callStatus: callStatus,
                          otro:otro,
                          incompleteStatus:incompleteStatus});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                location.reload();
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getIds : function(component,recordId) {
        var action = component.get("c.getById");
        action.setParams({recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.idCuenta", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getInfoCall : function(component,recordId) {
    var action = component.get("c.infoCall");
    action.setParams({recordId : recordId});
    action.setCallback(this, function(response) {
        var state = response.getState();
        
        if (state === "SUCCESS") {
            var conts = response.getReturnValue();
            component.set("v.infoCall", conts);
            
            if(conts.ONCALL__Call_Status__c == $A.get("$Label.c.COMPLETE") || conts.ONCALL__Call_Status__c == $A.get("$Label.c.Incompletepaymentjs")){
                component.set("v.infoStatus", false);
            }
            else{
                component.set("v.infoStatus", true);
            }
        }
        else if (state === "INCOMPLETE") {
            // do something
        }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
    });
    $A.enqueueAction(action);
    },

    getHoursOnPromiseDate: function(component, recordId){
        var action = component.get("c.getDeliveryDateHours");
        action.setParams({recordId : recordId});
        action.setCallback(this, function(response) {
        var state = response.getState();
        
        if (state === "SUCCESS") {
            var conts = response.getReturnValue();
            
            component.set("v.hoursToSum", conts);
            console.log('Variable horas: ' + conts);
            var today = new Date();
       		var hours = component.get("v.hoursToSum");
            var days = hours/24;
            console.log("dias: " + days);
            component.set('v.today', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + (today.getDate() + days));
            
            
        }
        else if (state === "INCOMPLETE") {
            // do something
        }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
    });
    $A.enqueueAction(action);
    },
})