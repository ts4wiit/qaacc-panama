({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        helper.getPicklistPaymentPromise(component);
        helper.getPicklistClarificationRequest(component,recordId);
        helper.getPicklistClarificationType(component);
        helper.getPicklistPaymentDelayReason(component);
        helper.getPicklistStatus(component);
        helper.getIds(component,recordId);
        helper.getInfoCall(component,recordId);
        helper.getPicklistIncompleteStatus(component,recordId);
        helper.getHoursOnPromiseDate(component, recordId);
        
    },
    
    clickCreateItem: function(component, event, helper) { 
        var recordId = component.get("v.recordId");
        var PayPromise = component.get("v.selectedValuePaymentPromise");
        var ClarificationRequest = component.get("v.selectedValueClarificationRequest");
        var ClarificationType = component.get("v.selectedValueClarificationType");
        var compromisedAmount = component.get("v.compromisedAmount");
        var todayValue = component.get("v.today");
        var ReasonDelay = component.get("v.selectedValueReasonDelay");
        var callStatus = component.get("v.selectedValueCallStatus");
        var otro = component.get("v.Otro");
        var otraOpcion = component.get("v.otraOpcion");
        var incompleteStatus = component.get("v.selectedValueIncompleteStatusPicklistValues");
        
        
        if(callStatus == $A.get("$Label.c.COMPLETE") && PayPromise != $A.get("$Label.c.YESPROMISE")){
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.STATUS_AND_PAYMENT_INCORRECT"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                
                
                return false;
            }
        
        if(callStatus == $A.get("$Label.c.Incompletepaymentjs") && incompleteStatus == "--"+$A.get("$Label.c.None")+"--"){
            
            var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.INCOMPLETE_CONDITION_STATUS") + ',\n'+ '\n' + $A.get("$Label.c.INCOMPLETE_CALL_REASON"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    return false;
        }
        //REVISAR CONDICIÓN - VALIDACIÓN
        if(callStatus == $A.get("$Label.c.Incompletepaymentjs") && incompleteStatus == $A.get("$Label.c.ClarificationRequest") && ClarificationRequest != $A.get("$Label.c.YESPROMISE")){
           if(ClarificationRequest != $A.get("$Label.c.YESPROMISE") || ClarificationType ==" "){
            var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.INCOMPLETE_CONDITION_STATUS") + ',\n'+ '\n' + $A.get("$Label.c.ClarificationRequest"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    return false;
        }
        }
        
        if(callStatus == $A.get("$Label.c.Incompletepaymentjs") && incompleteStatus != $A.get("$Label.c.ClarificationRequest") && ClarificationRequest == $A.get("$Label.c.YESPROMISE")){
           if(ClarificationRequest != $A.get("$Label.c.YESPROMISE") || ClarificationType ==" "){
            var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.CLARIFICATION_REQUEST_SELECT") + '\n'+ '\n' + $A.get("$Label.c.ClarificationRequest"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    return false;
        }
        }
           
        if(PayPromise == $A.get("$Label.c.YESPROMISE")){
            if(compromisedAmount == "" || compromisedAmount == null || todayValue =="" || todayValue == null || ReasonDelay == "--"+$A.get("$Label.c.None")+"--" || ReasonDelay ==" " || ReasonDelay == null){
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.PAYPROMISECONDITION") + '\n' + '\n' + $A.get("$Label.c.PAYMENT_PROMISE_DATE") + ',\n' + $A.get("$Label.c.COMMITTED_AMOUNT_TELE") + ',\n'+ $A.get("$Label.c.REASON_FOR_DELAY"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                
                
                return false;
            }
            
            if(ReasonDelay == $A.get("$Label.c.OTHERPAYBOOLEAN") && otro == null){
                 var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.NECESSARY_REASON"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                
                return false;
            }
        }
        if(ClarificationRequest == $A.get("$Label.c.YESPROMISE")){
            if(ClarificationType ==" "){
                
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.CLARIFICATION_REC_CONDITION") + ',\n'+ '\n' + $A.get("$Label.c.CLARIFICATION_TYPE"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                
                return false;
            }
            else{
                if(callStatus == $A.get("$Label.c.PENDING_TO_CALL_PAYMENT") || callStatus == " "){
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.PENDING_TO_CALL_CONDITION"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    return false;
                } 
                else{
                    helper.getInsertValues(component,recordId);
                }
            }
        }
        else{
            if(callStatus == $A.get("$Label.c.PENDING_TO_CALL_PAYMENT") || callStatus == " "){
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.PENDING_TO_CALL_CONDITION"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                return false;
            }
            else{
                if(ReasonDelay == "--"+$A.get("$Label.c.None")+"--" || ReasonDelay ==" " || ReasonDelay == null){
                } 
                else{
                    helper.getInsertValues(component,recordId);
                }
            }
        }
        if(PayPromise == $A.get("$Label.c.NOBOOLEAN") && ClarificationRequest == $A.get("$Label.c.NOBOOLEAN")){
            if(callStatus == $A.get("$Label.c.PENDING_TO_CALL_PAYMENT") || callStatus == " "){
            }
            else{
                helper.getInsertValues(component,recordId);
            }
        }
    },
    
    reasonValue: function(component, event, helper) { 
        
        var ReasonDelay = component.get("v.selectedValueReasonDelay");
        
        if(ReasonDelay == $A.get("$Label.c.OTHERPAYBOOLEAN")){
            component.set("v.otraOpcion", true);
        }
        else{
            component.set("v.otraOpcion", false);
        }
        
    },

    aclarationValue: function(component, event, helper) { 
        var aclaration = component.get("v.selectedValueClarificationRequest");
        if(aclaration == "No"){
            component.set("v.noAclaration", true);
            component.set("v.selectedValueClarificationType", "--"+$A.get("$Label.c.None")+"--");
        }else{
            component.set("v.noAclaration", false);
        }
    },
    
    statusValuePicklistEnable: function(component, event, helper) { 
        var callStatus = component.get("v.selectedValueCallStatus");
        if(callStatus == $A.get("$Label.c.COMPLETE")){
            component.set("v.incompleteStatusBool", false);
            component.set("v.incompleteStatusFields", false);
        }else if(callStatus == $A.get("$Label.c.Incompletepaymentjs")){
            component.set("v.incompleteStatusBool", false);
            component.set("v.incompleteStatusFields", true);
            component.set("v.selectedValuePaymentPromise", "No");
        }else{
            component.set("v.incompleteStatusBool", true);
            component.set("v.incompleteStatusFields", true);
            component.set("v.selectedValueIncompleteStatusPicklistValues",  "--"+$A.get("$Label.c.None")+"--");
            component.set("v.selectedValuePaymentPromise", "No");
            component.set("v.commentary", "");
        }
        var promise = component.get('c.resetPromise');
        $A.enqueueAction(promise);
    },
    
    resetPromise: function(component, event, helper){ 
        var today = new Date();
        var promise = component.get("v.selectedValuePaymentPromise");
        if(promise=="No"){
            component.set("v.selectedValueReasonDelay", "--"+$A.get("$Label.c.None")+"--");
            component.set('v.today', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
            component.set("v.compromisedAmount", "0.00");
            component.set("v.Otro", "");
            component.set("v.otraOpcion", false);
            component.set("v.promise", true);
        }else{
            component.set("v.promise", false);
        }
    },

    datevalue: function(component, event, helper) { 
        
        var todayValue = component.get("v.today");
        console.log("Valor de hoy: "+ todayValue);
        var selectedDate = new Date(todayValue);
        var today = new Date();
        
        console.log("Valor de hoy: "+ today);
        console.log("Valor seleccionado: "+ selectedDate);
        
        if(selectedDate < today){
             var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message: $A.get("$Label.c.PREVIOUS_DAY_CONDITION"),
                        messageTemplate: 'Record {0} created! See it {1}!',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
            
            component.set('v.today', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
        }
        else
        {
            
        }
        
    }
})