({
    getPicklistProducts : function(component) 
    {
        var action = component.get("c.getPickListValues");
        action.setParams({});
        action.setCallback(this, function(response) 
                           {
                               var state = response.getState();
                               
                               if (state === "SUCCESS")
                               {
                                   var conts = response.getReturnValue();
                                   component.set("v.product", conts);                
                               }
                               else if (state === "INCOMPLETE") 
                               {
                                   console.log("Product without info");
                               } 
                                   else if (state === "ERROR")
                                   {
                                       var errors = response.getError();
                                       if (errors) 
                                       {
                                           if (errors[0] && errors[0].message) 
                                           {
                                               console.log("Error message: " + errors[0].message);
                                           }
                                       } 
                                       else 
                                       {
                                           console.log("Unknown error");
                                       }
                                   }
                           });
        $A.enqueueAction(action);
    },
    
    changeProducts: function(component, event, helper) 
    {
        var idCase = component.get("v.recordId");
        console.log(idCase);
        var inputProduct = component.find('selectMaterial').get('v.value');
        console.log(inputProduct);
        var quantity = component.find('quantityId').get('v.value');
        
        if(inputProduct !=''){
            var action = component.get("c.GetProductCase");
            action.setParams({idCase: idCase, product: inputProduct});
            action.setCallback(this, function(response)
            {
                var state = response.getState();
                if(state === "SUCCESS")
                {
                    var conts = response.getReturnValue();
                    component.set("v.productId", conts.Id);
                    component.set("v.productName", conts.ONTAP__MaterialProduct__c);
                    component.set("v.productCode", conts.ONTAP__ProductCode__c);
                    component.set("v.quantity", quantity);
                    component.set("v.FinalValue", "True");
                }
                else if(state === "ERROR")
                {
                    var errors = response.getError();
                    if (errors)
                    {
                        if (errors[0] && errors[0].message)
                        {
                            console.log("Error message: " + errors[0].message);
                        }
                    } 
                    else
                    {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }

        
    },
    
    finalProducts: function(component, event, helper) 
    {
        var idCase = component.get("v.recordId");
        var action = component.get("c.SetProductCase");
        action.setParams({idCase: idCase});
        
        action.setCallback(this, function(response)
                           {
                               var state = response.getState();
                               if(state === "SUCCESS")
                               {
                                   var conts = response.getReturnValue();
                                   console.log(conts);
                                   if(conts.length > 0)
                                   {
                                       component.set("v.quantity", conts[0].ISSM_Count__c);
                                       component.set("v.productName", conts[1].ONTAP__MaterialProduct__c);
                                       component.set("v.productCode", conts[1].ONTAP__ProductCode__c);
                                       component.set("v.productId", conts[1].Id);
                                       component.set("v.selectedValue",conts[1].ONTAP__ProductCode__c);
                                       component.set("v.FinalValue", "True");
                                       console.log(conts[0].ISSM_Count__c);
                                       console.log(conts[1].ONTAP__MaterialProduct__c);
                                       console.log(conts[1].ONTAP__ProductCode__c);
                                       console.log(conts[1].Id);
                                       console.log(conts[1].ONTAP__ProductCode__c);
                                   }                
                               }
                               else if(state === "ERROR")
                               {
                                   var errors = response.getError();
                                   if (errors)
                                   {
                                       if (errors[0] && errors[0].message)
                                       {
                                           console.log("Error message: " + errors[0].message);
                                       }
                                   } 
                                   else
                                   {
                                       console.log("Unknown error");
                                   }
                               }
                           });
        $A.enqueueAction(action);
    },
    
    updateAndCallout: function(component, event, helper)
    {
        var idCase = component.get("v.recordId");
        var codeProduct = component.get("v.productCode");
        var productId = component.get("v.productId");
        var quantity= component.get("v.quantity");
        
        var action = component.get("c.UpdateCase");
        action.setParams({idCase:idCase, codeProduct:codeProduct, productId:productId, quantity:quantity});
               
        action.setCallback(this, function(response)
                           {
                               var state = response.getState();
                               if(state === "SUCCESS")
                               {
                                   var isUpdatedAndSend = response.getReturnValue();
                                   if(isUpdatedAndSend)
                                   {
                                       
                                       component.set("v.showSpinner", false);
                                       component.set("v.sendSuccess", true);
                                       component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Instalacion"));
                                       component.set("v.buttonDisable",true);
                                       component.set("v.Edit",false);
                                   }
                                   else
                                   {
                                       component.set("v.showSpinner", false);
                                       component.set("v.sendError", true);
                                       component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Error"));
                                       component.set("v.buttonDisable",true);
                                       component.set("v.Edit",false);
                                   }
                               }
                               else if(state === "ERROR")
                               {
                                   var errors = response.getError();
                                   if (errors)
                                   {
                                       if (errors[0] && errors[0].message)
                                       {
                                           console.log("Error message: " + errors[0].message);
                                       }
                                   } 
                                   else
                                   {
                                       console.log("Unknown error");
                                   }
                               }
                           });
        $A.enqueueAction(action);                
    },
    //SM
    updateOnly: function(component, event, helper)
    {
        var idCase = component.get("v.recordId");
        var codeProduct = component.get("v.productCode");
        var productId = component.get("v.productId");
        var quantity= component.get("v.quantity");
        var onlyUpdate = true;

        var action = component.get("c.UpdateCase");
        action.setParams({idCase:idCase, codeProduct:codeProduct, productId:productId, quantity:quantity, onlyUpdate});
               
        action.setCallback(this, function(response)
                           {
                               var state = response.getState();
                               if(state === "SUCCESS")
                               {
                                   var isUpdatedAndSend = response.getReturnValue();
                                   if(isUpdatedAndSend)
                                   {
                                       
                                       component.set("v.showSpinner", false);
                                       component.set("v.sendSuccess", true);
                                       //component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Instalacion"));
                                       component.set("v.mensajeEnvio","La información del activo se guardó correctamente");
                                       component.set("v.buttonDisable",true);
                                       component.set("v.Edit",false);
                                   }
                                   else
                                   {
                                       component.set("v.showSpinner", false);
                                       component.set("v.sendError", true);
                                       //component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Error"));
                                       component.set("v.mensajeEnvio","Se produjo un error al guardar los datos");
                                       component.set("v.buttonDisable",true);
                                       component.set("v.Edit",false);
                                   }
                               }
                               else if(state === "ERROR")
                               {
                                   var errors = response.getError();
                                   if (errors)
                                   {
                                       if (errors[0] && errors[0].message)
                                       {
                                           console.log("Error message: " + errors[0].message);
                                       }
                                   } 
                                   else
                                   {
                                       console.log("Unknown error");
                                   }
                               }
                           });
        $A.enqueueAction(action);                
    }
})