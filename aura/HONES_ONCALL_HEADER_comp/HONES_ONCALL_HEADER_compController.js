({
    refreshViews: function (component, event, helper) {
        //Account updated successfully
        //Fire the application event to update all view components               
        var appEvent = $A.get("e.c:eventReloadComponents");
        console.log('event fire from header');
        appEvent.setParams({"reloadView": true});
        setTimeout($A.getCallback(function() {
            console.log('event fire from header set');
            appEvent.fire();
        }), 2000);        		        
    }  
})