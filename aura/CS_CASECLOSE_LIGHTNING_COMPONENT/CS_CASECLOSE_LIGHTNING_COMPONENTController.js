({
    loadDataPage: function(cmp, event, helper)
    {
        var idCase = cmp.get("v.recordId");
        /* Case Detail */
        helper.GetDataCase(cmp, idCase);
        /* Account Name */
        helper.GetAccountName(cmp, idCase);
        /* Questions */
        helper.GetQuestions(cmp, idCase);
        /* Option Master Solution */
        helper.GetOptionsMasterSolution(cmp);

    },
    
    handleClickButtonClose: function(cmp, event, helper)
    {
        var inputValid = true;
        var mensajesError = [];
        
        cmp.set("v.showSpinner", true);
        
        var idCase = cmp.get("v.recordId");
        var idCase = cmp.get("v.recordId");
        var descriptionCloseCase = cmp.find("textareaDescription").get("v.value");
        var solutionMaster = cmp.find("SelectSolutionMaster").get("v.value");
        var DetailCase = cmp.get("v.DetailCase");        
                
        if(descriptionCloseCase === undefined || descriptionCloseCase === '')
        {
            inputValid = false;
            mensajesError.push($A.get("$Label.c.CS_Solucion_Caso"));
        }
        
        if(typeof solutionMaster === undefined || solutionMaster === '')
        {
            inputValid = false;
            mensajesError.push($A.get("$Label.c.CS_Seleccion_Maestro_Solucion"));
        }
        
        if(inputValid)
        {
            helper.CloseCase(cmp, idCase, DetailCase.ISSM_CaseForceNumber__c, descriptionCloseCase, solutionMaster);      
            cmp.set("v.showSpinner", false);
        }
        else
        {
            cmp.set("v.showSpinner", false);
            cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Campos_Obligatorios"));
            cmp.set("v.mensajesValidacion", mensajesError);
            cmp.set("v.showModal", !inputValid);
        } 
    },
    
    handleClickButtonCloseModal: function(cmp, event, helper)
    {
        cmp.set("v.showModal", false);
        cmp.set("v.showSpinner", false);
    }
})