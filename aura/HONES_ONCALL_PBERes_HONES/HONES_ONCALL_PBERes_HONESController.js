({
     doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        //helper.getpicklist(component,recordId);
    },
    
    
    
    selectRecord : function(component, event, helper){      
        // get the selected record from list selectRecord 
         var getSelectRecord = component.get("v.oRecord");
        getSelectRecord.quantity = component.get("v.quantity");
        
        // added extra validation for non integer numbers
        if (getSelectRecord.quantity == null || !Number.isInteger(getSelectRecord.quantity) || getSelectRecord.quantity<1 ){            
            console.log("::: PBE getSelectRecord.quantity" +  getSelectRecord.quantity)            
            alert($A.get("$Label.c.You_must_enter_an_amount"));
        }else{  
            	//component.get("v.oRecord.listaPBE").forEach( function(item){
                //if (item == component.get("v.selectedValue")){
				//	getSelectRecord.pbereason=component.get("v.selectedValue");
                //    getSelectRecord.productType='Good Shape Product';
                    
             		var compEvent = component.getEvent("oSelectedRecordEvent");
               				getSelectRecord.selectedByUser=true;

            compEvent.setParams({"recordByEvent" : getSelectRecord});  
             		 // fire the event  
             		 compEvent.fire();
                       component.find("inputbar").set("v.value", "");

                    
            }
            
            
    },
            
            clear :function(component,event,heplper){
            
            var pillTarget = component.find("lookup-pill");
            var lookUpTarget = component.find("lookupField");  
            
            $A.util.addClass(pillTarget, 'slds-hide');
            $A.util.removeClass(pillTarget, 'slds-show');
            
            $A.util.addClass(lookUpTarget, 'slds-show');
            $A.util.removeClass(lookUpTarget, 'slds-hide');
            component.set("v.SearchKeyWord",null);
            component.set("v.listOfSearchRecords", null );
            component.set("v.selectedRecord", {} );
            component.set("v.search", "slds-input__icon slds-show");
            
            
            }
            })