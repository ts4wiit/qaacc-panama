({
    onfocus : function(cmp, event, helper)
    {
        $A.util.addClass(cmp.find("mySpinner"), "slds-show");
        var forOpen = cmp.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        
        var getInputkeyWord = '';
        helper.searchHelper(cmp, event, getInputkeyWord);
    },
    
    onblur : function(cmp, event, helper)
    {       
        cmp.set("v.listOfSearchRecords", null );
        var forclose = cmp.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    keyPressController : function(cmp, event, helper) 
    { 
        var getInputkeyWord = cmp.get("v.SearchKeyWord");
        
        if( getInputkeyWord.length > 0 )
        {
            var forOpen = cmp.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(cmp, event, getInputkeyWord);
        }
        else
        {  
            cmp.set("v.listOfSearchRecords", null ); 
            var forclose = cmp.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
	},
    
    clear : function(cmp, event, helper)
    {
        var pillTarget = cmp.find("lookup-pill");
        var lookUpTarget = cmp.find("lookupField"); 
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
      
        cmp.set("v.SearchKeyWord",null);
        cmp.set("v.listOfSearchRecords", null );
        cmp.set("v.selectedRecord", {} );   
    },
    
    handleComponentEvent : function(cmp, event, helper) 
    { 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
	    cmp.set("v.selectedRecord" , selectedAccountGetFromEvent); 
       
        var forclose = cmp.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = cmp.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = cmp.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');  
	},
})