({
    getDataLimit : function(cmp,recordId) {
        var action = cmp.get("c.creditLimit");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.creditLimits", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataDisponible : function(cmp,recordId) {
        var action = cmp.get("c.disponible");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.disponibles", conts);
                if(conts >= 0.0){
                    cmp.set("v.disponibleColorTotal",true);
                }
                else
                {
                    cmp.set("v.disponibleColorTotal",false);
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataOutOfDate : function(cmp,recordId) {
        var action = cmp.get("c.outOfDateCredits");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.outOfDateCredit", conts);
                if(conts < 0.0){
                    cmp.set("v.disponibleColorVencido",true);
                }
                else
                {
                    cmp.set("v.disponibleColorVencido",false);
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataOutOfDateLiquid : function(cmp,recordId) {
        var action = cmp.get("c.outOfDateCreditsLiquid");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.outOfDateCreditsLiquids", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataOutOfDateContainer : function(cmp,recordId) {
        var action = cmp.get("c.outOfDateCreditsContainer");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.outOfDateCreditsContainers", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataOfDate : function(cmp,recordId) {
        var action = cmp.get("c.dateCredits");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.dateCredit", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataOfDateTotal : function(cmp,recordId) {
        var action = cmp.get("c.total");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.total", conts);                
                if(conts < 0.0){
                    cmp.set("v.disponibleColorTotal",true);
                }
                else
                {
                    cmp.set("v.disponibleColorTotal",false);
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataOnTimeDateLiquid : function(cmp,recordId) {
        var action = cmp.get("c.onTimeDateCreditsLiquid");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.onTimeDateCreditsLiquids", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataOnTimeDateContainer : function(cmp,recordId) {
        var action = cmp.get("c.onTimeDateCreditsContainer");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.onTimeDateCreditsContainers", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            //console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        //console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    }
})