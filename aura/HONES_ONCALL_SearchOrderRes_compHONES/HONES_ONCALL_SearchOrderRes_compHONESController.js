({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        //helper.getpicklist(component,recordId);
    },
    
    selectRecord : function(component, event, helper){
        // get the selected record from list selectRecord         
        var getSelectRecord = component.get("v.oRecord");
        getSelectRecord.quantity = component.get("v.quantity");

        if (getSelectRecord.quantity == null){
            alert($A.get("$Label.c.You_must_enter_an_amount"));
        } else {
            component.set("v.isLoading", true);
            component.get("v.oRecord.lista").forEach(function(item){
                if (item == component.get("v.selectedValue")){
                    getSelectRecord.productType=component.get("v.selectedValue"); 
                    getSelectRecord.selectedByUser=component.get("v.clickedbyUser");
                    // call the event

                    //FANNY ALERT
                    var finishedProduct = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
   
                    if(getSelectRecord.productType == finishedProduct) {
                        var compEvent = component.getEvent("oSelectedRecordEvent");

                        if(getSelectRecord.isRelated == false & getSelectRecord.relatedProducts.length == 0 & getSelectRecord.isReturnable == true){
                            alert($A.get("$Label.c.You_can_not_select_this_material"));
                        } else {
                            var relatedItem = getSelectRecord.relatedProducts;
                            var i = 0;

                            for(i = 0; i < relatedItem.length; i++){
                                if(getSelectRecord.relatedProducts[i].isRelated == true){
                                    getSelectRecord.relatedProducts[i].quantity = getSelectRecord.relatedProducts[i].quantity * getSelectRecord.quantity;
                                }
                            };
                            compEvent.setParams({"recordByEvent" : getSelectRecord});
                            fireEvent();
                        }
 
                    } else {
                        var compEvent = component.getEvent("oSelectedRecordEvent");
                        delete getSelectRecord["relatedProducts"];
                        compEvent.setParams({"recordByEvent" : getSelectRecord});
                        fireEvent();
                    };

                    function fireEvent(){
                        compEvent.fire();
                        component.find("inputbar").set("v.value", "");
                        var searchEvent = component.getEvent("oSearchRecordEvent");
                        searchEvent.setParams({"cleanAndFocus" : true});  
                        // fire the event  
                        searchEvent.fire();
                    };
                    component.set("v.isLoading", false);
                }
            });
        }                        
    },
    
    clear :function(component,event,heplper){        
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField");  
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');

        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
        component.set("v.selectedRecord", {} );
        component.set("v.search", "slds-input__icon slds-show");                
    }
})