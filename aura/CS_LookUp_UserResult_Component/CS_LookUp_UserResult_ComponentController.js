({
   selectRecord : function(cmp, event, helper)
    {      
      var getSelectRecord = cmp.get("v.oRecord");
      var compEvent = cmp.getEvent("oSelectedRecordEvent");
      compEvent.setParams({"recordByEvent" : getSelectRecord });  
      compEvent.fire();
    }
})