({	
    doInit : function(component, event, helper)
    {
        //Get Lists of Questions and Answers
        console.log('Ingresando a metodo doInit');
        helper.getQuestionsAnswersFunc(component, event);        
    },    
    
    saveAnsw : function(component, event, helper)
    {
        //Save List of answers
        console.log('Ingresando a metodo saveFunc');
        helper.saveFunc(component, event);
    },
    
    collapseSeccion : function(component, event, helper) 
    {
       console.log('Ingresando a metodo collapse');
       helper.collapseFunc(component,event,'expand');
    },
    
    functionPhone : function(component, event, helper)
    {
        helper.formatPhone(component, event);
	},
    
    functionNumber : function(component, event, helper){
        helper.formatNumber(component, event);
    }
})