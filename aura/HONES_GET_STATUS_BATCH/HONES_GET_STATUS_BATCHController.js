({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        console.log('getStatus:doInit' + recordId);
    },
     getStatusHelper: function(component, event, helper){
        helper.getStatusSAPBatch(component, event, helper);
        $A.get("e.force:closeQuickAction").fire();
     }
})