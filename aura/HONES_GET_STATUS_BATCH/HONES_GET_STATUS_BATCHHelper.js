({
    getStatusSAPBatch : function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId");
        var action = cmp.get("c.getStatusBatch");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                console.log("conts: ");
                console.log(conts);
                if(conts.status == 'OK'){
                    this.setMessagge(cmp, event, "Obteniendo OK" + conts.message);
                    location.reload();
                }
            }
            else if (state === "ERROR") { 
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            this.setMessagge(cmp, event, "Error " + errors[0].message, "error");
                            
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    }, 

    setMessagge : function(cmp,event,message, severity){
        cmp.set("v.message", message);
        cmp.set("v.severity", severity);
        $A.get('e.force:refreshView').fire();
    }
})