({
	GetTypificationCase: function(component, pIdCase)
    {
        var action = component.get("c.GetCaseTypification");
        action.setParams({idCase: pIdCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                component.set("v.DetailCase", response.getReturnValue());
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getTypificationN1: function(component)
    {
        var action = component.get("c.GetTypificationN1");
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                component.set("v.listN1", response.getReturnValue());
                component.set("v.ShowlistN1", 'True');
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    getTypificationN2: function(cmp, typificationN1)
    {
        var action = cmp.get("c.GetTypificationN2");
        action.setParams({typificationNivel1: typificationN1});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.listN2", response.getReturnValue());
                if(response.getReturnValue().length > 1)
                {
                    cmp.set("v.ShowlistN2", 'True');
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getTypificationN3: function(cmp, typificationN1, typificationN2)
    {
        var action = cmp.get("c.GetTypificationN3");
        action.setParams({typificationNivel1: typificationN1, typificationNivel2: typificationN2});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.listN3", response.getReturnValue());
                if(response.getReturnValue().length > 1)
                {
                    cmp.set("v.ShowlistN3", 'True');
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getTypificationN4: function(cmp, typificationN1, typificationN2, typificationN3)
    {
        var action = cmp.get("c.GetTypificationN4");
        action.setParams({typificationNivel1:typificationN1, typificationNivel2:typificationN2, typificationNivel3:typificationN3});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.listN4", response.getReturnValue());
                if(response.getReturnValue().length > 1)
                {
                    cmp.set("v.ShowlistN4", 'True');   
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    UpdateTypification: function(cmp)
    {
        var idCase = cmp.get("v.recordId");
        var typificationNivel1 = cmp.get("v.TypificationN1"); 
        var typificationNivel2 = cmp.get("v.TypificationN2");
        var typificationNivel3 = cmp.get("v.TypificationN3");
        var typificationNivel4 = cmp.get("v.TypificationN4");
        
        var action = cmp.get("c.UpdateTypificationCase");
        action.setParams({idCase:idCase, typificationN1:typificationNivel1, typificationN2:typificationNivel2, typificationN3:typificationNivel3, typificationN4:typificationNivel4});
        action.setCallback(this, function(response)
        {  
            var state = response.getState();
            if(state === "SUCCESS")
            { 
                var isSuccess = response.getReturnValue();
                if(isSuccess)
                {
                     cmp.set("v.showSpinner", false);
                     sforce.one.navigateToURL('/'+ idCase, true);
                }
                else
                {
                    cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Error"));
                    cmp.set("v.mensajesValidacion", $A.get("$Label.c.CS_Retyping_Error"));
                    cmp.set("v.showModal", true);  
                    cmp.set("v.showSpinner", false);
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action); 
    }
})