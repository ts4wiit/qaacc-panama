({
	searchApex : function(cmp,recordId,event) {
        var action = cmp.get("c.sendOrder");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                console.log("conts: " + conts);
                cmp.set("v.orderId", conts);
                location.reload();
            }
            else if (state === "ERROR") { 
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    }

})