({
	init : function(component, event, helper) {
        helper.getProductList(component);
        
        component.set('v.columns_products', [
            { label: 'SKU', fieldName: 'ONTAP__ProductCode__c', type: 'text' },
            { label: 'Nombre del producto', fieldName: 'ONTAP__MaterialProduct__c', type: 'text' }
        ]);
	},
    
    searchProductList : function(component, event, helper){
        var searchKey = component.get("v.searchKey");
        if(searchKey.length >= 3){
            component.set("v.isLoading",true);
            component.set("v.flagUpdateProductList",true);
            helper.getProductList(component);
        }
    },
    
    editExcludedProducts : function(component, event, helper){
        var productsInList = component.get("v.SearchProducts");
        var selectedProductsInList = component.find('linesTable').getSelectedRows();
        var selectedRows = component.get("v.selectedRows");
        
        for(var i in productsInList){
            var checkedProd = selectedProductsInList.find(x => x.Id === productsInList[i].Id);
            if(checkedProd== undefined && selectedRows.includes(productsInList[i].Id) && !component.get("v.flagUpdateProductList")){ // eliminar producto previamente seleccionado
                selectedRows = selectedRows.filter(x => x !== productsInList[i].Id);
            }else if(checkedProd != undefined && !selectedRows.includes(productsInList[i].Id)){//añadir producto seleccionado en lista
                selectedRows.push(checkedProd.Id);
            }
        }
        component.set("v.selectedRows", selectedRows);
        component.set("v.disabledButton", false);
        component.set("v.flagUpdateProductList",false);
        component.set('v.saved',false);
    }, 
    
    saveProductExclusion : function(component, event, helper){
        var massive = component.get("v.massive");
        component.set('v.saved',false);
        if(massive){
            component.set("v.disabledButton", true);
            helper.saveMassiveExclusion(component);
        }else{
            helper.saveExclusionPerClient(component);
        }
    }
    
})