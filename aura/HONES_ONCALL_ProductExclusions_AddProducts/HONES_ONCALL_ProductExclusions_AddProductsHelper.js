({
	getProductList : function(component) {
        var action = component.get("c.getProducts");
        action.setParams({
            searchKey : '%' +  component.get('v.searchKey') + '%',
        });
        action.setCallback(this, function(response) {
            component.set("v.isLoading", true);
            var state = response.getState();
            if (state === "SUCCESS") {
                var SearchProducts = response.getReturnValue();
                component.set("v.SearchProducts", SearchProducts);
                component.set('v.saved',false);
                component.set('v.savedMessage','');
                if(component.get("v.initialGet")){
                    var massive = component.get("v.massive");
                    if(massive){
                        var productExcluded = component.get("v.productExcludedMassive");
                    }else{
                        var productExcluded = component.get("v.productExcludedPerClient");
                    }
                    
                    var selectedRowsIds = []
                    for (var i = 0; i < productExcluded.length; i++) {
                        selectedRowsIds.push(productExcluded[i].HONES_RelatedProduct__c );
                    }
                    component.set("v.selectedRows", selectedRowsIds);
                    component.set("v.initialGet", false);
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message){
                        component.set("v.errorMessage", 'No se pudo obtener lista de productos');
                        console.log("======== No se pudo obtener lista de productos: " + errors[0].message);
                    }
                } else { console.log("Unknown error"); }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
	}, 
    
    saveExclusionPerClient : function(component){
        var action = component.get("c.saveExclusionPerClient");
        action.setParams({
            client : component.get('v.client'),
            id_products: component.get('v.selectedRows')
        });
        action.setCallback(this, function(response) {
            component.set("v.isLoading", true);
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.saved',true);
                component.set('v.savedMessage','Se han actualizado las exclusiones por producto de este cliente');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message){
                        component.set("v.errorMessage", 'Error al generar exclusiones');
                        console.log("======== Error al generar exclusiones: " + errors[0].message);
                    }
                } else { console.log("Unknown error"); }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    }, 
    
    saveMassiveExclusion : function(component){
        var action = component.get("c.saveMassiveExclusion");
        action.setParams({
            searchCriteria : component.get('v.searchCriteria'),
            id_products: component.get('v.selectedRows'), 
            clientsReturned: component.get('v.clientsReturned'), 
            nInitialProducts : component.get("v.productExcludedMassive").length
        });
        action.setCallback(this, function(response) {
            component.set("v.isLoading", true);
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.saved',true);
            	component.set('v.savedMessage','Se ha iniciado una modificación masiva de registros');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message){
                        component.set("v.errorMessage", 'Error al generar exclusiones');
                        console.log("======== Error al generar exclusiones: " + errors[0].message);
                    }
                } else { console.log("Unknown error"); }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    }
    
})