({
	doInit : function(component, event, helper) {
		
	},
    
    clickSend :function(component, event, helper) {
        
        var childComponentRetiro = component.find('childRetiro');
		childComponentRetiro.methodRetiroButtonSend();

        var childComponentInstalacion = component.find('childInstalacion');
		childComponentInstalacion.methodInstalacionButtonSend();	
	},

	clickSave:function(component) {

		var childComponentRetiro = component.find('childRetiro');
		childComponentRetiro.methodRetiroButtonSave();

		var childComponentInstalacion = component.find('childInstalacion');
		childComponentInstalacion.methodInstalacionButtonSave();
	}
    
})