({
	loadDataPage : function(cmp, event, helper) 
    {
        var idCase = cmp.get("v.recordId");
        /* Detail Case */
        helper.GetDataCase(cmp, idCase);
        /* Account Name */
        helper.GetAccountName(cmp, idCase);
        /* Assigned */
        helper.GetAssignedIntelocutorOption(cmp);
	},
    
    handleChangeAssigned : function(cmp, event, helper)
    {
        var asignacionUsuario = $A.get("$Label.c.CS_Asignacion_Usuario");
        var assignedType = cmp.find("SelectAssignedTo").get("v.value");
        var idCase = cmp.get("v.recordId");
        cmp.set("v.showSpinner", true);
        if(assignedType == asignacionUsuario)
        {
           cmp.set("v.showLookUp", true);
        }
        else
        {
            cmp.set("v.showLookUp", false);
            helper.GetInterlocutorDetail(cmp, assignedType, idCase)
        }
        cmp.set("v.showSpinner", false);
    },
    
    handleClickButtonAssigned : function (cmp, event, helper)
    {
        var inputValid = true;
        var mensajesError = [];
        var idOwner;
        
        var idCase = cmp.get("v.recordId");
        var idCaseForce = cmp.get("v.DetailCase.ISSM_CaseForceNumber__c");
        var assignedType = cmp.find("SelectAssignedTo").get("v.value");
        var asignacionUsuario = $A.get("$Label.c.CS_Asignacion_Usuario");
        
        cmp.set("v.showSpinner", true);
        
        /* Se valida si se selecciono un tipo de assignación */
        if(typeof assignedType === undefined || assignedType === '')
        {
            mensajesError.push($A.get("$Label.c.CS_Error_Asignacion"));
            inputValid = false;
        }
        else if(assignedType == asignacionUsuario) /* Si la asignacion es para un usuario se valida que se haya seleccionado uno en el lookup */
        {
            idOwner = cmp.get("v.selectedLookUpRecord.Id");
            if(idOwner == undefined)
            {
                mensajesError.push($A.get("$Label.c.CS_Error_Usuario_Obligatorio"));
                inputValid = false;
            }
        }
        else /* Interlocutores */
        {
            idOwner = cmp.get("v.DetailIntelocutor.Id");
            /* Se valida que la cuenta tenga un interlocutor asignado */
            if(typeof idOwner === undefined || idOwner === '' || idOwner === undefined || idOwner == null)
            {
                mensajesError.push($A.get("$Label.c.CS_Error_Interlocutor") + " " + assignedType);
                inputValid = false;
            }
        }
        
        
        if(inputValid)
        {
            helper.UpdateCaseOwner(cmp, idCase, idCaseForce, idOwner, assignedType)
        }
        else
        {
            cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Campos_Obligatorios"));
            cmp.set("v.mensajesValidacion", mensajesError);
            cmp.set("v.showModal", !inputValid); 
            cmp.set("v.showSpinner", false); 
        }
    },
    
    handleClickButtonCloseModal : function(cmp, event, helper)
    {
       cmp.set("v.showModal", false);
    }
})