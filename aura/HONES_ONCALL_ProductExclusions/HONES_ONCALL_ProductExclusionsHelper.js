({
    getPickListSubchannelRequest : function(component) {
        var action = component.get("c.getPickListValuesSubchannel");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var Subcanales = response.getReturnValue();
                component.set("v.Subcanales", Subcanales);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    getPickListKATR3Request : function(component) {
        var action = component.get("c.getPickListValuesKATR3");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var Atributos3 = response.getReturnValue();
                component.set("v.Atributos3", Atributos3);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    getPickListDistributionCenterRequest : function(component) {
        var action = component.get("c.getPickListValuesDistributionCenter");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var subchannels = response.getReturnValue();
                component.set("v.Localidades", subchannels);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getClient : function(component){
        var action = component.get("c.getClient");
        action.setParams({
            Id_SAP : component.get('v.Id_SAP_Cliente'),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var cliente = response.getReturnValue();
                this.getProductExcludedPerClient(component, cliente['Id']);
                component.set("v.client", cliente);
                component.set("v.showErrors",false);
                component.set('v.Id_SAP_Bool',true);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.showErrors",true);
                        component.set("v.errorMessage", 'No se pudo localizar al cliente');
                        console.log("===== ERROR, NO SE PUDO LOCALIZAR AL CLIENTE:" + errors[0].message);
                        component.set('v.Id_SAP_Bool',false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }, 
    
    getMassiveClients : function(component){
        var action = component.get("c.getCountClients");
        action.setParams({
            DistributionCenter : component.get('v.localidadSeleccionada'),
            Subchannel : component.get('v.subcanalSeleccionado'),
            Attr3 : component.get('v.attr3Seleccionado'),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var clientsReturned = response.getReturnValue();
                if(clientsReturned > 0 ){
                    component.set("v.clientsReturned", clientsReturned);
                    this.getMassiveProductExcluded(component);
                	component.set("v.showErrors",false);
                    component.set("v.errorMessage", '');
                    component.set('v.busquedaMasivaBool',true);
                }else{
                    component.set("v.showErrors",true);
                    component.set("v.errorMessage", 'No localizó ningún cliente bajo los criterios indicados');
                    component.set('v.busquedaMasivaBool',false);
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.showErrors",true);
                        component.set("v.errorMessage", 'No se pudo localizar clientes');
                        console.log("===== ERROR, NO SE PUDO LOCALIZAR AL CLIENTE:" + errors[0].message);
                        component.set('v.Id_SAP_Bool',false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    }, 
    
    getMassiveProductExcluded : function(component) {
        var action = component.get("c.getMassiveProductExcluded");
        action.setParams({
            DistributionCenter : component.get('v.localidadSeleccionada'),
            Subchannel : component.get('v.subcanalSeleccionado'),
            Attr3 : component.get('v.attr3Seleccionado'),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var productExcludedMassive = response.getReturnValue();
                component.set("v.productExcludedMassive", productExcludedMassive);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.showErrors",true);
                        component.set("v.errorMessage", 'Se produjo un error al recuperar exclusiones previas');
                        console.log("===== Se produjo un error al recuperar exclusines previas:" + errors[0].message);
                        component.set('v.Id_SAP_Bool',false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }, 
    
    getProductExcludedPerClient : function(component, id_client){
        var action = component.get("c.getProductExcludedPerClient");
        action.setParams({
            id_client : id_client
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var productExcludedPerClient = response.getReturnValue();
                component.set("v.productExcludedPerClient", productExcludedPerClient);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.showErrors",true);
                        component.set("v.errorMessage", 'Se produjo un error al recuperar exclusiones previas');
                        console.log("===== Se produjo un error al recuperar exclusines previas:" + errors[0].message);
                        component.set('v.Id_SAP_Bool',false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
});