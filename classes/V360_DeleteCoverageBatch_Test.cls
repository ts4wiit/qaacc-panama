/*
 * Clase: V360_DeleteCoverageBatch_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Carlos Leal        Creation of methods.
 */
@isTest
public class V360_DeleteCoverageBatch_Test {
    
    /**
   * Method test for delete all the coverage in V360_Coverage.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testBatch(){
        V360_Coverage__c ob = new V360_Coverage__c(V360_This__c =TRUE);
        insert ob;
        V360_DeleteCoverageBatch obj01 = new V360_DeleteCoverageBatch();
        Database.executeBatch(obj01, 200);
    }
}