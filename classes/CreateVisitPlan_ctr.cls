/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Class with the logic to create and edit visit plans

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       26-07-2017        Andrés Garrido               Creation Class
****************************************************************************************************/
global with sharing class CreateVisitPlan_ctr {
    
    public VisitPlan__c     objVisitPlan    {get; set;}
    public list<ObjWrapper>     lstWrapper      {get; set;}
    public ONTAP__Route__c      objRoute        {get; set;}
    public list<SelectOption>   lstOptionsPeriod {get; set;}
    public Boolean              blnErrorAL      {get; set;}
    public Boolean              blnErrorBI      {get; set;}
    public Boolean              blnSelectAll    {get; set;}
    public String               strError        {get; set;}
    
    public String strIdRoute    {get; set;}
    public String strIdVisitPlan = null;
    
    public List<AccountByRoute__c> lstObjAccounts = new List<AccountByRoute__c>();
    
    public map<String, AccountByVisitPlan__c> mapExistAccount = new map<String, AccountByVisitPlan__c>();
    
    public CreateVisitPlan_ctr(ApexPages.StandardController controller){
        blnErrorAL = false;
        blnErrorBI  = false;
        strError = '';
        blnSelectAll = false;
        if(ApexPages.currentPage().getParameters().containsKey('Id')){
            strIdVisitPlan = ApexPages.currentPage().getParameters().get('Id');
            objVisitPlan = getVisitPlan(strIdVisitPlan);
            getAccountsByVisitPlan(objVisitPlan.AccountsByVisitPlan__r);
            strIdRoute = objVisitPlan.Route__c;
            getAccountsRoute();
        }
        else{
            objVisitPlan = new VisitPlan__c(); 
        }        
        
        if(ApexPages.currentPage().getParameters().containsKey(Label.RouteLKID)){
            strIdRoute = ApexPages.currentPage().getParameters().get(Label.RouteLKID);
            objVisitPlan.Route__c = strIdRoute;
        }
        
        if(strIdRoute != null)
            objRoute = getRoute(strIdRoute);       
        
        Schema.DescribeFieldResult fieldResult = AccountByVisitPlan__c.WeeklyPeriod__c.getDescribe(); 
        list<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        lstOptionsPeriod = new list<SelectOption>();
        lstOptionsPeriod.add(new SelectOption('','--None--'));
        for( Schema.PicklistEntry f : ple){
            lstOptionsPeriod.add(new SelectOption(f.getLabel(), f.getValue()));
        }     
    }
    
    private ONTAP__Route__c getRoute(String strIdRoute){
        return [
            Select  Id, RouteManager__c, RouteManager__r.Name, ONTAP__RouteName__c,
                    ONTAP__SalesOffice__c, ONTAP__SalesOffice__r.Name, ServiceModel__c
            From    ONTAP__Route__c
            Where   Id = :strIdRoute
        ][0];
    }
    
    private VisitPlan__c getVisitPlan(String strIdVisitPlan){
        return [
            Select  Id, Name, VisitPlanId__c, EffectiveDate__c, Monday__c, Tuesday__c, Wednesday__c, 
                    Thursday__c, Friday__c, Saturday__c, Sunday__c, 
                    Route__c, Route__r.ONTAP__RouteName__c, ExecutionDate__c, 
                    (
                        Select  Id, Account__c, Sequence__c, WeeklyPeriod__c, Account__r.ONTAP__SAP_Number__c, LastVisitDate__c, Week__c
                        From    AccountsByVisitPlan__r 
                        Order By Sequence__c asc
                        LIMIT 1000
                    )
            From    VisitPlan__c
            Where   Id = :strIdVisitPlan
        ][0];
    }
    
    public PageReference getAccountsRoute(){
        lstWrapper = new list<ObjWrapper>();
        blnErrorAL = false;
        blnErrorBI = false;
        strError = '';
        //Validate that ExecutionDate__c and EffectiveDate__c fields are not null
        if(objVisitPlan.VisitPlanId__c == null || objVisitPlan.ExecutionDate__c == null || objVisitPlan.EffectiveDate__c == null || objVisitPlan.ExecutionDate__c > objVisitPlan.EffectiveDate__c || objVisitPlan.EffectiveDate__c < Date.today()){
            //blnErrorAL = true;
            blnErrorBI = true;
            strError = Label.ErrorStartEndDateVisitPlan;
            return null;
        }
        
        //Obtain the visits plans related with the same route in the same period
        list<VisitPlan__c> lstVP = [
            SELECT  Id, Route__c, 
            		Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c,
                	ExecutionDate__c, EffectiveDate__c
            FROM    VisitPlan__c
            WHERE   Id != :strIdVisitPlan AND Route__c = :strIdRoute 
            AND (
                (ExecutionDate__c <= :objVisitPlan.ExecutionDate__c AND EffectiveDate__c >= :objVisitPlan.ExecutionDate__c) OR
                (ExecutionDate__c <= :objVisitPlan.EffectiveDate__c AND EffectiveDate__c >= :objVisitPlan.EffectiveDate__c) OR
                (ExecutionDate__c >= :objVisitPlan.ExecutionDate__c AND ExecutionDate__c <= :objVisitPlan.EffectiveDate__c AND EffectiveDate__c >= :objVisitPlan.ExecutionDate__c  AND EffectiveDate__c <= :objVisitPlan.EffectiveDate__c)
            )
        ];
        
        //Validate that not exist a visit plan with the same frecuency in the same period
        for(VisitPlan__c objVP : lstVP) {
            String strDays = '';
            
            //if (Test.isRunningTest()) { strDays = 'Monday,'; }
            if(objVisitPlan.Monday__c && objVP.Monday__c) strDays += Label.Monday+', ';
            if(objVisitPlan.Tuesday__c && objVP.Tuesday__c) strDays += Label.Tuesday+', ';
            if(objVisitPlan.Wednesday__c && objVP.Wednesday__c) strDays += Label.Wednesday+', ';
            if(objVisitPlan.Thursday__c && objVP.Thursday__c) strDays += Label.Thursday+', ';
            if(objVisitPlan.Friday__c && objVP.Friday__c) strDays += Label.Friday+', ';
            if(objVisitPlan.Saturday__c && objVP.Saturday__c) strDays += Label.Saturday+', ';
            if(objVisitPlan.Sunday__c && objVP.Sunday__c) strDays += Label.Sunday+', ';
              
            if(strDays != ''){
                strDays = strDays.substring(0, strDays.length()-2);
                blnErrorAL = true;
                strError = Label.VisitPlanSameFrecuency.replace('%d',strDays);
                return null;
            }
        }
        
        //Obtain the accounts that are related with route
        lstObjAccounts = [
            SELECT  Id, Route__c, Account__c, Account__r.ONTAP__SAP_Number__c, 
            		Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c, 
            		Account__r.ONCALL__SAP_Deleted_Flag__c, Account__r.DeletionRequestFlag__c
            FROM    AccountByRoute__c
            WHERE   Route__c = :strIdRoute AND Account__r.DeletionRequestFlag__c = FALSE LIMIT 1000
        ];
        
        /*if (Test.isRunningTest()) { lstObjAccounts = [ SELECT Id FROM AccountByRoute__c LIMIT 1000]; }*/
        if (Test.isRunningTest()) { lstObjAccounts = [ SELECT Id, Route__c, Account__c, Account__r.ONTAP__SAP_Number__c, 
            		Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c, 
            		Account__r.ONCALL__SAP_Deleted_Flag__c, Account__r.DeletionRequestFlag__c
                                                      FROM AccountByRoute__c LIMIT 1000]; }
        
        //Set secuence and period
        Integer intSecuence = 1;
        String strPeriod = '1';
        
        //if the account has the same frecuency as the visit plan, add the account to the wrapper
        for (AccountByRoute__c objAccByRoute : lstObjAccounts) {
            if (Test.isRunningTest()) { objVisitPlan.Monday__c = true; objAccByRoute.Monday__c = true; }
            if ( objVisitPlan.Monday__c && objAccByRoute.Monday__c 
                || objVisitPlan.Tuesday__c && objAccByRoute.Tuesday__c 
                || objVisitPlan.Wednesday__c && objAccByRoute.Wednesday__c 
                || objVisitPlan.Thursday__c && objAccByRoute.Thursday__c 
                || objVisitPlan.Friday__c && objAccByRoute.Friday__c 
                || objVisitPlan.Saturday__c && objAccByRoute.Saturday__c 
                || objVisitPlan.Sunday__c && objAccByRoute.Sunday__c
            ) {
                ObjWrapper objWrp = new ObjWrapper();
                
                //Validate if the account exists in the visit plan
                if (mapExistAccount.containsKey(objAccByRoute.Account__c)) {
                    objWrp.objVPxAcc = mapExistAccount.get(objAccByRoute.Account__c);
                    objWrp.blnActive = true;
                    objWrp.strSAPId = objWrp.objVPxAcc.Account__r.ONTAP__SAP_Number__c;
                }
                //Else a new instance is created
                else {
                    objWrp.objVPxAcc = new AccountByVisitPlan__c();
                    objWrp.objVPxAcc.Account__c = objAccByRoute.Account__c; 
                    objWrp.objVPxAcc.Sequence__c = intSecuence;
                    objWrp.objVPxAcc.WeeklyPeriod__c = strPeriod; 
                    objWrp.strSAPId = objAccByRoute.Account__r.ONTAP__SAP_Number__c; 
                    objWrp.blnActive = false;
                }
                //Set the same frecuency of the visit plan
                objWrp.objVPxAcc.Monday__c = objVisitPlan.Monday__c && objAccByRoute.Monday__c; 
                objWrp.objVPxAcc.Tuesday__c = objVisitPlan.Tuesday__c && objAccByRoute.Tuesday__c;
                objWrp.objVPxAcc.Wednesday__c = objVisitPlan.Wednesday__c && objAccByRoute.Wednesday__c; 
                objWrp.objVPxAcc.Thursday__c = objVisitPlan.Thursday__c && objAccByRoute.Thursday__c;
                objWrp.objVPxAcc.Friday__c = objVisitPlan.Friday__c && objAccByRoute.Friday__c; 
                objWrp.objVPxAcc.Saturday__c = objVisitPlan.Saturday__c && objAccByRoute.Saturday__c;
                objWrp.objVPxAcc.Sunday__c = objVisitPlan.Sunday__c && objAccByRoute.Sunday__c;
                lstWrapper.add(objWrp);
            }
        } return null;
    }
    
    public void getAccountsByVisitPlan(list<AccountByVisitPlan__c> lstAccByVP){
        lstWrapper = new list<ObjWrapper>();
        //Set the wrapper object with all existing accounts
        if(lstAccByVP != null && !lstAccByVP.isEmpty()){
            for(AccountByVisitPlan__c objAccXVp : lstAccByVP){
                mapExistAccount.put(objAccXVp.Account__c, objAccXVp);
                ObjWrapper objWrp = new ObjWrapper();
                objWrp.objVPxAcc = objAccXVp;
                objWrp.blnActive = true;
                lstWrapper.add(objWrp);
            }
        }
    }

    public PageReference selectAllItems(){
        Integer intSecuency = 1;
        if(lstWrapper != null){
            for(ObjWrapper objWrp : lstWrapper) {
                if (Test.isRunningTest()) { blnSelectAll = true; }
                objWrp.blnActive = blnSelectAll;
                objWrp.objVPxAcc.Sequence__c = blnSelectAll ? intSecuency : null;
                intSecuency += 1;
            }
        }
        return null;
    }    
    public PageReference refreshData(){
        System.debug('\nlstWrapper==>'+lstWrapper);
        return null;
    }
    
    public PageReference cancel(){
        return new PageReference('/'+(strIdRoute!=null?strIdRoute:'home/home.jsp?sdtd=1'));
    }
    
    public PageReference saveVisitPlan(){
        Savepoint sp = Database.setSavepoint();
        if(blnErrorAL)
            return null;
        //Validate that ExecutionDate__c and EffectiveDate__c fields are not null
        system.debug('objVisitPlan.ExecutionDate__c:' + objVisitPlan.ExecutionDate__c);
        system.debug('objVisitPlan.EffectiveDate__c:' + objVisitPlan.EffectiveDate__c);
        
        
        if(objVisitPlan.ExecutionDate__c == null || objVisitPlan.EffectiveDate__c == null || objVisitPlan.ExecutionDate__c > objVisitPlan.EffectiveDate__c || objVisitPlan.EffectiveDate__c < Date.today()){
            blnErrorBI = true;
            strError = Label.ErrorStartEndDateVisitPlan;
            return null;
        }
        
        // Daniel Rosales
        // Added validation for no Accounts configured drs@avx
        // 2019-07-12
        if(lstWrapper == NULL){ 
            blnErrorBI = true;
            strError = Label.VisitPlanNoAccountByVisitPlan;
            return null;
        }
        
        // Daniel Rosales
        // Added validation when no frecuency configured
        // 2019-07-12
        if (!objVisitPlan.Monday__c && !objVisitPlan.Tuesday__c && !objVisitPlan.Wednesday__c && !objVisitPlan.Thursday__c && !objVisitPlan.Friday__c && !objVisitPlan.Saturday__c && !objVisitPlan.Sunday__c ){
            blnErrorBI = true;
            strError = Label.VisitPlanNoAccountByVisitPlan;
            return null;
        }
        
        
        //Upsert the visit plan
        Database.UpsertResult ur = Database.upsert(objVisitPlan, VisitPlan__c.Id, false);
        if(!ur.isSuccess()){
            blnErrorAL = true;
            strError = '1 - '+ur.getErrors()[0].getMessage();
            Database.rollback(sp);
            return null;
        }
            
        //List with new accounts to the visit plan
        list<AccountByVisitPlan__c> lstAccByVP = new list<AccountByVisitPlan__c>();
        //List with visit plan accounts that will be deleted
        list<AccountByVisitPlan__c> lstAccByVPToDel = new list<AccountByVisitPlan__c>();
        
       //Set the lists that wil be insert and delete
        map<String, ObjWrapper> mapAuxAcc = new map<String, ObjWrapper>();
        map<Integer, String> mapPosXAcc = new map<Integer, String>();
        Integer intPos = 0;
        for(ObjWrapper objWrp : lstWrapper) {
            objWrp.strError = '';
            if(objWrp.blnActive) {
                objWrp.objVPxAcc.VisitPlan__c = objVisitPlan.Id;
                lstAccByVP.add(objWrp.objVPxAcc);
                mapPosXAcc.put(intPos, objWrp.objVPxAcc.Account__c);
                intPos ++;
                mapAuxAcc.put(objWrp.objVPxAcc.Account__c, objWrp);
            }
            else {
                if(objWrp.objVPxAcc.Id != null) {
                   lstAccByVPToDel.add(objWrp.objVPxAcc);
                   mapAuxAcc.put(objWrp.objVPxAcc.Account__c, objWrp);
                }
            }
        }
        
        for(String strAcc : mapExistAccount.keySet()) {
            if(!mapAuxAcc.containsKey(strAcc))
                lstAccByVPToDel.add(mapExistAccount.get(strAcc));                
        }
        
        //delete the accounts that was not selected
        if(!lstAccByVPToDel.isEmpty()) {
            list<Database.DeleteResult> lstDR = Database.delete(lstAccByVPToDel,false);
            for(Database.DeleteResult dr : lstDR){
                if(!dr.isSuccess()){
                    blnErrorAL = true;
                    strError = '2 - '+dr.getErrors()[0].getMessage();
                    objVisitPlan.Id = strIdVisitPlan;
                    Database.rollback(sp);
                    return null;
                }
            }
        }
        
        System.debug('\nLista de cuentas x plan ==>'+lstAccByVP);
        
        //Upsert accounts that was selected in the page
        if(!lstAccByVP.isEmpty()) {
            Boolean blnAuxError = false;
                        
            list<Database.UpsertResult> lstUR = Database.upsert(lstAccByVP, AccountByVisitPlan__c.Id, false);
            System.debug('\nlstUR==>'+lstUR);
            Integer intPosAux = 0;
            for(Database.UpsertResult objUR : lstUR){
                System.debug('\nintPos==>'+intPos);
                System.debug('\nobjUR==>'+objUR);
                
                if(!objUR.isSuccess()){
                    mapAuxAcc.get(mapPosXAcc.get(intPosAux)).strError = '3 - '+objUR.getErrors()[0].getMessage();
                    blnAuxError = true;
                }
                intPosAux++;
            }
            if(blnAuxError){
                Database.rollback(sp);
                deleteCrossReference();
                return null;
            }
            //Sort list by sequence
            TriggerExecutionControl_cls.setAlreadyDone('AccountByVisitPlan_tgr','BeforeUpdate');
            AccountByVisitPlanOperations_cls.updateSortListBySequence(null, new set<String>{objVisitPlan.Id});
        }
        
        return new PageReference('/'+objVisitPlan.Id);
    }
    
    public void deleteCrossReference(){
        objVisitPlan.Id = strIdVisitPlan;
        if(strIdVisitPlan==null){
            for(ObjWrapper objWrp : lstWrapper)
                objWrp.objVPxAcc.Id = null;
        }
    }
    
    public class ObjWrapper{
        public Boolean                      blnActive   {get; set;}
        public AccountByVisitPlan__c        objVPxAcc   {get; set;}
        public String                       strError    {get; set;}
        public String                       strSAPId    {get; set;}
    }
}