/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: MassiveTourDeletion_tst.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 11/01/2019        Jose Luis Vargas          test methods creation
 */

@isTest
public class MassiveTourDeletion_tst {
    
    /* * SetUp the data of the tests
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @testsetup
    private static void createDta(){
      //Wiit  Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_tst());
       //Wiit insertCustomSetting();
       //Wiit list<ISSM_PriceEngineConfigWS__c> lstConf = new list<ISSM_PriceEngineConfigWS__c>();
        
        List<ONTAP__Tour__c> tours = new List<ONTAP__Tour__c>();
        List<Event> objEvent_lst = new List<Event>();
        Id rt = [SELECT Id FROM RecordType WHERE DeveloperName='Telesales'][0].Id;
        Id rtBDR = [SELECT Id FROM RecordType WHERE DeveloperName='BDR'][0].Id;
        Id rtPres = [SELECT Id FROM RecordType WHERE DeveloperName='Presales'][0].Id;
        Date dtNextVisit = System.today();
        for(Integer i=0;i<20;i++){
            tours.add(new ONTAP__Tour__c(ONTAP__TourId__c='xx', RecordTypeId=rt));
        }
        for(Integer i=0;i<20;i++){
            tours.add(new ONTAP__Tour__c(ONTAP__TourId__c='xx', RecordTypeId=rtBDR, ONTAP__TourStatus__c='Created', TourSubStatus__c='Tour creado en SFDC'));
        }
        for(Integer i=0;i<10;i++){
            tours.add(new ONTAP__Tour__c(ONTAP__TourId__c='xx', RecordTypeId=rtPres, ONTAP__TourStatus__c='Created', TourSubStatus__c='Tour creado en SFDC'));
        }
        insert tours;
        account acc = new account(name='test');
        insert acc;
        
        Event objEvent = new Event();
        ONCALL__Call__c objCall = new ONCALL__Call__c();
        List<ONCALL__Call__c> objCall_lst = new List<ONCALL__Call__c>();
        for(ONTAP__Tour__c tr : tours){
            if(tr.recordtypeid==rtBDR || tr.recordtypeid==rtPres){
                for(Integer i = 0;i<50;i++){
                    objEvent = new Event();
                    objEvent.WhatId                          =   acc.Id;
                    objEvent.Subject                         =   'xxxx';
                    objEvent.StartDateTime                   =   DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(),16, 00, 0);
                    objEvent.EndDateTime                     =   DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(), 18, 00, 0);
                    objEvent.Sequence__c                     =   1;
                    objEvent.VisitList__c                    =   tr.Id;
                    objEvent.CustomerId__c                   =  '0888888888';
                    objEvent_lst.add(objEvent);
                    objEvent = new Event();
                }
            }else{
                    for(Integer i = 0;i<50;i++){
                        objCall.CallList__c = tr.Id;
                        objCall.ONCALL__POC__c = acc.Id;
                        objCall_lst.add(objCall);
                        objCall = new ONCALL__Call__c();
                    }
            }
            
        }
        upsert objEvent_lst;
        insert objCall_lst;
    }
      
    /* * Test method to show when a is selected
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest private static void whenNoTourIsSelected(){
        List<ONTAP__Tour__c> tours = new List<ONTAP__Tour__c>();
        Test.setCurrentPage(Page.MassiveTourDeletion);
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(tours);
        MassiveTourDeletion_ctr tour = new MassiveTourDeletion_ctr(controller);
        String s = tour.jobRunningMessage;
        s = tour.retURL;
        s = tour.validationMessage;
        tour.executeBatch();
    }
    
    /* * Test method to show when more than 20 tours are selected
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest private static void whenMoreThan20TourIsSelected(){
        List<ONTAP__Tour__c> tours = new List<ONTAP__Tour__c>();
        Id rt = [SELECT Id FROM RecordType WHERE DeveloperName='Telesales'][0].Id;
        for(Integer i=0;i<21;i++){
            tours.add(new ONTAP__Tour__c(ONTAP__TourId__c='xx', RecordTypeId=rt));
        }
        insert tours;
        Test.setCurrentPage(Page.MassiveTourDeletion);
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(tours);
        controller.setSelected(tours);
        System.assertEquals(21, controller.getSelected().size());
        MassiveTourDeletion_ctr tour = new MassiveTourDeletion_ctr(controller);
        tour.executeBatch();
    }
    
    /* * Test method to deleted in a massive way, records with different RT
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest private static void massiveDeleteRecordsWithDifferentRT(){        
        List<ONTAP__Tour__c> tours = [SELECT Id FROM ONTAP__Tour__c limit 20];
        Test.setCurrentPage(Page.MassiveTourDeletion);
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(tours);
        controller.setSelected(tours);
        MassiveTourDeletion_ctr tour = new MassiveTourDeletion_ctr(controller);
        tour.executeBatch();
        tour.updateStatus();
    }
    
    /* * Test method to deleted in a massive way, records with different RT Telesales
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest private static void massiveDeleteRecordsWithRTTelesales(){        
        List<ONTAP__Tour__c> tours = [SELECT Id FROM ONTAP__Tour__c where recordtype.developername='Telesales' limit 20];
        Test.setCurrentPage(Page.MassiveTourDeletion);
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(tours);
        controller.setSelected(tours);
        MassiveTourDeletion_ctr tour = new MassiveTourDeletion_ctr(controller);
        tour.executeBatch();
        tour.updateStatus();
    }
    
    /* * Test method to deleted in a massive way, record with different RT/BDR
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest private static void massiveDeleteRecordsWithRTBDR(){        
        List<ONTAP__Tour__c> tours = [SELECT Id FROM ONTAP__Tour__c where recordtype.developername='BDR' limit 20];
        Test.setCurrentPage(Page.MassiveTourDeletion);
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(tours);
        controller.setSelected(tours);
        MassiveTourDeletion_ctr tour = new MassiveTourDeletion_ctr(controller);
        tour.executeBatch();
        Decimal prog = tour.progress;
        
        tour.updateStatus();
    }
    
    /* * Test method to deleted in a massive way, record with different RT Presales
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest private static void massiveDeleteRecordsWithRTPresales(){        
        List<ONTAP__Tour__c> tours = [SELECT Id FROM ONTAP__Tour__c where recordtype.developername='Presales' limit 20];
        Test.setCurrentPage(Page.MassiveTourDeletion);
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(tours);
        controller.setSelected(tours);
        MassiveTourDeletion_ctr tour = new MassiveTourDeletion_ctr(controller);
        tour.executeBatch();
        Decimal prog = tour.progress;
        
        tour.updateStatus();
    }
    
   /*Wiit public static void insertCustomSetting(){
        SyncHerokuParams__c objConf = new SyncHerokuParams__c();
        objConf.Name = 'SyncToursEvents';
        objConf.StartTime__c = 0;
        objConf.EndTime__c = 24;
        objConf.IsActive__c = true;
        objConf.LastModifyDate__c = DateTime.now().addDays(-30);
        objConf.RecordTypeIds__c = 'Presales';
        objConf.RunFrequency__c = 60;
        insert objConf;
        
        list<ISSM_PriceEngineConfigWS__c> lstConf = new list<ISSM_PriceEngineConfigWS__c>();
        ISSM_PriceEngineConfigWS__c objConf1 = new ISSM_PriceEngineConfigWS__c();
        objConf1.Name = 'DeleteHerokuTours';
        objConf1.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf1.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/deletetours';
        objConf1.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf1.ISSM_Method__c = 'DELETE';
        lstConf.add(objConf1);
        
        ISSM_PriceEngineConfigWS__c objConf2 = new ISSM_PriceEngineConfigWS__c();
        objConf2.Name = 'DeleteHerokuEvents';
        objConf2.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf2.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/deleteevents';
        objConf2.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf2.ISSM_Method__c = 'DELETE';
        lstConf.add(objConf2);
        
        ISSM_PriceEngineConfigWS__c objConf3 = new ISSM_PriceEngineConfigWS__c();
        objConf3.Name = 'GetHerokuTours';
        objConf3.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf3.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/gettours';
        objConf3.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf3.ISSM_Method__c = 'POST';
        lstConf.add(objConf3);
        
        ISSM_PriceEngineConfigWS__c objConf4 = new ISSM_PriceEngineConfigWS__c();
        objConf4.Name = 'GetHerokuEvents';
        objConf4.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf4.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/getevents';
        objConf4.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf4.ISSM_Method__c = 'POST';
        lstConf.add(objConf4);
        
        ISSM_PriceEngineConfigWS__c objConf5 = new ISSM_PriceEngineConfigWS__c();
        objConf5.Name = 'InsertHerokuTours';
        objConf5.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf5.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/inserttours';
        objConf5.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf5.ISSM_Method__c = 'POST';
        lstConf.add(objConf5);
        
        ISSM_PriceEngineConfigWS__c objConf6 = new ISSM_PriceEngineConfigWS__c();
        objConf6.Name = 'InsertHerokuEvents';
        objConf6.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf6.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/insertevents';
        objConf6.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf6.ISSM_Method__c = 'POST';
        lstConf.add(objConf6);
        
        ISSM_PriceEngineConfigWS__c objConf7 = new ISSM_PriceEngineConfigWS__c();
        objConf7.Name = 'UpdateHerokuTours';
        objConf7.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf7.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/updatetours';
        objConf7.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf7.ISSM_Method__c = 'PUT';
        lstConf.add(objConf7);
        
        ISSM_PriceEngineConfigWS__c objConf8 = new ISSM_PriceEngineConfigWS__c();
        objConf8.Name = 'UpdateHerokuEvents';
        objConf8.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf8.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/updateevents';
        objConf8.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf8.ISSM_Method__c = 'PUT';
        lstConf.add(objConf8);
        
        insert lstConf;
    }*/
}