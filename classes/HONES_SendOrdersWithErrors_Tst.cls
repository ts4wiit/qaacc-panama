/* ----------------------------------------------------------------------------
 * AB InBev :: OnCall
 * ----------------------------------------------------------------------------
 * Clase: HONES_SendOrdersWithErrors_Tst.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 22/01/2019           Oscar Garcia        Creación de métodos para clase prueba de HONES_SendOrdersWithErrors 
 */

@isTest
private class HONES_SendOrdersWithErrors_Tst {
    @isTest
    static void test_HONES_SendOrdersWithErrors_Tst(){
        //creating data
        ONTAP__Order__c order_inSAP = new ONTAP__Order__c( 
            ONCALL__SAP_Order_Number__c = '2012365', 
            ONCALL__SAP_Order_Response__c = 'S - Alles gut'
        );
        
        insert order_inSAP;
        
        ONTAP__Order__c order_notSAP = new ONTAP__Order__c();
         
        insert order_notSAP;
        system.debug('No sap : '+order_notSAP.Id);
        
        String response_inSAP='';
        String response_notSAP='';
        
        Test.setMock(HttpCalloutMock.class, new MockHTTPResponseGeneratorSendOrder());
        test.startTest();
        response_inSAP = HONES_SendOrdersWithErrors.sendOrder(order_inSAP.Id);
        response_inSAP = HONES_SendOrdersWithErrors.sendOrder(order_notSAP.Id);
        test.stopTest();  
    }
}