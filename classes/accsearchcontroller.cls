/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: FlexibleDataController.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 18/12/2018     Luis Arturo Parra        Creation of methods.
*/


public with sharing class accsearchcontroller { 
    /**
* Methods getters and setters for entity DealsTestJson
* Created By: Luis Parra
* Suport contact: g.martinez.g.martinez.cabral@accenture.com
* @param void
* @return void
*/
    public list <account> acc {get;set;} 
    public string searchstring {get;set;} 
    public accsearchcontroller( ) { 
    } 
    public void search(){ 
        /*search all the account in the class
/* Created By: Luis Parra
* Suport contact: g.martinez.g.martinez.cabral@accenture.com
* @param void
* @return void
*/
        string searchquery='select name,id from account where name like \'%'+searchstring+'%\' Limit 20'; 
        acc= Database.query(searchquery); 
    } 
    
    /*clean the account in the class
/* Created By: Luis Parra
* Suport contact: g.martinez.g.martinez.cabral@accenture.com
* @param void
* @return void
*/
    public void clear(){ 
        acc.clear(); 
    } 
}