/*******************************************************************************
Developed by: TS4
Author: Daniel Hernández
Proyect: HONES
Description: Apex class to test methods for HONES_ONCALL_ProductExclusionsController, 
		     HONES_ONCALL_MasiveProductExclusion & HONES_ONCALL_MasiveProductExclusionQueue functionality

------ ---------- -------------------------- -----------------------------------
No.    Date       Autor                      Description
------ ---------- -------------------------- -----------------------------------
1.0    09/12/2019 Daniel Hernández           Created Class
*******************************************************************************/

@isTest
public class HONES_ONCALL_ProductExclusions_Test {
    
    @testSetup static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' OR Name = 'Administrador del sistema'];
        String strEmail = 'testuser' + '@ab-inbev.com';
        User admin = new User(Alias             = 'testuser', Email                   = strEmail,
                              EmailEncodingKey  = 'UTF-8',    LastName                = 'Lastname',
                              LanguageLocaleKey = 'es_MX',    LocaleSidKey            = 'es_MX',
                              ProfileId         = p.Id,       TimeZoneSidKey          = 'America/Mexico_City',
                              UserName          = strEmail,   ONTAP__Country_Alias__c = 'SV',
                              ManagerId         = UserInfo.getUserId());
        insert admin;
        Account cta = new Account(Name = 'cliente de prueba', ONTAP__LegalName__c = 'Legal name', 
                                  V360_BASISNum__c           = '00001', ONTAP__SAP_Number__c = '0034567891', 
                                  IREP_DistributionCenter__c = 'DS01',  ONCALL__KATR9__c     = 'S00', 
                                  ONCALL__KATR3__c           = '00');
        insert cta;
        ONTAP__Product__c prod = new ONTAP__Product__c(ONTAP__ProductCode__c='321', ONTAP__MaterialProduct__c='pilsener');
        insert prod;
        ONTAP__Product__c prod2 = new ONTAP__Product__c(ONTAP__ProductCode__c='172', ONTAP__MaterialProduct__c='Coca-cola');
        insert prod2;
        HONES_ProductPerClient__c exclusion = new HONES_ProductPerClient__c(HONES_RelatedClient__c=cta.Id, HONES_RelatedProduct__c = prod.Id, 
                                                                            SAP_SKU_ID__c='321', SAP_Account_ID__c = '111', HONES_MassiveKey__c='--Todos--|--Todos--|--Todos--');
        insert exclusion;
    }
    
    @isTest static void getInfo() {
        HONES_ONCALL_ProductExclusionsController.getClient('0034567891');
        HONES_ONCALL_ProductExclusionsController.getCountClients('--Todos--','--Todos--','--Todos--');
        HONES_ONCALL_ProductExclusionsController.getCountClients('DS01','S00','00');
        HONES_ONCALL_ProductExclusionsController.getMassiveProductExcluded('DS01','S00','00');
        HONES_ONCALL_ProductExclusionsController.getProducts('%%');
        HONES_ONCALL_ProductExclusionsController.getProducts('321');
        HONES_ONCALL_ProductExclusionsController.getPickListValuesDistributionCenter();
        HONES_ONCALL_ProductExclusionsController.getPickListValuesSubchannel();
        HONES_ONCALL_ProductExclusionsController.getPickListValuesKATR3();
    }
    
    @isTest static void saveExclusions() {
        List<Account> ctas = [SELECT Id, Name, ONTAP__LegalName__c, V360_BASISNum__c, ONTAP__SAP_Number__c, IREP_DistributionCenter__c, ONCALL__KATR9__c, ONCALL__KATR3__c 
                        FROM Account WHERE ONTAP__SAP_Number__c = '0034567891' LIMIT 1];
        Account cta = ctas[0];
        HONES_ONCALL_ProductExclusionsController.getProductExcludedPerClient(cta.Id);
        List<ONTAP__Product__c> prods = [SELECT Id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c FROM ONTAP__Product__c WHERE ONTAP__ProductCode__c != null];
        List<Id> idProds = new List<Id>();
        for(ONTAP__Product__c prod: prods){
            idProds.add(prod.Id);
        }
        HONES_ONCALL_ProductExclusionsController.saveExclusionPerClient(cta, idProds);
        HONES_ONCALL_ProductExclusionsController.saveMassiveExclusion('--Todos--|--Todos--|--Todos--',idProds, 1, 1);
        HONES_ONCALL_ProductExclusionsController.saveMassiveExclusion('DS01|S00|00',idProds, 1, 1);
        
        Map<Id, ONTAP__Product__c> selectedProducts = new Map <Id, ONTAP__Product__c>([SELECT Id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c FROM ONTAP__Product__c WHERE ONTAP__ProductCode__c!=null]);
        HONES_ONCALL_MasiveProductExclusion exclude = new HONES_ONCALL_MasiveProductExclusion('--Todos--|--Todos--|--Todos--', 'SV', selectedProducts);
        exclude.execute(null,ctas);
        exclude.Finish(null);
    }
    
    @isTest static void massiveExclusions() {
        List<Account> ctas = [SELECT Id, Name, ONTAP__LegalName__c, V360_BASISNum__c, ONTAP__SAP_Number__c, IREP_DistributionCenter__c, ONCALL__KATR9__c, ONCALL__KATR3__c 
                        FROM Account WHERE ONTAP__SAP_Number__c = '0034567891' LIMIT 1];
        List<ONTAP__Product__c> prods = [SELECT Id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c FROM ONTAP__Product__c WHERE ONTAP__ProductCode__c != null];
        
        Map<Id, ONTAP__Product__c> selectedProducts = new Map <Id, ONTAP__Product__c>([SELECT Id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c FROM ONTAP__Product__c WHERE ONTAP__ProductCode__c!=null]);
        HONES_ONCALL_MasiveProductExclusion exclude = new HONES_ONCALL_MasiveProductExclusion('--Todos--|--Todos--|--Todos--', 'SV', selectedProducts);
        exclude.execute(null,ctas);
        exclude.Finish(null);
    }

}