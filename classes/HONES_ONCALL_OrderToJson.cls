/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_OrderToJson.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/01/2019           Heron Zurita          Creation of methods.
*/
public class HONES_ONCALL_OrderToJson {
    
    /**
    * Methods getters and setters for entity HONES_ONCALL_OrderToJson
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    public String Ids{get; set;}
    public String Assigned_Agent{get; set;}
    public String PaymentMethod{get; set;}
    public String DeliveryDate{get; set;}
    public String CustomerSAPID{get; set;}
    public String SalesOgId{get; set;}
    public String DocumentationType{get; set;}
    
    //public String idsItems{get; set;}
	//public String ProductId{get; set;}
    //public String OnCall_Quantity{get; set;}
    
    
    public List<ONTAP__Order_Item__c> Items {get; set;}
	
}