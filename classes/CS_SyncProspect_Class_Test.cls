/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_SyncProspect_Class_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 29/04/2019     Jose Luis Vargas          Creation of test class.
*/

@isTest
public class CS_SyncProspect_Class_Test 
{
    /**
    * method to test SyncProspect method
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest
    static void test_SyncProspect()
    {
        Id RecordTypeProspect = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
        Id idRecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.ACCOUNT_RECORDTYPE_NAME).getRecordTypeId();
        
        CS_SyncProspect_Class oSyncProspect = new CS_SyncProspect_Class();
        ONTAP__Case_Force__c oCaseForce = new ONTAP__Case_Force__c();
        ONTAP__Case_Force__c oCaseForceDelete = new ONTAP__Case_Force__c();
        
        CS_RO_Cooler_Settings__c oSetting = new CS_RO_Cooler_Settings__c();
        oSetting.CS_RT_RO_Generate_Client__c = 'CS_RT_RO_Generate_Client';
        insert oSetting;
        
        
        /* Se inserta una cuenta */
        Account oAccount = new Account( Name='AccountTest', HONES_IdentificationNumber__c = '90181891', ONTAP__Latitude__c= '25.65653', ONTAP__Longitude__c='-100.39177', RecordTypeId = idRecordTypeAccount);
        Insert oAccount;
        
        /* Se inserta el prospecto */
        Account oProspect = new Account();
        oProspect.Name = 'Prospect Test';
        oProspect.CS_Last_Name__c = 'Test';
        oProspect.CS_Second_Last_Name__c = 'Test';
        oProspect.ONTAP__LegalName__c = 'Tiendas Unidas';
        oProspect.ISSM_ProspectType__c = 'PN';
        oProspect.CS_Tipo_Identificacion__c = '00';
        oProspect.HONES_IdentificationNumber__c = '90181891';
        oProspect.ONTAP__Mobile_Phone__c = '5549001010';
        oProspect.CS_Tipo_Via__c = 'CL';
        oProspect.ONTAP__Street__c = 'Romero';
        oProspect.ONTAP__Street_Number__c = '12';
        oProspect.CS_Tipo_Poblacion__c = 'Urbano';
        oProspect.CS_Poblacion__c = '06 - Choluteca';
        oProspect.CS_Distrito__c = 'ALUBAREN';
        oProspect.V360_ReferenceClient__c = oAccount.Id;
        oProspect.RecordTypeId = RecordTypeProspect;
        oProspect.CS_Prospect_Create_Date__c = Date.today();
        Insert oProspect;
        
        /* Se inserta el prospecto para borrar */
        Account oProspectDelete = new Account();
        oProspectDelete.Name = 'Prospect Test';
        oProspectDelete.CS_Last_Name__c = 'Test';
        oProspectDelete.CS_Second_Last_Name__c = 'Test';
        oProspectDelete.ONTAP__LegalName__c = 'Tiendas Unidas';
        oProspectDelete.ISSM_ProspectType__c = 'PN';
        oProspectDelete.CS_Tipo_Identificacion__c = '00';
        oProspectDelete.HONES_IdentificationNumber__c = '9018189108';
        oProspectDelete.ONTAP__Mobile_Phone__c = '5549001010';
        oProspectDelete.CS_Tipo_Via__c = 'CL';
        oProspectDelete.ONTAP__Street__c = 'Romero';
        oProspectDelete.ONTAP__Street_Number__c = '12';
        oProspectDelete.CS_Tipo_Poblacion__c = 'Urbano';
        oProspectDelete.CS_Poblacion__c = '06 - Choluteca';
        oProspectDelete.CS_Distrito__c = 'ALUBAREN';
        oProspectDelete.V360_ReferenceClient__c = oAccount.Id;
        oProspectDelete.RecordTypeId = RecordTypeProspect;
        oProspectDelete.CS_Prospect_Create_Date__c = Date.today().addDays(-60);
        Insert oProspectDelete;
                        
        oCaseForce.ONTAP__Account__c =  oProspect.Id;
        Insert oCaseForce;
        
        /* Se inserta el caso asociado al prospecto */
        Case oCaseProspect = new Case();
        oCaseProspect.AccountId = oProspect.Id;
        oCaseProspect.ISSM_CaseForceNumber__c = oCaseForce.Id;
        Insert oCaseProspect;
        
        oCaseForceDelete.ONTAP__Account__c = oProspectDelete.Id;
        Insert oCaseForceDelete;
        
        /* Se inserta caso para ser borrado */
        Case oCaseProspectDelete = new Case();
        oCaseProspectDelete.AccountId = oProspectDelete.Id;
        oCaseProspectDelete.ISSM_CaseForceNumber__c = oCaseForceDelete.Id;
        Insert oCaseProspectDelete;

        
        oSyncProspect.SyncProspect();
    }
}