/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASE_USER_ESCALATION_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 17/01/2019     Carlos Leal Betrán       Creation of methods.
 */
@isTest
public class CS_Case_User_Escalation_Class_Test 
{
    
    /**
    * Test method for assign the escalation for user assignation
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void Test_AssignEscalationUser()
    {
        CS_Case_User_Escalation_Class testEscalation= new CS_Case_User_Escalation_Class();
        List<Profile> prof = [Select Name,Id from Profile];
        User ManagerEmpt = new User();
        User ManagerEmpt1 = new User();
        User ManagerEmpt2 = new User();
        User ManagerEmpt3 = new User();
        User ManagerEmpt4 = new User();
        User ManagerEmpt5 = new User();
        Case caseNew = new Case();
        insert caseNew;
        
        String idCase= caseNew.Id;
        ManagerEmpt.Id= ManagerEmpt1.Id;
        ManagerEmpt1.Id= ManagerEmpt2.Id;
        ManagerEmpt2.Id= ManagerEmpt3.Id;
        ManagerEmpt3.Id= ManagerEmpt4.Id;
        ManagerEmpt4.Id= ManagerEmpt5.Id;
        
        User CaseOwner = new User(Email = 'user12345@email.com', Username = 'user12345@email.com', LastName = 'Test', Alias = 'Test', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        CaseOwner.ManagerId = ManagerEmpt.Id;
        insert CaseOwner;   
        
        testEscalation.AssignEscalationUser(idCase, null);
        
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
       
        User ManagerN1 = new User(Email = 'user1230@email.com', Username = 'user1230@email.com', LastName = 'Test0', Alias = 'Test0', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ManagerN1;
        
        CaseOwner.ManagerId =ManagerEmpt5.Id;
        update CaseOwner;
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
        
        CaseOwner.ManagerId = ManagerN1.Id;
        update CaseOwner; 
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);  
        
        User ManagerN2 = new User(Email = 'user1231@email.com', Username = 'user1231@email.com', LastName = 'Test1', Alias = 'Test1', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ManagerN2;
        CaseOwner.ManagerId =ManagerEmpt4.Id;
        //update ManagerN1;
        update CaseOwner;
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
        
        ManagerN1.ManagerId = ManagerN2.Id;
        update ManagerN1;
        CaseOwner.ManagerId = ManagerN1.Id;
        update CaseOwner; 
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
        
        User ManagerN3 = new User(Email = 'user1232@email.com', Username = 'user1232@email.com', LastName = 'Test2', Alias = 'Test2', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ManagerN3;
        CaseOwner.ManagerId =ManagerEmpt3.Id;
        //update ManagerN2;
        update CaseOwner;
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
        ManagerN2.ManagerId = ManagerN3.Id;
        update ManagerN2;
        CaseOwner.ManagerId = ManagerN1.Id;
        update CaseOwner; 
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
        
        User ManagerN4 = new User(Email = 'user1233@email.com', Username = 'user1233@email.com', LastName = 'Test3', Alias = 'Test3', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ManagerN4;
        CaseOwner.ManagerId =ManagerEmpt2.Id;
        //update ManagerN3;
        update CaseOwner;
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
        
        ManagerN3.ManagerId = ManagerN4.Id;
        update ManagerN3;
        CaseOwner.ManagerId = ManagerN1.Id;
        update CaseOwner; 
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
            
        User ManagerN5 = new User(Email = 'user1234@email.com', Username = 'user1234@email.com', LastName = 'Test4', Alias = 'Test4', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert ManagerN5;
        CaseOwner.ManagerId =ManagerEmpt1.Id;
        update ManagerN4;
        //update CaseOwner;
        testEscalation.AssignEscalationUser(idCase, ManagerN4.Id);
        
        ManagerN4.ManagerId = ManagerN5.Id;
        update ManagerN4;
        CaseOwner.ManagerId = ManagerN1.Id;
        update CaseOwner; 
        testEscalation.AssignEscalationUser(idCase, CaseOwner.Id);
    }
    
    /**
    * Test method for assign the escalation for user assignation (trigger)
    * Created By: gabriel.e.garcia@accenture.com
    * @param void
    * @return void
    */
    @isTest static void Test_AssignEscalationUserTrigger()
    {        
        Set<Id> setUser = new Set<Id>();
        List<Profile> prof = [Select Name,Id from Profile];                
        
        Case caseNew = new Case();
        insert caseNew;
        String idCase= caseNew.Id;                 
        
        User CaseOwner = new User(Email = 'user12345@email.com', Username = 'user1234test@email.com', LastName = 'Test1', Alias = 'Test1', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');        
        insert CaseOwner;   
        
        CS_Case_User_Escalation_Class testEscalation = new CS_Case_User_Escalation_Class(setUser);
        testEscalation.AssignEscalationUserTrigger(idCase, CaseOwner.Id);
        
        setUser.add(CaseOwner.Id);
        
        CS_Case_User_Escalation_Class testEscalation1= new CS_Case_User_Escalation_Class(setUser);
        testEscalation1.AssignEscalationUserTrigger(idCase, CaseOwner.Id);
                
        User Manager5 = new User(Email = 'Manager5@email.com', Username = 'Manager5test@email.com', LastName = 'Manager5', Alias = 'M5', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es');
        insert Manager5;
        CaseOwner.ManagerId = Manager5.Id;
        update CaseOwner;
        setUser.add(CaseOwner.Id);
        
        CS_Case_User_Escalation_Class testEscalation2 = new CS_Case_User_Escalation_Class(setUser);
        testEscalation2.AssignEscalationUserTrigger(idCase, CaseOwner.Id);
        
        User Manager4 = new User(Email = 'Manager4@email.com', Username = 'Manager4test@email.com', LastName = 'Manager4', Alias = 'M4', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es', ManagerId = Manager5.Id);
        insert Manager4;
        
        CaseOwner.ManagerId = Manager4.Id;
        update CaseOwner;
        setUser.add(CaseOwner.Id);
        
        CS_Case_User_Escalation_Class testEscalation3 = new CS_Case_User_Escalation_Class(setUser);
        testEscalation3.AssignEscalationUserTrigger(idCase, CaseOwner.Id);
        
        User Manager3 = new User(Email = 'Manager3@email.com', Username = 'Manager3test@email.com', LastName = 'Manager3', Alias = 'M3', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es', ManagerId = Manager4.Id);
        insert Manager3;
        
        CaseOwner.ManagerId = Manager3.Id;
        update CaseOwner;
        setUser.add(CaseOwner.Id);
        
        CS_Case_User_Escalation_Class testEscalation4 = new CS_Case_User_Escalation_Class(setUser);
        testEscalation4.AssignEscalationUserTrigger(idCase, CaseOwner.Id);
        
        User Manager2 = new User(Email = 'Manager2@email.com', Username = 'Manager2test@email.com', LastName = 'Manager2', Alias = 'M2', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es', ManagerId = Manager3.Id);
        insert Manager2;
        
        CaseOwner.ManagerId = Manager2.Id;
        update CaseOwner;
        setUser.add(CaseOwner.Id);
        
        CS_Case_User_Escalation_Class testEscalation5 = new CS_Case_User_Escalation_Class(setUser);
        testEscalation5.AssignEscalationUserTrigger(idCase, CaseOwner.Id);
        
        User Manager1 = new User(Email = 'Manager1@email.com', Username = 'Manager1test@email.com', LastName = 'Manager1', Alias = 'M1', TimeZoneSidKey = 'America/Bogota',
                           LocaleSidKey = 'es', EmailEncodingKey = 'ISO-8859-1', ProfileId = prof[0].id,LanguageLocaleKey = 'es', ManagerId = Manager2.Id);
        insert Manager1;
        
        CaseOwner.ManagerId = Manager1.Id;
        update CaseOwner;
        setUser.add(CaseOwner.Id);
        
        CS_Case_User_Escalation_Class testEscalation6 = new CS_Case_User_Escalation_Class(setUser);
        testEscalation6.AssignEscalationUserTrigger(idCase, CaseOwner.Id);
                      
    }
}