public class OnCall_ExclSKUTriggerHandler extends TriggerHandlerCustom{
	private Map<Id, HONES_ProductPerClient__c> newMap;
    private Map<Id, HONES_ProductPerClient__c> oldMap;
    private List<HONES_ProductPerClient__c> newList;
    private List<HONES_ProductPerClient__c> oldList;
    
    public OnCall_ExclSKUTriggerHandler() {
        this.newMap = (Map<Id, HONES_ProductPerClient__c>) Trigger.newMap;
        this.oldMap = (Map<Id, HONES_ProductPerClient__c>) Trigger.oldMap;
        this.newList = (List<HONES_ProductPerClient__c>) Trigger.new;
        this.oldList = (List<HONES_ProductPerClient__c>) Trigger.old;
    }
    
    public override void beforeInsert(){
        /*
         * No need for code if accounts are linked using an External Id-Unique
         * To relate the account use instead ONTAP__Codigo_del_cliente__c
         * ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
         * drs@avx
         */
        //relateAccountsToExclSKUData(this.newlist);
        //relateProductsToExclSKUData(this.newlist);
    }
    
    public override void beforeUpdate(){
        /*
         * No need for code if accounts are linked using an External Id-Unique
         * To relate the account use instead ONTAP__Codigo_del_cliente__c
         * ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
         * drs@avx
         */
        //relateAccountsToExclSKUData(this.newlist);
        //relateProductsToExclSKUData(this.newlist);
    }
    
    /*
 	* No need for code if accounts are linked using an External Id-Unique
 	* To relate the account use instead ONTAP__Codigo_del_cliente__c
 	* ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
 	* drs@avx
 	*/
    /*
	public static void relateProductsToExclSKUData(List<HONES_ProductPerClient__c> newlist){
    	Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,ONTAP__Product__c> prdReference = new Map<String,ONTAP__Product__c>();
        
        for (HONES_ProductPerClient__c accExclSKU:newlist){
            if(accExclSKU.SAP_SKU_ID__c!=NULL){
                SAPNumsToRelate.add(accExclSKU.SAP_SKU_ID__c);
            }
        }
        
        for (List<ONTAP__Product__c> prds : [SELECT Id, ONTAP__ProductCode__c FROM ONTAP__Product__c WHERE ONTAP__ProductCode__c IN:SAPNumsToRelate]){
            for (ONTAP__Product__c prd : prds){
                 prdReference.put(prd.ONTAP__ProductCode__c,prd);
            }            
        }
        
        System.debug('prdReference '+prdReference);
      
        
        for (HONES_ProductPerClient__c finalExcSKU:newlist){
           ONTAP__Product__c prd = prdReference.get(finalExcSKU.SAP_SKU_ID__c);
            if(prd!= NULL){           
                finalExcSKU.HONES_RelatedProduct__c = prd.Id;
			}
           System.debug('Related Products: '+finalExcSKU);
        }
    }
    */
    
    /*
     * No need for code if accounts are linked using an External Id-Unique
     * To relate the account use instead ONTAP__Codigo_del_cliente__c
     * ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
     * drs@avx
     */
    /*
    public static void relateAccountsToExclSKUData(List<HONES_ProductPerClient__c> newlist){
        Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,Account> accReference = new Map<String,Account>();
        
        for (HONES_ProductPerClient__c accExclSKU:newlist){
            if(accExclSKU.SAP_Account_ID__c!=NULL){
                SAPNumsToRelate.add(accExclSKU.SAP_Account_ID__c);
            }
        }
        
        for (List<Account> accs : [SELECT Id, ONTAP__SAP_Number__c FROM Account WHERE ONTAP__SAP_Number__c IN:SAPNumsToRelate]){
            for (Account acc : accs){
                 accReference.put(acc.ONTAP__SAP_Number__c,acc);
            }            
        }
        
        System.debug('accReference'+accReference);
      
        
        for (HONES_ProductPerClient__c finalExcSKU:newlist){
           Account acc = accReference.get(finalExcSKU.SAP_Account_ID__c);
            if(acc!= NULL){           
                finalExcSKU.HONES_RelatedClient__c = acc.Id;
			}
           System.debug('Related Clients: '+finalExcSKU);
        }
    }
    */
}