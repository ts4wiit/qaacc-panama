/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_CoverageTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 03/01/2019     Carlos Leal             Creation of methods.
 */

public class V360_CoverageTriggerHandler extends TriggerHandlerCustom{

    private Map<Id, v360_Coverage__c> newMap;
    private Map<Id, v360_Coverage__c> oldMap;
    private List<v360_Coverage__c> newList;
    private List<v360_Coverage__c> oldList;
    
    
   /**
    * Constructor of the class
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    
    public V360_CoverageTriggerHandler() {
        
        this.newMap = (Map<Id, v360_Coverage__c>) Trigger.newMap;
        this.oldMap = (Map<Id, v360_Coverage__c>) Trigger.oldMap;
        this.newList = (List<v360_Coverage__c>) Trigger.new;
        this.oldList = (List<v360_Coverage__c>) Trigger.old;
    }
    
   /**
    * method wich is executed every time a coverage is inserted
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    
    public override void beforeInsert(){
         //relateAccountsToEmpties(this.newlist);
    }
    
    /**
    * method wich is executed every time a coverage is inserted
    * Created By: c.leal.beltran@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Last Modify Date: 28/03/2019
    * @param New values of Product modified
    * @return void
    */
    
    public override void afterInsert(){   
        if(!System.isFuture()){
        	//relateAccountsToEmpties(this.newlist);
        	
        	// ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
        	// Using ONTAP__Codigo_del_cliente__c instead 
        	// drs@avx
        	//relateAccountsToEmpties(this.newMap.keySet()); // removed as ONTAP__SAP_Number__c is not indexed in current 
        }
    }
    
  /**
    * method wich is executed every time a coverage is updated
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    
    public override void beforeUpdate(){
        //relateAccountsToEmpties(this.newlist);
    }
    
     /**
    * method wich is executed every time a coverage is updated
    * Created By: c.leal.beltran@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Last Modify Date: 28/03/2019
    * @param New values of Product modified
    * @return void
    */
    
    public override void afterUpdate(){
        if(!System.isFuture()){
        	//relateAccountsToEmpties(this.newlist);
        	// ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
        	// drs@avx
        	//relateAccountsToEmpties(this.newMap.keySet());
        }
    }
    
    
  /**
    * method wich relates an account to a coverage record
    * Created By: c.leal.beltran@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Last Modify Date: 28/03/2019
    * @param New values of Product modified
    * @return void
    */
    
    /*
     * No need for code if accounts are linked using an External Id-Unique
     * To relate the account use instead ONTAP__Codigo_del_cliente__c
     * ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
     * drs@avx
     */
     
    /*
    @Future
    public static void relateAccountsToEmpties(Set<Id> setIds){
        Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,Account> accReference = new Map<String,Account>();
        
        List<v360_Coverage__c> newlist = new List<v360_Coverage__c>();
        List<v360_Coverage__c> listtoUpdate = new List<v360_Coverage__c>();
        newlist = [SELECT Id, V360_Id_client_SAP__c FROM v360_Coverage__c WHERE Id IN: setIds];
        
        for (v360_Coverage__c accEmp:newlist){
            if(accEmp.V360_Id_client_SAP__c!=NULL){
                SAPNumsToRelate.add(accEmp.V360_Id_client_SAP__c);
            }
        }
        
        for (List<Account> accs : [SELECT Id, ONTAP__SAP_Number__c FROM Account WHERE ONTAP__SAP_Number__c IN:SAPNumsToRelate]){
            for (Account acc : accs){
                 accReference.put(acc.ONTAP__SAP_Number__c,acc);
            }            
        }

        for (v360_Coverage__c finalEmp:newlist){
           Account acc = accReference.get(finalEmp.V360_Id_client_SAP__c );
            if(acc!= NULL){           
                finalEmp.V360_Account__c = acc.Id;
                listtoUpdate.add(finalEmp);
			}
        }
        
        if(!listtoUpdate.isEmpty()){
            update listtoUpdate;
        }
    }
    */
}