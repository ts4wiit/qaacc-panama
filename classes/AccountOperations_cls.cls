/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Service class for Account related operations

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    23/08/2017 Daniel Peñaloza            Class created
*******************************************************************************/

public class AccountOperations_cls {
    private static Map<String, SObjectField> mapAccountFields = DevUtils_cls.getFieldsMap('Account');

    // Validate if Account should be deleted from Plan or Tour
    public static void validateAccountsToRemove(Account[] lstAccounts) {
        ObjectValidationSettings__mdt[] lstAccountValidationSettings = DevUtils_cls.getObjectValidationSettings('Account');
        Map<Id, Account> mapAccountsToDeleteFromPlan = new Map<Id, Account>();
        Map<Id, Account> mapAccountsToDeleteFromTour = new Map<Id, Account>();

        // Iterate accounts and validate if they require to be deleted
        for (Account objAccount : lstAccounts) {
            for (ObjectValidationSettings__mdt accValidation : lstAccountValidationSettings) {
                /*String fieldName = accValidation.FieldToValidate__r.DeveloperName + '__c';
                String dataType = mapAccountFields.get(fieldName).getDescribe().getSOAPType().name();
                String valueToCompare = accValidation.ValueToCompare__c;

                // Boolean
                if (DevUtils_cls.DATA_TYPE_BOOLEAN.equalsIgnoreCase(dataType)) {
                    Boolean accountFieldValue = Boolean.valueOf(objAccount.get(fieldName));
                    Boolean blnValueToCompare = Boolean.valueOf(valueToCompare);

                    // Validate if field value in Account matches specified value in settings
                    if (accountFieldValue == blnValueToCompare) {
                        if (accValidation.DeleteFromVisitPlan__c && !mapAccountsToDeleteFromPlan.containsKey(objAccount.Id)) {
                            mapAccountsToDeleteFromPlan.put(objAccount.Id, objAccount);
                        }

                        if (accValidation.DeleteFromTour__c && !mapAccountsToDeleteFromTour.containsKey(objAccount.Id)) {
                            mapAccountsToDeleteFromTour.put(objAccount.Id, objAccount);
                        }
                    }
                }*/
                // Double
                //else if (DevUtils_cls.DATA_TYPE_DOUBLE.equalsIgnoreCase(dataType)) {
                //    // TODO: Add field validations
                //}
                //// Integer
                //else if (DevUtils_cls.DATA_TYPE_INTEGER.equalsIgnoreCase(dataType)) {
                //    // TODO: Add field validations
                //}
                //// String
                //else if (DevUtils_cls.DATA_TYPE_STRING.equalsIgnoreCase(dataType)) {
                //    // TODO: Add field validations
                //}
            }
        }

        // Remove filtered Accounts from Visit Plans and Tour Events
        if (!mapAccountsToDeleteFromPlan.isEmpty()) {
            removeAccountsFromPlans(mapAccountsToDeleteFromPlan);
        }

        if (!mapAccountsToDeleteFromTour.isEmpty()) {
            removeAccountsFromTours(mapAccountsToDeleteFromTour);
        }
    }

    /**
     * Removes accounts from plans where End Date is greater than today
     * @param      mapAccountsToRemove  The map of accounts to remove
     */
    public static void removeAccountsFromPlans(Map<Id, Account> mapAccountsToRemove) {
        Date todayDate = Date.today();

        AccountByVisitPlan__c[] lstAccountsByVisitPlan = [
            SELECT Id
            FROM AccountByVisitPlan__c
            WHERE Account__c IN :mapAccountsToRemove.keySet()
                AND VisitPlan__r.EffectiveDate__c > :todayDate
        ];

        // Delete accounts from Visit Plans
        if (!lstAccountsByVisitPlan.isEmpty()) {
            Database.DeleteResult[] lstDeleteResults = Database.delete(lstAccountsByVisitPlan, false);
        }
    }

    /**
     * Removes Calls and Events related to accounts from Tours where Status is Created
     * @param      mapAccountsToRemove  The map of accounts to remove
     */
    public static void removeAccountsFromTours(Map<Id, Account> mapAccountsToRemove) {
        VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();

        // Query for Calls and Events related to filtered Accounts that are in Visit Lists with status "Created"
        ONCALL__Call__c[] lstCalls = [
            SELECT  Id
            FROM    ONCALL__Call__c
            WHERE   (CallList__c != null And CallList__r.ONTAP__TourStatus__c = :visitPlanSettings.CreatedTourStatus__c) 
                    And ONCALL__POC__c IN: mapAccountsToRemove.keySet()
        ];

        Event[] lstEvents = [
            SELECT  Id
            FROM    Event
            WHERE   (VisitList__c != null And VisitList__r.ONTAP__TourStatus__c = :visitPlanSettings.CreatedTourStatus__c) 
                    And WhatId IN :mapAccountsToRemove.keySet()
        ];

        // Delete Calls related to accounts
        if (!lstCalls.isEmpty()) {
            Database.DeleteResult[] lstDeleteResults = Database.delete(lstCalls, false);
            //delete lstCalls;
        }

        // Delete Events related to accounts
        if (!lstEvents.isEmpty()) {
            Database.DeleteResult[] lstDeleteResults = Database.delete(lstEvents, false);
            //delete lstEvents;
        }
    }

}