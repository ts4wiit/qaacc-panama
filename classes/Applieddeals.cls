/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: FlexibleDataController.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 18/12/2018     Luis Arturo Parra        Creation of methods.
*/

public class Applieddeals {

    /**
    * Methods getters and setters By entity Applieddeals
    * Created By: Luis Parra
    * Suport contact: g.martinez.g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */        
    public String condcode{get; set;}
    public Double amount{get; set;}
    public Double percentage{get; set;}
    public String pronr{get; set;}
}