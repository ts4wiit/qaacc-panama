/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_VIEW360_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 08/01/2019           Jose Luis Vargas       Crecion de la clase para testing de la clase CS_VIEW360_CLASS 
 */

@isTest
private class CS_View360_Class_Test
{
    /**
    * Method for test the method GetIdAccount
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetIdAccount()
    {      
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        
        CS_View360_Class obj01 = new CS_View360_Class();
        CS_View360_Class.GetIdAccount(oNewCase.Id);
    }
}