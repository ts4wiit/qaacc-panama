public with sharing class HONES_GetStatusBatch_LT {
     /*El servicio está preparado para enviar múltiples ids de Ordenes
    *Este método sólo es para uso del componente HONES_GET_STATUS_BATCH
    *que es llamado desde la página de 
    */

    @AuraEnabled
    public static CustomResponse getStatusBatch(String recordId){
          CustomResponse response = new CustomResponse();
          list<ONTAP__Order__c> orders = new list<ONTAP__Order__c>();
          ONTAP__Order__c order = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c, ONCALL__OnCall_Status__c,
                                          ONCALL__SAP_Order_Response__c, ONTAP__SAP_Order_Number__c,
                                          ONTAP__OrderAccount__r.ONTAP__ExternalKey__c
                                   FROM ONTAP__Order__c
                                   WHERE Id =: recordId];
          RequestClass request = new RequestClass();
          request.ids.add(new IdElement(order.Id));
          
          /*JSONGenerator generator = JSON.createGenerator(true);
          generator.writeObject(request);
          String jsonRequest= generator.getAsString().replaceAll('\\n',''); */
          String jsonRequest = '[ {    "Id" : "' + order.Id  + '"  } ]';
          Response = sentGetStatusBatch (jsonRequest, order);
          return response;
    }

    public static CustomResponse sentGetStatusBatch(String json ,ONTAP__Order__c order){
        String country = order.ONTAP__OrderAccount__r.ONTAP__ExternalKey__c.Left(2);
        List<End_Point__mdt> endpoint = [SELECT DeveloperName,MasterLabel,Token__c,URL__c 
                                        FROM End_Point__mdt 
                                        WHERE MasterLabel = 'Get_Status_Batch' AND Country__c =: country];
        CustomResponse cResponse= new CustomResponse();

        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint(endpoint[0].URL__c);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json;charset=utf-8');
        httpReq.setHeader('bearer', endpoint[0].Token__c);
        httpReq.setBody(json);
        System.debug('sentGetStatusBatch::json sent: ' +json);
        HTTPResponse authresp = new HTTPResponse();
        try{
            authresp = h.send(httpReq);
            if(authresp.getStatusCode() == 200 && authresp.getBody() != null){
                 System.debug('sentGetStatusBatch::Success: ' + authresp.getBody());
                 ResponseElement rbol = new ResponseElement(); //No se necesitaria si creamos otra clase aparte y colcoamos el metodo de parse como estatico
                 //ResponseClass response = rbol.parse(authresp.getBody());
                 //System.debug('sentGetStatusBatch::response: ' + response);
                 String bodyTransformed = authresp.getBody().replace('ONCALL__SAP_Order_Response__c', 'orderResponse');
                 bodyTransformed = bodyTransformed.replace('ONCALL__SAP_Order_Number__c', 'sapNumber');
                 bodyTransformed = bodyTransformed.replace('Id', 'id');
                 bodyTransformed = bodyTransformed.replace('[', '').replace(']', '');
                 
                 System.debug('sentGetStatusBatch::bodyTransformed: ' + bodyTransformed);
                 ResponseElement response = rbol.parse(bodyTransformed);
                 if(response != null){
                      order.ONCALL__SAP_Order_Response__c = response.orderResponse;
                      order.ONTAP__SAP_Order_Number__c = response.sapNumber;
                      
                      cResponse.success = true;
                      cResponse.status = 'OK';
                      cResponse.message = response.orderResponse;
                      
                 }else{
                     cResponse.status = 'ERROR';
                     cResponse.message = 'CONSULT ADMIN 1:' + authresp.getBody();
                 }
            }else{
                 cResponse.status = 'ERROR';
                 cResponse.message = 'CONSULT ADMIN 2:' + authresp.getBody() + ':: StatusCode:: ' + authresp.getStatusCode();
            }
        }catch(Exception e){
            cResponse.status = 'ERROR';
            cResponse.message = 'CONSULT ADMIN 3: message: ' + e.getMessage() + ', cause: ' + e.getCause() + ', line: ' + e.getLineNumber() + ', json:' + json;  
        }finally{
            ISSM_TriggerManager_cls.inactivate('CS_TRIGGER_PEDIDO');
            update order;
        }

        return cResponse;
    }

    
    public class RequestClass {
        public list<IdElement> ids {get; set;}
        public RequestClass(){
            ids = new list<IdElement>();
        }
    }

    public class IdElement{
        public String Id {get;set;}
        public IdElement(String id){
            this.Id = id;
        }
    }

    public class ResponseClass{
        public list<ResponseElement> result {get;set;}

        public  ResponseClass parse(String json){
		    return (ResponseClass) System.JSON.deserialize(json, ResponseClass.class);
	    }
    }

    public class ResponseElement{
        public  String id {get; set;}
        public String orderResponse {get; set;}
        public String sapNumber {get; set;}

        public  ResponseElement parse(String json){
		    return (ResponseElement) System.JSON.deserialize(json, ResponseElement.class);
	    }
    }

    public class CustomResponse{
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String message {get;set;}
        @AuraEnabled public boolean success {get;set;}
        public CustomResponse(){
            this.success = true;
            this.status = 'OK';
        }
    }
}