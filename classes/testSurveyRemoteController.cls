/**
 * @author Magdalena Tomala <m.tomala@polsource.com>
 * @since 12/10/2015
 * @description Remote methods used for Survey execution.
 */
 global with sharing class testSurveyRemoteController {
    
    public static final String SUCCESS_CODE   = '0';
    public static final String EXCEPTION_CODE = '-1';
    public static final String RESULTS        = 'Results';

    public static ONTAP__Auth_Keys__c azureSettings {
        get {
            ONTAP__Auth_Keys__c azureSettings;
            String azureInstanceName;
            ONTAP__Auth_Settings__c azureInstance = ONTAP__Auth_Settings__c.getInstance('Azure');
            if (azureInstance != null) {
                azureInstanceName = azureInstance.ONTAP__Instance__c;
            }
            if (azureInstanceName != null) {
                azureSettings = ONTAP__Auth_Keys__c.getInstance(azureInstanceName );
            }
            return azureSettings;
        }
    }

    global testSurveyRemoteController(ApexPages.StandardController stdController) { }

    public Boolean getMakeReadOnly() {
        return testSureveReadOnly_Checker.isReadOnly();
    }
    
    @RemoteAction
    global static testSurveyWrappers.testListDataWrapper getAzureResponses(String surveyTakerId) {
        List<String> result = new List<String>();
        Map<String, testAzureForceUtil.AzureBlob> azureBlobs = testSurveyService.getAzureResponses(surveyTakerId);
        result.add(JSON.serialize(azureBlobs));
        return new testSurveyWrappers.testListDataWrapper(result);
    }

    @RemoteAction
    global static testSurveyWrappers.testSurveyWrapper getAvailableSurveys(String surveyName) {
        List<ONTAP__Survey__c> surveys = testSurveyService.getAvailableSurveys(surveyName);
        return new testSurveyWrappers.testSurveyWrapper(RESULTS, SUCCESS_CODE, surveys);
    }

    @RemoteAction
    global static testSurveyWrappers.testSurveyQuestionsResponseWrapper getSurveyDataBySurveyTaken(String surveyTakerId) {
        testSurveyWrappers.testSurveyQuestionsWrapper survey;
        try {
            survey = testSurveyService.getSurveyDataBySurveyTaken(surveyTakerId);
        } catch (Exception e) {
            return new testSurveyWrappers.testSurveyQuestionsResponseWrapper(e.getMessage(), EXCEPTION_CODE, null);
        }
        return new testSurveyWrappers.testSurveyQuestionsResponseWrapper(RESULTS, SUCCESS_CODE, new List<testSurveyWrappers.testSurveyQuestionsWrapper>{survey});
    }

    @RemoteAction
    global static testSurveyWrappers.testSurveyQuestionsResponseWrapper getSurveyDataByAccountAndId(String accountId, String surveyId) {
        testSurveyWrappers.testSurveyQuestionsWrapper survey;
        try {
            survey = testSurveyService.getSurveyDataById(accountId, surveyId); 
        } catch (Exception e) {
            return new testSurveyWrappers.testSurveyQuestionsResponseWrapper(e.getMessage(), EXCEPTION_CODE, null);
        }
        
        return new testSurveyWrappers.testSurveyQuestionsResponseWrapper(RESULTS, SUCCESS_CODE, new List<testSurveyWrappers.testSurveyQuestionsWrapper>{survey});
    }

    @RemoteAction
    global static testSurveyWrappers.testSurveyResponsesWrapper submitSurveyTaker(String surveyTakerId, String userAnswers, String questions, String surveyType, String status) {
        return submitSurveyTakerWithNA(surveyTakerId, userAnswers, '{}', questions, surveyType, status);
    }

    @RemoteAction
    global static testSurveyWrappers.testSurveyResponsesWrapper submitSurveyTakerWithNA(String surveyTakerId, String userAnswers, String questionsNA, String questions, String surveyType, String status) {
       Map<String,Object> result = testSurveyService.submitSurveyTaker(surveyTakerId, userAnswers, questionsNA, questions, surveyType, status);

        if (result.isEmpty()) {
            return new testSurveyWrappers.testSurveyResponsesWrapper(RESULTS, SUCCESS_CODE, null);
        }
        return new testSurveyWrappers.testSurveyResponsesWrapper(RESULTS, SUCCESS_CODE, result);
    }

    @RemoteAction
    global static testSurveyWrappers.testSurveyResponsesWrapper submitSurvey(String surveyId, String accountId, String userAnswers, String questions, String surveyType, String status) {
        return submitSurveyWithNA(surveyId, accountId, userAnswers, '{}', questions, surveyType, status);
    }

    @RemoteAction
    global static testSurveyWrappers.testSurveyResponsesWrapper submitSurveyWithNA(String surveyId, String accountId, String userAnswers, String questionsNA, String questions, String surveyType, String status) {
        Map<String,String> result;
        try {
            result = testSurveyService.submitSurvey(surveyId, accountId, userAnswers, questionsNA, questions, surveyType, status);
        } catch (Exception e) {
            return new testSurveyWrappers.testSurveyResponsesWrapper(e.getMessage(), EXCEPTION_CODE, result);  
        }
        return new testSurveyWrappers.testSurveyResponsesWrapper(RESULTS, SUCCESS_CODE, result);
    }

    @RemoteAction
    global static testSurveyWrappers.testSurveyResponsesWrapper getUserResponses(String surveyTakenId) {
        List<ONTAP__SurveyQuestionResponse__c> responses = testSurveyService.retrieveResponses(surveyTakenId);
        Map<String,Object> result = testSurveyService.getUserResponses(responses);
        Map<String, Boolean> naSelected = testSurveyService.getUserSelectedNAs(responses);

        if (result.isEmpty()) {
            return new testSurveyWrappers.testSurveyResponsesWrapper(RESULTS, SUCCESS_CODE, null, null);
        }
        return new testSurveyWrappers.testSurveyResponsesWrapper(RESULTS, SUCCESS_CODE, result, naSelected);
    }
    
    @RemoteAction
    public static String doUploadAttachment(String sqrId, String attachmentBody, String attachmentName, String attachmentId)
    {
        String result = testSurveyService.doUploadAttachment(sqrId,attachmentBody,attachmentName,attachmentId);
        return result;
    }

    @RemoteAction
    public static String deleteAttachment(String attId) {
        String result = testSurveyService.deleteAttachment(attId);
        return result;
    }

    global static testSurveyWrappers.testSurveyQuestionsResponseWrapper getSurveyDataById(String surveyId) {
        return null; // deprecated
    }

    global static testSurveyWrappers.testSurveyQuestionsResponseWrapper getSurveyDataById(String accountId, String surveyId) {
        return null; // deprecated
    }

}