/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: accsearchcontroller_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 10/01/2018     Carlos Leal               Creation of methods.
 */
@isTest
public class accsearchcontroller_Test {
	
    /**
    * Test method for fill and clean the account in the class
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void test_searchClear_Case(){
        accsearchcontroller obj01 = new accsearchcontroller();
        Account acc = new Account();
        acc.Name = 'TestAcc'; 
        insert acc;
        obj01.search();
        obj01.clear();
    }
}