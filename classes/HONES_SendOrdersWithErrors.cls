/* ----------------------------------------------------------------------------
 * AB InBev :: OnCall
 * ----------------------------------------------------------------------------
 * Clase: HONES_SendOrdersWithErrors.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 22/01/2019           Oscar Garcia       	Creación de métodos para envío de órdenes manuales 
 */

public class HONES_SendOrdersWithErrors {
    
    @AuraEnabled
    public Static String sendOrder(String recordId){
		
        List<ONTAP__Order__c> order = new List<ONTAP__Order__c>();
        
        //get order information
        order = [Select Id, Name, ONCALL__SAP_Order_Number__c, ONCALL__SAP_Order_Response__c,ONTAP__OrderAccount__c,
                ONTAP__OrderAccount__r.ONTAP__SAPCustomerId__c
                From ONTAP__Order__c
                Where Id =: recordId 
                Limit 1
                ];
        String SAPIdCuentakey = order[0].ONTAP__OrderAccount__r.ONTAP__SAPCustomerId__c;
        String country = SAPIdCuentakey.left(2);
        //validate if the order is not in SAP
        if(order[0].ONCALL__SAP_Order_Number__c == null){
            ONCALL_orderToJSON order2json = new ONCALL_orderToJSON();              
            Set<Id> setIdOrders = new Set<Id>();
            setIdOrders.add(recordId);
        	//order2json.createJson(recordId);
        	//order2json.createJson(setIdOrders);
            if(country == GlobalStrings.PANAMA_COUNTRY_CODE){
                ONCALL_orderToJSON.createJsonOnCall(setIdOrders);
            }else if(country == GlobalStrings.EL_SALVADOR_COUNTRY_CODE || country == GlobalStrings.HONDURAS_COUNTRY_CODE){
                ONCALL_orderToJSON.createJsonOnCall_HONES(setIdOrders,'');
            }
            
        }     
        
        return recordId;
    }	
}