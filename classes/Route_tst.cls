/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Test class for ONTAP_Route__c (Route_cls) related actions

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    28/08/2017 Daniel Peñaloza            Created Class
1.1    19/04/2018 Rodrigo Resendiz            Add methods to set BDR as the Owner of Accounts
*******************************************************************************/

@isTest
private class Route_tst {
    
    public static final Map<String, RecordType> mapRouteRecordTypes = DevUtils_cls.getRecordTypes('ONTAP__Route__c', 'DeveloperName');
    
    @testSetup
    static void setup() {
        ONTAP__Route__c route = TestUtils_tst.generateFullRoute();
        insert route;
    }
    
    @isTest static void changeSupervisor() {
        ONTAP__Route__c routeWithSupervisor = new ONTAP__Route__c();
        
        // Sales Agent (Route Manager) 1
        User routeManager1 = TestUtils_tst.generateUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 1);
        insert routeManager1;
        
        // Sales Agent (Route Manager) 2
        User routeManager2 = TestUtils_tst.generateUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 2);
        insert routeManager2;
        
        // Supervisor 1
        User supervisor1 = TestUtils_tst.generateUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 3);
        insert supervisor1;
        
        // Supervisor 2
        User supervisor2 = TestUtils_tst.generateUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 4);
        insert supervisor2;

        // Sales Office Account
        Account salesOfficeAccount = TestUtils_tst.generateAccount(TestUtils_tst.RECORD_TYPE_ACCOUNT_SALESOFFICE, null, 12345, null);
        salesOfficeAccount.ONTAP__SalesOffId__c = 'FF05';
        insert salesOfficeAccount;

        // Customer Account
        Account customerAccount = TestUtils_tst.generateAccount(null, null, 123, salesOfficeAccount.Id);
        insert customerAccount;

        // Add user to Account Team
        DevUtils_cls.addUserToAccountTeam(customerAccount.Id, routeManager1.Id, new Set<String> { TestUtils_tst.TEAM_MEMBER_ROLE_PRESALES, TestUtils_tst.TEAM_MEMBER_ROLE_SALES_MANAGER });
        
        ONTAP__Vehicle__c objONTAPVehicle       = new ONTAP__Vehicle__c();
        objONTAPVehicle.ONTAP__VehicleId__c     = '112345-23';
        objONTAPVehicle.ONTAP__VehicleName__c   = 'Zurdo Movil';
        objONTAPVehicle.ONTAP__SalesOffice__c   = salesOfficeAccount.Id;
        insert objONTAPVehicle;
        
        String recordTypeDevName = 'Sales';
        String routeId = 'ABC123';

        // Set Fields for Route record
        routeWithSupervisor.RecordTypeId          = mapRouteRecordTypes.get(recordTypeDevName).Id;
        routeWithSupervisor.ServiceModel__c       = 'Presales';
        routeWithSupervisor.ONTAP__SalesOffice__c = salesOfficeAccount.Id;
        routeWithSupervisor.RouteManager__c       = routeManager1.Id;
        routeWithSupervisor.Supervisor__c         = supervisor1.Id;
        routeWithSupervisor.OwnerId               = routeManager1.Id;
        routeWithSupervisor.ONTAP__RouteId__c     = routeId;
        routeWithSupervisor.Vehicle__c            = objONTAPVehicle.id;
        
        insert routeWithSupervisor;
        
        Test.startTest();
        
        routeWithSupervisor.Supervisor__c = null;
        update routeWithSupervisor;
        
        routeWithSupervisor.RouteManager__c = routeManager2.Id;
        update routeWithSupervisor;
        
        delete routeWithSupervisor;
        
        Test.stopTest();
    }

	@isTest static void should_validate_sales_agent() {
        Route_cls routeService = new Route_cls();
		ONTAP__Route__c[] lstRoutes = [
            SELECT Id, ServiceModel__c, RouteManager__c, ONTAP__SalesOffice__c
            FROM ONTAP__Route__c
        ];
        routeService.validateSalesAgent(lstRoutes);
	}
    @isTest static void should_setBDRAsAccountOwner() {
        Route_cls routeService = new Route_cls();
        Set<Id> idstodel = new Set<Id>();
		ONTAP__Route__c[] lstRoutes = [
            SELECT Id, ServiceModel__c, RouteManager__c, ONTAP__SalesOffice__c
            FROM ONTAP__Route__c
        ];
        Map<Id,ONTAP__Route__c> mapRoutes = new Map<Id,ONTAP__Route__c>();
        for(ONTAP__Route__c route:lstRoutes){
            mapRoutes.put(route.id,route);
            idstodel.add(route.id);
        }
        
        Route_cls.setBDRAsAccountOwner(lstRoutes, mapRoutes);
        Route_cls.setAccountTeam(lstRoutes, mapRoutes);
        routeService.deleteRelatedAccountsByRoute(idstodel);
	}

	@isTest static void should_filter_service_models() {
		Route_cls routeService = new Route_cls();
        ONTAP__Route__c[] lstRoutes = [
            SELECT Id, ServiceModel__c, RouteManager__c, ONTAP__SalesOffice__c
            FROM ONTAP__Route__c
        ];
        routeService.filterRoutes(lstRoutes);
	}
    
    @isTest static void test_AssignManagerToSalesAgent_UseCase1() {
        List<ONTAP__Route__c> route_lst = new List<ONTAP__Route__c>();
        Map<Id, ONTAP__Route__c> mapRoutes = new Map<Id, ONTAP__Route__c>();
        
        Route_cls rout = new Route_cls();  
        rout.AssignManagerToSalesAgent(route_lst, mapRoutes);
    }

}