/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_Cooler_Request_Class_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                User                   Description
 * 14/03/2019          Jose Luis Vargas       Crecion de la clase para testing de la clase CS_Cooler_Request_Class 
 */

@isTest
public class CS_Cooler_Request_Class_Test 
{
    @isTest
    static void test_CS_Cooler_Request_Class()
    {
        CS_Cooler_Request_Class oCooler = new CS_Cooler_Request_Class();
        oCooler.salesOrg = 'test';
        oCooler.customerOrder = '1234';
        oCooler.deliveryDate = '2019-04-28';
        oCooler.empties = 'true';
        oCooler.itmNumber = 'N235';
        oCooler.measurecode = 'Lt';
        oCooler.numb = 'test345NS';
        oCooler.orderReason = 'PFN';
        oCooler.orderType = 'Finished_Product';
        oCooler.paymentMethod = 'Credit';
        oCooler.position = '2';
        oCooler.quantity = 12;
        oCooler.role = 'CS';
        oCooler.sfdcId = '00122345';
        oCooler.sku = '9909';
    }
}