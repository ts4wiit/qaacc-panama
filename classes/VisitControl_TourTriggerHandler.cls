/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_TourTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class VisitControl_TourTriggerHandler extends TriggerHandlerCustom{

    private Map<Id, ONTAP__Tour__c > newMap;
    private Map<Id, ONTAP__Tour__c > oldMap;
    private List<ONTAP__Tour__c > newList;
    private List<ONTAP__Tour__c > oldList;
    
    /**
    * Constructor of the class
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public VisitControl_TourTriggerHandler() {
        this.newMap = (Map<Id, ONTAP__Tour__c >) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__Tour__c >) Trigger.oldMap;
        this.newList = (List<ONTAP__Tour__c >) Trigger.new;
        this.oldList = (List<ONTAP__Tour__c >) Trigger.old;
    }
    
    /**
    * Method wich is executed every time before a insert
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void 
    */
    public override void beforeInsert(){
        List<ONTAP__Tour__c > listObj = PreventExecutionUtil.validateTourWithOutId(this.newList);
        retraceTourOneDay(listObj);
        anticipateTourOneDay(listObj);
    }
    
    
    /**
    * Method wich is executed every time before a insert
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    
    /*public void changeOwner( List<ONTAP__Tour__c > newList){
        
        Id routeRt = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.RECORDTYPE_VISITCONTROL_ROUTE).getRecordTypeId();
        Id tourRt = Schema.SObjectType.ONTAP__Tour__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.TELESALES).getRecordTypeId();
        
        Set<Id> routeIds = new Set<Id>();
        for(ONTAP__Tour__c tour : newList){
            
            if (tour.VisitPlan__c != NULL && tour.VisitPlan__r.Route__c != NULL) {
                routeIds.add(tour.VisitPlan__r.Route__c);
            }
            
        }
        
        Map<Id,ONTAP__Route__c> routes =  new Map<Id, ONTAP__Route__c> ([SELECT ID, RouteManager__c, Supervisor__c FROM ONTAP__Route__c WHERE Id IN :routeIds]);
        
        for(ONTAP__Tour__c tour : newList){
            
            if(tour.VisitPlan__c != NULL && tour.RecordTypeId!= NULL  && tour.RecordTypeId =tourRt){}
            
            
            if (tour.VisitPlan__c != NULL && tour.VisitPlan__r.Route__c != NULL  
                && tour.VisitPlan__r.Route__r.Supervisor__c != NULL && tour.RecordTypeId != NULL 
                && tour.VisitPlan__r.Route__r.RecordTypeId  != NULL
                && tour.RecordTypeId== tourRt && tour.VisitPlan__r.Route__r.RecordTypeId == routeRt){
                    
                    
                
            }else if (tour.VisitPlan__c != NULL && tour.VisitPlan__r.Route__c != NULL && 
                      tour.VisitPlan__r.Route__r.RouteManager__c != NULL && tour.RecordTypeId != NULL && 
                      tour.VisitPlan__r.Route__r.RecordTypeId != NULL &&
                      tour.VisitPlan__r.Route__r.RecordTypeId == routeRt){
                          
            }
        }
        
        
    }*/
    
    
   
    /**
    * Method to set a retrace by tour in a day    
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public static void retraceTourOneDay( List<ONTAP__Tour__c> newList){
        
        Set<Id> routes = new Set<Id>();
        
        for( ONTAP__Tour__c tourObj: newList){
            if(tourObj.VisitControl_IsRetraced__c  != NULL && tourObj.VisitControl_IsRetraced__c  == TRUE ){
                routes.add(tourObj.VisitControl_VisitplanRoute__c );
            }       
        }
        
        List<VisitControl_RetraceAndAnticipateRoute__c> routesToAnticipate = [SELECT Id, VisitControl_Route__c, VisitControl_RetraceOrAnticipate__c, VisitControl_DateToSkip__c FROM VisitControl_RetraceAndAnticipateRoute__c  WHERE 
                                                                 VisitControl_Route__c IN: routes AND 
                                                                 VisitControl_RetraceOrAnticipate__c =:GlobalStrings.VISITCONTROL_RETRACE]; 
        for( ONTAP__Tour__c  tourObj: newList){
            
            for (VisitControl_RetraceAndAnticipateRoute__c obj:routesToAnticipate){    
                
                Date startDate = tourObj.ONTAP__TourDate__c;
                
                if (tourObj.VisitControl_VisitplanRoute__c  != NULL && tourObj.VisitControl_VisitplanRoute__c   == obj.VisitControl_Route__c && startDate.toStartOfWeek() == obj.VisitControl_DateToSkip__c.toStartOfWeek() && 
                    startDate >= obj.VisitControl_DateToSkip__c){
                    System.debug('--------------------  TOUR START DATE startDate' +startDate);
                    Date startDateRetraced = startDate.addDays(1);
                    tourObj.ONTAP__TourDate__c = startDateRetraced ;
                    System.debug('--------------------  TOUR START DATE new Date' +startDateRetraced);
                }
            }
        }
    }
    
    /**
    * Method to set a anticipe by tour in a day    
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public static void anticipateTourOneDay( List<ONTAP__Tour__c> newList){
        
        Set<Id> routes = new Set<Id>();
        
        for( ONTAP__Tour__c tourObj: newList){
            if(tourObj.VisitControl_IsRetraced__c  != NULL && tourObj.VisitControl_IsRetraced__c  == TRUE ){
                routes.add(tourObj.VisitControl_VisitplanRoute__c );
            }       
        }
        
        List<VisitControl_RetraceAndAnticipateRoute__c> routesToAnticipate = [SELECT Id, VisitControl_Route__c, VisitControl_RetraceOrAnticipate__c, VisitControl_DateToSkip__c FROM VisitControl_RetraceAndAnticipateRoute__c  WHERE 
                                                                 VisitControl_Route__c IN: routes AND 
                                                                 VisitControl_RetraceOrAnticipate__c =: GlobalStrings.VISITCONTROL_ANTICIPATE]; 
        for( ONTAP__Tour__c  tourObj: newList){
            
            for (VisitControl_RetraceAndAnticipateRoute__c obj:routesToAnticipate){    
                
                Date startDate = tourObj.ONTAP__TourDate__c;
                
                if (tourObj.VisitControl_VisitplanRoute__c  != NULL && tourObj.VisitControl_VisitplanRoute__c   == obj.VisitControl_Route__c && startDate.toStartOfWeek() == obj.VisitControl_DateToSkip__c.toStartOfWeek() && 
                    startDate <= obj.VisitControl_DateToSkip__c){
                    System.debug('--------------------  TOUR START DATE startDate' +startDate);
                    Date startDateRetraced = startDate-1;
                    System.debug('--------------------  TOUR START DATE new Date' +startDateRetraced);
                    tourObj.ONTAP__TourDate__c = startDateRetraced ;
                }
            }
        }
    }
}