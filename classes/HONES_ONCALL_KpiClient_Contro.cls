/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_KpiClient_Contro.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public class HONES_ONCALL_KpiClient_Contro {

    /**
    * Method for get flexible data by id of user
    * Created By: luis.arturo.parra@accenture.com
    * @param String recordId
    * @return List<Id>
    */
    @AuraEnabled
    public static List<Id> getFlexibleDataById(String recordId){
        
        List<Id> AccId = new List<Id>();
        List<ONCALL__Call__c> acc = new List<ONCALL__Call__c>();
            acc=[SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =:recordId Limit 1];    
        AccId.add(acc[0].ONCALL__POC__c);
        return AccId;
    }
    
    /**
    * Method for get infromation of users
    * Created By: luis.arturo.parra@accenture.com
    * @param String recordId
    * @return List<ONTAP__KPI__c>
    */
    @AuraEnabled
    public static List<ONTAP__KPI__c> getInfoExample(String recordId){
        
        List<ONTAP__KPI__c> acc2=[SELECT ONTAP__KPI_name__c,P_C_Target__c,ONTAP__Target__c, ONTAP__Actual__c, ONTAP_Last__c, ONTAP_Percentage__c, Months_remains__c FROM ONTAP__KPI__c WHERE ONTAP__Account_id__c =:recordId ];    

        List<ONTAP__KPI__c> acc = new List<ONTAP__KPI__c>();
        
        acc = calc_PC(calcRemainDays(recordId), acc2);
       
        return acc;
    }

    /**
    * Method for get kpis
    * Created By: luis.arturo.parra@accenture.com
    * @param String recordId
    * @return List<ONTAP__KPI__c>
    */
    public static List<ONTAP__KPI__c> calc_PC(Integer remain_days, List<ONTAP__KPI__c>kpis){
        for(ONTAP__KPI__c aux : kpis){
            if(remain_days != 0) aux.P_C_Target__c = (aux.Months_remains__c/remain_days).round(System.RoundingMode.CEILING);
        }   
        return kpis;
    }
    
    /**
    * Method for get the remain days
    * Created By: luis.arturo.parra@accenture.com
    * @param String recordId
    * @return List<ONTAP__KPI__c>
    */
    public static Integer calcRemainDays(String recordId){
        
        integer days_remaining = 0;
        
        Date today = System.Today();
        Date lastMonthDate = Date.newInstance(9999, 1, 1);
        Date start = DATE.newInstance(1900, 1, 7);
        
        if(today.month() == 12){
        	lastMonthDate = Date.newInstance(today.year(), 12, 31);
        }else{
            lastMonthDate = Date.newInstance(today.year(), today.month() + 1, 1) - 1;
        }
        
        List<Account> account = new List<Account>();
        account = [Select id, V360_CallFrequency__c, V360_VisitFrequency__c, ONTAP__Delivery_Delay_Code__c from Account Where id =: recordId Limit 1];
        
        String saleDays = '';
        if(account[0].V360_CallFrequency__c != null && account[0].V360_VisitFrequency__c != null) {
            saleDays = account[0].V360_CallFrequency__c + account[0].V360_VisitFrequency__c;
        }else if(account[0].V360_CallFrequency__c != null){
            saleDays = account[0].V360_CallFrequency__c;
        }else{
            saleDays = account[0].V360_VisitFrequency__c;
        }         
        
		List<Integer> saleDaysValue_list = new List<Integer>();
        Integer numberDeliveryDays = Integer.valueOf(account[0].ONTAP__Delivery_Delay_Code__c.substring(0,2))/24;
        
        for(string aux : saleDays.Split('')){
            if(aux == 'D') saleDaysValue_list.add(1+numberDeliveryDays);
            else if (aux == 'L') saleDaysValue_list.add(2+numberDeliveryDays);
            else if (aux == 'M') saleDaysValue_list.add(3+numberDeliveryDays);
            else if (aux == 'R') saleDaysValue_list.add(4+numberDeliveryDays);
            else if (aux == 'J'){
                if(5+numberDeliveryDays > 7) saleDaysValue_list.add(2);   
                else saleDaysValue_list.add(5+numberDeliveryDays);
            }
            else if (aux == 'V'){
                if(6+numberDeliveryDays <= 7) saleDaysValue_list.add(6+numberDeliveryDays);   
                else saleDaysValue_list.add(2);
            }
            else if (aux == 'S'){
                if(numberDeliveryDays == 3) saleDaysValue_list.add(3);   
                else saleDaysValue_list.add(2);
            }
        }
        
        System.debug('saleDaysValue_list: ' +saleDaysValue_list);
        
        for(integer aux : saleDaysValue_list){
            days_remaining += ((today.daysBetween(lastMonthDate.addDays(-(math.mod(start.daysBetween(lastMonthDate.addDays(1-aux)),7)+1)))) + 8)/7;
            
        } 
		
        return days_remaining;
    }
    
}