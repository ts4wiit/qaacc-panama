/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASEASSIGNMENT_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 08/01/2019           Jose Luis Vargas       Crecion de la clase para testing de la clase CS_CASEASSIGNMENT_CLASS 
 */

@isTest
private class CS_CaseAssignment_Class_Test
{
    /**
    * Method for test the method GetCaseInterlocutorTelventa and AssignEscalationTelventa
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetCaseInterlocutorTelventa()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        
        CS_CASEASSIGNMENT_CLASS obj01 = new CS_CASEASSIGNMENT_CLASS();
        obj01.GetCaseInterlocutorTelventa('Agente Telventa', null, 'Telventa');
        obj01.AssignEscalationTelventa(null,'Telventa');
        
        obj01.GetCaseInterlocutorTelventa('Agente Telventa', oNewAccount.Id,'Telventa');
        obj01.AssignEscalationTelventa(oNewCase.Id,'Telventa');
        
        obj01.GetCaseInterlocutorTelventa('Supervisor Telventa', oNewAccount.Id,'Telventa');
        obj01.AssignEscalationTelventa(oNewCase.Id,'Telventa');
        
        obj01.GetCaseInterlocutorTelventa('Gerente Telventa', oNewAccount.Id,'Telventa');
        obj01.AssignEscalationTelventa(oNewCase.Id,'Telventa');
        
        obj01.GetCaseInterlocutorTelventa('Director Telventa', oNewAccount.Id,'Telventa');
        obj01.AssignEscalationTelventa(oNewCase.Id,'Telventa');
        
        /*Method for trigger*/
        Set<String> setAccounts = new Set<String>();
		setAccounts.add(oNewAccount.Id);
        
        CS_CASEASSIGNMENT_CLASS obj01Trigger = new CS_CASEASSIGNMENT_CLASS(setAccounts);        
        obj01Trigger.GetCaseInterlocutorTelventaTrigger('Agente Telventa', null,'Telventa');
        obj01Trigger.AssignEscalationTelventaTrigger('Agente Telventa', null,'Telventa');
        
        obj01Trigger.GetCaseInterlocutorTelventaTrigger('Agente Telventa', oNewAccount.Id,'Telventa');
        obj01Trigger.AssignEscalationTelventaTrigger('Agente Telventa', oNewCase.Id,'Telventa');
        
        obj01Trigger.GetCaseInterlocutorTelventaTrigger('Supervisor Telventa', oNewAccount.Id,'Telventa');
        obj01Trigger.AssignEscalationTelventaTrigger('Supervisor Telventa', oNewCase.Id,'Telventa');
        
        obj01Trigger.GetCaseInterlocutorTelventaTrigger('Gerente Telventa', oNewAccount.Id,'Telventa');
        obj01Trigger.AssignEscalationTelventaTrigger('Gerente Telventa', oNewCase.Id,'Telventa');
        
        obj01Trigger.GetCaseInterlocutorTelventaTrigger('Director Telventa', oNewAccount.Id,'Telventa');
        obj01Trigger.AssignEscalationTelventaTrigger('Director Telventa', oNewCase.Id,'Telventa');
        
    }
    
    /**
    * Method for test the method test_GetCaseInterlocutorPreVenta and AssignEscalationPreVenta
    * for case generate and trigger case force
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetCaseInterlocutorPreVenta()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase; 
        
        CS_CASEASSIGNMENT_CLASS obj01 = new CS_CASEASSIGNMENT_CLASS();
        obj01.GetCaseInterlocutorPreVenta('Representante de Ventas', null, 'Preventa');
        obj01.AssignEscalationPreVenta(null, 'Preventa');
        
        obj01.GetCaseInterlocutorPreVenta('Representante de Ventas', oNewAccount.Id, 'Preventa');
        obj01.AssignEscalationPreVenta(oNewCase.Id, 'Preventa');
        
        obj01.GetCaseInterlocutorPreVenta('Supervisor Ventas', oNewAccount.Id, 'Preventa');
        obj01.AssignEscalationPreVenta(oNewCase.Id, 'Preventa');
        
        obj01.GetCaseInterlocutorPreVenta('Gerente Ventas', oNewAccount.Id, 'Preventa');
        obj01.AssignEscalationPreVenta(oNewCase.Id, 'Preventa');
        
        obj01.GetCaseInterlocutorPreVenta('Director Ventas', oNewAccount.Id, 'Preventa');
        obj01.AssignEscalationPreVenta(oNewCase.Id, 'Preventa');
        
        /*Method for trigger*/
        Set<String> setAccounts = new Set<String>();
		setAccounts.add(oNewAccount.Id);
        
        CS_CASEASSIGNMENT_CLASS obj01Trigger = new CS_CASEASSIGNMENT_CLASS(setAccounts);
        obj01Trigger.GetCaseInterlocutorPreVentaTrigger('Representante de Ventas', null, 'Preventa');
        obj01Trigger.AssignEscalationPreVentaTrigger('Representante de Ventas', null, 'Preventa');
        
        obj01Trigger.GetCaseInterlocutorPreVentaTrigger('Representante de Ventas', oNewAccount.Id, 'Preventa');
        obj01Trigger.AssignEscalationPreVentaTrigger('Representante de Ventas', oNewCase.Id, 'Preventa');
        
        obj01Trigger.GetCaseInterlocutorPreVentaTrigger('Supervisor Ventas', oNewAccount.Id, 'Preventa');
        obj01Trigger.AssignEscalationPreVentaTrigger('Supervisor Ventas', oNewCase.Id, 'Preventa');
        
        obj01Trigger.GetCaseInterlocutorPreVentaTrigger('Gerente Ventas', oNewAccount.Id, 'Preventa');
        obj01Trigger.AssignEscalationPreVentaTrigger('Gerente Ventas', oNewCase.Id, 'Preventa');
        
        obj01Trigger.GetCaseInterlocutorPreVentaTrigger('Director Ventas', oNewAccount.Id, 'Preventa');
        obj01Trigger.AssignEscalationPreVentaTrigger('Director Ventas', oNewCase.Id, 'Preventa');
    }
    
    /**
    * Method for test the method GetCaseInterlocutorCoolers and AssignEscalationCoolers
    * for case generate and trigger case force
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetCaseInterlocutorCoolers()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
              
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        
        CS_CASEASSIGNMENT_CLASS obj01 = new CS_CASEASSIGNMENT_CLASS();
        obj01.GetCaseInterlocutorCoolers('Jefe Equipo Frio', null, 'Cooler');
        obj01.AssignEscalationCoolers(null, 'Cooler', UserInfo.getUserId());
        
        obj01.GetCaseInterlocutorCoolers('Jefe Equipo Frio', oNewAccount.Id, 'Cooler');
        obj01.AssignEscalationCoolers(oNewCase.Id, 'Cooler', UserInfo.getUserId());
                
        obj01.GetCaseInterlocutorCoolers('Supervisor Equipo Frio', oNewAccount.Id, 'Cooler');
        obj01.AssignEscalationCoolers(oNewCase.Id, 'Cooler', UserInfo.getUserId());
        
        obj01.GetCaseInterlocutorCoolers('Director Equipo Frio', oNewAccount.Id, 'Cooler');
        obj01.AssignEscalationCoolers(oNewCase.Id, 'Cooler', UserInfo.getUserId());
        
        obj01.GetCaseInterlocutorCoolers('Gerente Equipo Frio', oNewAccount.Id, 'Cooler');
        obj01.AssignEscalationCoolers(oNewCase.Id, 'Cooler', UserInfo.getUserId());
        
        /*Method for trigger*/
        Set<String> setAccounts = new Set<String>();
		setAccounts.add(oNewAccount.Id);
        
        CS_CASEASSIGNMENT_CLASS obj01Trigger = new CS_CASEASSIGNMENT_CLASS(setAccounts);
        obj01Trigger.GetCaseInterlocutorCoolersTrigger('Jefe Equipo Frio', null, 'Cooler');
        obj01Trigger.AssignEscalationCoolersTrigger('Jefe Equipo Frio', null, 'Cooler', UserInfo.getUserId());
        
        obj01Trigger.GetCaseInterlocutorCoolersTrigger('Jefe Equipo Frio', oNewAccount.Id, 'Cooler');
        obj01Trigger.AssignEscalationCoolersTrigger('Jefe Equipo Frio', oNewCase.Id, 'Cooler', UserInfo.getUserId());
                
        obj01Trigger.GetCaseInterlocutorCoolersTrigger('Supervisor Equipo Frio', oNewAccount.Id, 'Cooler');
        obj01Trigger.AssignEscalationCoolersTrigger('Jefe Equipo Frio', oNewCase.Id, 'Cooler', UserInfo.getUserId());
        
        obj01Trigger.GetCaseInterlocutorCoolersTrigger('Director Equipo Frio', oNewAccount.Id, 'Cooler');
        obj01Trigger.AssignEscalationCoolersTrigger('Jefe Equipo Frio', oNewCase.Id, 'Cooler', UserInfo.getUserId());
        
        obj01Trigger.GetCaseInterlocutorCoolersTrigger('Gerente Equipo Frio', oNewAccount.Id, 'Cooler');
        obj01Trigger.AssignEscalationCoolersTrigger('Jefe Equipo Frio', oNewCase.Id, 'Cooler', UserInfo.getUserId());                  
    }
    
    /**
    * Method for test the method GetCaseInterlocutorTellecolection and AssignEscalationTellecolection
    * for case generate and trigger case force
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetCaseInterlocutorTellecolection()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        
        CS_CASEASSIGNMENT_CLASS obj01 = new CS_CASEASSIGNMENT_CLASS();
        obj01.GetCaseInterlocutorTellecolection('Telecobrador', null,'Telecobranza');
        obj01.AssignEscalationTellecolection(null,'Telecobranza');
        
        obj01.GetCaseInterlocutorTellecolection('Telecobrador', oNewAccount.Id,'Telecobranza');
        obj01.AssignEscalationTellecolection(oNewCase.Id,'Telecobranza');
        
        obj01.GetCaseInterlocutorTellecolection('Supervisor Telecobranza', oNewAccount.Id,'Telecobranza');
        obj01.AssignEscalationTellecolection(oNewCase.Id,'Telecobranza');
        
        obj01.GetCaseInterlocutorTellecolection('Gerente Telecobranza', oNewAccount.Id,'Telecobranza');
        obj01.AssignEscalationTellecolection(oNewCase.Id,'Telecobranza');
        
        obj01.GetCaseInterlocutorTellecolection('Director Telecobranza', oNewAccount.Id,'Telecobranza');
        obj01.AssignEscalationTellecolection(oNewCase.Id,'Telecobranza');
        
        /*Method for trigger*/
        Set<String> setAccounts = new Set<String>();
		setAccounts.add(oNewAccount.Id);
        
        CS_CASEASSIGNMENT_CLASS obj01Trigger = new CS_CASEASSIGNMENT_CLASS(setAccounts);
        obj01Trigger.GetCaseInterlocutorTellecolectionTrigger('Telecobrador', null,'Telecobranza');
        obj01Trigger.AssignEscalationTellecolectionTrigger('Telecobrador', null,'Telecobranza');
        
        obj01Trigger.GetCaseInterlocutorTellecolectionTrigger('Telecobrador', oNewAccount.Id,'Telecobranza');
        obj01Trigger.AssignEscalationTellecolectionTrigger('Telecobrador', oNewCase.Id,'Telecobranza');
        
        obj01Trigger.GetCaseInterlocutorTellecolectionTrigger('Supervisor Telecobranza', oNewAccount.Id,'Telecobranza');
        obj01Trigger.AssignEscalationTellecolectionTrigger('Supervisor Telecobranza', oNewCase.Id,'Telecobranza');
        
        obj01Trigger.GetCaseInterlocutorTellecolectionTrigger('Gerente Telecobranza', oNewAccount.Id,'Telecobranza');
        obj01Trigger.AssignEscalationTellecolectionTrigger('Gerente Telecobranza', oNewCase.Id,'Telecobranza');
        
        obj01Trigger.GetCaseInterlocutorTellecolectionTrigger('Director Telecobranza', oNewAccount.Id,'Telecobranza');
        obj01Trigger.AssignEscalationTellecolectionTrigger('Director Telecobranza', oNewCase.Id,'Telecobranza');
    }
    
    /**
    * Method for test the method GetCaseInterlocutorCredit and AssignEscalationCredit
    * for case generate and trigger case force
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetCaseInterlocutorCredit()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        
        CS_CASEASSIGNMENT_CLASS obj01 = new CS_CASEASSIGNMENT_CLASS();
        obj01.GetCaseInterlocutorCredit('Agente Credito', null,'Credito');
        obj01.AssignEscalationCredit(null, 'Credito');
        
        obj01.GetCaseInterlocutorCredit('Agente Credito', oNewAccount.Id,'Credito');
        obj01.AssignEscalationCredit(oNewCase.Id, 'Credito');
        
        obj01.GetCaseInterlocutorCredit('Supervisor Credito', oNewAccount.Id,'Credito');
        obj01.AssignEscalationCredit(oNewCase.Id, 'Credito');
        
        obj01.GetCaseInterlocutorCredit('Gerente Credito', oNewAccount.Id,'Credito');
        obj01.AssignEscalationCredit(oNewCase.Id, 'Credito');
        
        obj01.GetCaseInterlocutorCredit('Director Credito', oNewAccount.Id,'Credito');
        obj01.AssignEscalationCredit(oNewCase.Id, 'Credito');
        
        /*Method for trigger*/
        Set<String> setAccounts = new Set<String>();
		setAccounts.add(oNewAccount.Id);
        
        CS_CASEASSIGNMENT_CLASS obj01Trigger = new CS_CASEASSIGNMENT_CLASS(setAccounts);
        obj01Trigger.GetCaseInterlocutorCreditTrigger('Agente Credito', null,'Credito');
        obj01Trigger.AssignEscalationCreditTrigger('Agente Credito', null, 'Credito');
        
        obj01Trigger.GetCaseInterlocutorCreditTrigger('Agente Credito', oNewAccount.Id,'Credito');
        obj01Trigger.AssignEscalationCreditTrigger('Agente Credito', oNewCase.Id, 'Credito');
        
        obj01Trigger.GetCaseInterlocutorCreditTrigger('Supervisor Credito', oNewAccount.Id,'Credito');
        obj01Trigger.AssignEscalationCreditTrigger('Supervisor Credito', oNewCase.Id, 'Credito');
        
        obj01Trigger.GetCaseInterlocutorCreditTrigger('Gerente Credito', oNewAccount.Id,'Credito');
        obj01Trigger.AssignEscalationCreditTrigger('Gerente Credito', oNewCase.Id, 'Credito');
        
        obj01Trigger.GetCaseInterlocutorCreditTrigger('Director Credito', oNewAccount.Id,'Credito');
        obj01Trigger.AssignEscalationCreditTrigger('Director Credito', oNewCase.Id, 'Credito');
    }    
}