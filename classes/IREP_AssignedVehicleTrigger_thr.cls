public with sharing class IREP_AssignedVehicleTrigger_thr {

	public static void updateMileageOnTour(list<ONTAP__AssignedVehicle__c> lstAssignedVehicle){
		set<Id> setTourIds = new set<Id>();
		for(ONTAP__AssignedVehicle__c objAssignedVehicle : lstAssignedVehicle)
			setTourIds.add(objAssignedVehicle.ONTAP__Tour__c);

		list<ONTAP__Tour__c> lstTour = [
				Select 	Id, IREP_EndMile__c, IREP_BeginMile__c, (Select Id, IREP_EndMile__c, IREP_BeginMile__c From ONTAP__Assigned_Vehicles__r)
				From	ONTAP__Tour__c
				Where	Id = :setTourIds
		];

		for(ONTAP__Tour__c objTour : lstTour){
			objTour.IREP_BeginMile__c = 0;
			objTour.IREP_EndMile__c = 0;

			for(ONTAP__AssignedVehicle__c objAssignedVehicle : objTour.ONTAP__Assigned_Vehicles__r){
				objTour.IREP_BeginMile__c += (objAssignedVehicle.IREP_BeginMile__c != null ? objAssignedVehicle.IREP_BeginMile__c : 0);
				objTour.IREP_EndMile__c += (objAssignedVehicle.IREP_EndMile__c != null ? objAssignedVehicle.IREP_EndMile__c : 0);
			}
		}

		if(!lstTour.isEmpty())
			update lstTour;
	}
}