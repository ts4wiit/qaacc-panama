/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: ONCALL_SAP_OrderResponseBatch.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 27/09/2019           Rogelio          Creation of Entities.
*/
public class ONCALL_SAP_OrderResponseBatch{
	public Integer statusCode;	//200
	public String message;	//Request Placed, an email notification will be send after update process finish.
	public boolean success;
	public static ONCALL_SAP_OrderResponseBatch parse(String json){
		return (ONCALL_SAP_OrderResponseBatch) System.JSON.deserialize(json, ONCALL_SAP_OrderResponseBatch.class);
	}

/*	static testMethod void testParse() {
		String json=		'{'+
		'  "statusCode": 200,'+
		'  "message": "Request Placed, an email notification will be send after update process finish.",'+
		'  "success": true'+
		'}';
		ONCALL_SAP_OrderResponseBatch obj = parse(json);
		System.assert(obj != null);
	}*/
}