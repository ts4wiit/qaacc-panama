/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteAllOpenItSchudeler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Carlos Leal        Creation of methods.
 */
@isTest
private class V360_DeleteAllOpenItSchudeler_Test{
  /**
   * Method for test theSchudeler for open items.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
  @isTest static void test_execute_UseCase1(){
    V360_DeleteAllOpenItSchudeler obj01 = new V360_DeleteAllOpenItSchudeler();
    SchedulableContext sc;
    obj01.execute(sc);
  }
}