/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_ANSWERCASE_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * 
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 07/02/2019           Gabriel E Garcia       Test class of CS_ANSWERCASE_CLASS 
 */

@isTest
private class CS_AnswerCase_Class_Test 
{
    
    /**
    * Method Config Setup
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @testSetup static void setup() 
    {
        test.startTest();
        Account accountTest = new Account(Name = 'Account Test');   
        insert accountTest;
        
        ISSM_TypificationMatrix__c typificationMTest = new ISSM_TypificationMatrix__c();
        typificationMTest.CS_Days_to_End__c = 10;
        insert typificationMTest;
        
        CS_Question_Typification_Matrix__c question = new CS_Question_Typification_Matrix__c();
        question.CS_Country__c = 'Honduras';
        question.CS_Question__c = '¿Pregunta Test?';
        question.CS_Question_Type__c = 'Text';
        question.CS_Typification_Matrix_Number__c = typificationMTest.Id;        
        insert question;        
            
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Test Subject';
        caseTest.ISSM_TypificationNumber__c = typificationMTest.Id;
        insert caseTest;
        test.stopTest();
    }

    /**
    * Method for test the method getQuestionsAnswers
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_getQuestionsAnswers()
    {       
       	Account accountTest1 = new Account(Name = 'Account Test Question');   
        insert accountTest1;
        
        ISSM_TypificationMatrix__c typificationMTest1 = new ISSM_TypificationMatrix__c();
        typificationMTest1.CS_Days_to_End__c = 10;
        insert typificationMTest1;
        
        Case caseTest1 = new Case();
        caseTest1.Accountid = accountTest1.Id;
        caseTest1.Description = 'Test Description for Question';
        caseTest1.Status = 'New';
        caseTest1.Subject = 'Test Subject for Question';
        caseTest1.ISSM_TypificationNumber__c = typificationMTest1.Id;
        insert caseTest1;  
        
        CS_Question_Typification_Matrix__c question1 = new CS_Question_Typification_Matrix__c();
        question1.CS_Country__c = 'El Salvador';
        question1.CS_Question__c = '¿Pregunta Test Question?';
        question1.CS_Question_Type__c = 'Text';
        question1.CS_Typification_Matrix_Number__c = typificationMTest1.Id;
        insert question1;        
  
        CS_AnswerCase_Class.getQuestionsAnswers(caseTest1.Id);
        
    }
    
    /**
    * Method for test the method saveAnswers
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_saveAnswers()
    {       
        Case caseTest = [SELECT Id FROM Case LIMIT 1];
		CS_Question_Typification_Matrix__c question = [SELECT Id FROM CS_Question_Typification_Matrix__c LIMIT 1];                        
        List<CS_AnswerCase__c> listCase = new List<CS_AnswerCase__c>();
        
        CS_AnswerCase__c answer = new CS_AnswerCase__c();
        answer.Answer__c = 'Answer test';
        answer.Case__c = caseTest.Id; 
        answer.TypificationMatrixQuestions__c = question.Id;        
        insert answer;
        
        listCase.add(answer);        
        String listCaseJSON = JSON.serialize(listCase);     
        
        Test.startTest();                      
        	CS_AnswerCase_Class.saveAnswers(listCaseJSON);   
        
        	listCaseJSON = '';
        	
        	CS_AnswerCase_Class.saveAnswers(listCaseJSON);   
        
        Test.stopTest();
        
    }
}