/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase:HONES_TelecollectioncampaignBatch_Test.apxc
 * Versión: 1.0.0.0
 * 
 * 
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 17/01/2019    Heron Zurita	  heron.zurita@accenture.com     Creación de la clase  
 */
@isTest
private class HONES_TelecollectioncampaignBatch_Test{
    /*
     * test Method which get the payment telecollection by id
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/    
    @isTest static void testBatch(){
        
        User oUpdateUserOne = new User();
        oUpdateUserOne.Country__c = 'El Salvador';
        oUpdateUserOne.Username= 'juan@acc.com'; 
        oUpdateUserOne.LastName= 'Sawuan'; 
        oUpdateUserOne.Email= 'juan@acc.com'; 
        oUpdateUserOne.Alias= 'Juanis'; 
        oUpdateUserOne.CommunityNickname = 'jusuw';
        oUpdateUserOne.TimeZoneSidKey= 'America/Bogota'; 
        oUpdateUserOne.LocaleSidKey= 'es';
        oUpdateUserOne.EmailEncodingKey = 'ISO-8859-1';
        oUpdateUserOne.AboutMe = 'yas';
        oUpdateUserOne.LanguageLocaleKey = 'es';
        oUpdateUserOne.ProfileId= userinfo.getProfileId();
        insert oUpdateUserOne;
        
        List<Account> listAcc = new List<Account>();
        List<ONTAP__OpenItem__c> listOpI = new List<ONTAP__OpenItem__c>();
        
        Account accM = new Account();
        accM.Name = 'Test';
        accM.V360_Tellecolector__c = oUpdateUserOne.Id;
        accM.HONES_Telecollection_Frequency__c = 'Monday';
        accM.HONES_CreditRating__c='RCC';
        accM.ONTAP__ExternalKey__c='SVXXXXX';
        insert accM;
        listAcc.add(accM);
        
        Account accTu = new Account();
        accTu.Name = 'Test';
        accTu.V360_Tellecolector__c = oUpdateUserOne.Id;
        accTu.HONES_Telecollection_Frequency__c = 'Tuesday';
        accTu.HONES_CreditRating__c='RCC';
        accTu.ONTAP__ExternalKey__c='SV12345';
        insert accTu;
        listAcc.add(accTu);
        
        Account accW = new Account();
        accW.Name = 'Test';
        accW.V360_Tellecolector__c = oUpdateUserOne.Id;
        accW.HONES_Telecollection_Frequency__c = 'Wednesday';
        accW.HONES_CreditRating__c='RCC';
        accW.ONTAP__ExternalKey__c='SVXXXX4';
        insert accW;
        listAcc.add(accW);
        
        Account accT = new Account();
        accT.Name = 'Test';
        accT.V360_Tellecolector__c = oUpdateUserOne.Id;
        accT.HONES_Telecollection_Frequency__c = 'Thursday';
        accT.ONTAP__ExternalKey__c='HNXXXX5';
        insert accT;
        listAcc.add(accT);
        
        Account accF = new Account();
        accF.Name = 'Test';
        accF.V360_Tellecolector__c = oUpdateUserOne.Id;
        accF.HONES_Telecollection_Frequency__c = 'Friday';
        accF.ONTAP__ExternalKey__c='SVXXXX6';
        insert accF;
        listAcc.add(accF);
        
        ONTAP__OpenItem__c oiM = new ONTAP__OpenItem__c();
        oiM.ONTAP__DueDate__c = System.today()+15;
        //oiM.V360_ExpirationDate__c=System.today();
        oiM.ONTAP__Account__c = accM.id;
        oiM.ONTAP__Amount__c = Double.valueOf(999.9);
        insert oiM;
        listOpI.add(oiM);
        
        ONTAP__OpenItem__c oiTu = new ONTAP__OpenItem__c();
        oiTu.ONTAP__DueDate__c = date.valueOf('2018-11-11');
        oiTu.ONTAP__Account__c = accTu.id;
        oiTu.ONTAP__Amount__c = Double.valueOf(999.9);
        insert oiTu;
        listOpI.add(oiTu);
        
        ONTAP__OpenItem__c oiW = new ONTAP__OpenItem__c();
        oiW.ONTAP__DueDate__c = date.valueOf('2018-11-11');
        oiW.ONTAP__Account__c = accW.id;
        oiW.ONTAP__Amount__c = Double.valueOf(999.9);
        insert oiW;
        listOpI.add(oiW);
        
        ONTAP__OpenItem__c oiT = new ONTAP__OpenItem__c();
        oiT.ONTAP__DueDate__c = date.valueOf('2018-11-11');
        oiT.ONTAP__Account__c = accT.id;
        oiT.ONTAP__Amount__c = Double.valueOf(999.9);
        oiT.V360_BaseDate__c = date.valueOf('2019-01-01');
        oiT.V360_OverdueDays__c = 2.0;
        insert oiT;
        listOpI.add(oiT);
        
        ONTAP__OpenItem__c oiF = new ONTAP__OpenItem__c();
        oiF.ONTAP__DueDate__c = date.valueOf('2018-11-11');
        oiF.ONTAP__Account__c = accF.id;
        oiF.ONTAP__Amount__c =Double.valueOf(999.9);
        oiF.V360_BaseDate__c = date.valueOf('2019-01-01');
        oif.V360_OverdueDays__c = 2.0;
        insert oiF;
        listOpI.add(oiF);
        
        Account accYesterday = new Account();
        accYesterday.Name = 'Test';
        accYesterday.V360_Tellecolector__c = oUpdateUserOne.Id;
        accYesterday.HONES_Telecollection_Frequency__c = 'Friday';
        insert accYesterday;
        listAcc.add(accYesterday);
        
        ONTAP__OpenItem__c oiYesterday = new ONTAP__OpenItem__c();
        oiYesterday.ONTAP__DueDate__c = date.valueOf('2018-11-11');
        oiYesterday.ONTAP__Account__c = accM.id;
        oiYesterday.ONTAP__Amount__c = Double.valueOf(999.9);
        insert oiYesterday;
        listOpI.add(oiYesterday);
        
        Schema.DescribeFieldResult Call_StatusPick = ONCALL__Call__c.ONCALL__Call_Type__c.getDescribe();
            List<Schema.PicklistEntry> Call_StatusPick_list = Call_StatusPick.getPicklistValues();
            Map<String, String> Call_StatusPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : Call_StatusPick_list) {
                Call_StatusPick_values.put(v.getLabel(),v.getValue());
            }
        
        ONCALL__Call__c call = new ONCALL__Call__c();
        call.Name = 'Test';
        call.ONCALL__Call_Status__c = 'Pending to call';
        //call.CreatedDate = date.valueOf('2018-11-11');
        call.ONCALL__Call_Type__c = Call_StatusPick_values.get(Label.Telecollection);
        call.RecordTypeId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
        call.ONCALL__POC__c = accM.Id;
        
        insert call;
        test.setCreatedDate(call.id, date.valueOf('2018-11-11'));
        List<ONCALL__Call__c> callList = new List<ONCALL__Call__c>();
        List<sObject> scop = new List<sObject>();
        
        System.debug('Lista Open: '+listOpI.size());
        callList.add(call);
        scop.addAll(listAcc);
        System.debug('Lista de cuenta: '+listAcc);
        scop.addAll(callList);
        //scop.addAll(listOpI);
        System.debug('Scop:'+ scop);
        
        Test.startTest();
		HONES_TelecollectioncampaignBatch obj01 = new HONES_TelecollectioncampaignBatch();
        Database.BatchableContext BC;
	    obj01.execute(BC, scop);
  		Id batchprocessId = Database.executeBatch(obj01, 200);
        Test.stopTest();
    }
}