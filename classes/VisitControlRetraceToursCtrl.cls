/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControlRetraceToursCtrl.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class VisitControlRetraceToursCtrl {


   public ApexPages.StandardSetController setCtrl;

    public List <WrapperError> routesErrorList {get; set;}
    public List<SelectOption> typeOfskipt {get;set;}
    public Set<Id> selectedIds {get; set;}

    public Date dateSelected {get; set;}
    public String typeOfSkiptSelected {get;set;}

    public Integer sizeErrors{get;set;}
    public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    public Boolean error {get;set;}

    /**
    * Front Method to add ids
    * Created By: g.martinez.cabral@accenture.com
    * @param ApexPages.StandardSetController controller
    * @return void
    */
    public VisitControlRetraceToursCtrl(ApexPages.StandardSetController controller){
        setCtrl = controller;
        selectedIds = new Set<Id>();
        typeOfskipt = getPickListValuesIntoList();
        routesErrorList = new List<WrapperError>();


        for(ONTAP__Route__c route: (ONTAP__Route__c[])setCtrl.getSelected()){
                selectedIds.add(route.Id);
        }       
    }
    
    /**
    * Front Method to pass values for a pick list
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return list<SelectOption>
    */
    public list<SelectOption> getPickListValuesIntoList(){
        list<SelectOption> opt = new list<SelectOption>();

        schema.DescribeFieldResult fd = VisitControl_RetraceAndAnticipateRoute__c.VisitControl_RetraceOrAnticipate__c.getdescribe();

        list<PicklistEntry> pc = fd.getpicklistvalues();

        for(PicklistEntry f: pc){

            opt.add(new selectoption(f.getvalue(),f.getLabel()));

        }

        return opt;
    }
    
    /**
    * Front Method to pass the routes
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public ApexPages.StandardSetController routes {
        get {
            if(routes == null) {
                routes = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Name, ONTAP__SalesOfficeId__c, ONTAP__RouteId__c, ONTAP__RouteName__c ,RouteManager__c   FROM ONTAP__Route__c WHERE Id IN :selectedIds]));
                size = 100;
                routes.setPageSize(size);
                noOfRecords = routes.getResultSize();    
            }
            return routes;
        }
        private set;
    }
  
    /**
    * Front Method to add the routes to accounts
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    Public List<ONTAP__Route__c> getRoutesToRetrace(){
        List<ONTAP__Route__c> accList = new List<ONTAP__Route__c>();
        for(ONTAP__Route__c a : (List<ONTAP__Route__c>)routes.getRecords())
            accList.add(a);
        return accList;
    }
     
    /**
    * Front Method to refesh the pages
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public pageReference refresh() {
        routes = null;
        getRoutesToRetrace();
        routes.setPageNumber(1);
        return null;
    }
     
    /**
    * Front Method to go next
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public Boolean hasNext {
        get {
            return routes.getHasNext();
        }
        set;
    }
    
    /**
    * Front Method to go previous
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public Boolean hasPrevious {
        get {
            return routes.getHasPrevious();
        }
        set;
    }
  
    /**
    * Front Method to go the page number
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public Integer pageNumber {
        get {
            return routes.getPageNumber();
        }
        set;
    }
  
    /**
    * Front Method to go to first
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public void first() {
        routes.first();
    }
  
    /**
    * Front Method to go to last
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public void last() {
        routes.last();
    }
  
    /**
    * Front Method to go to previous
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public void previous() {
        routes.previous();
    }
  
    /**
    * Front Method to go to next
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public void next() {
        routes.next();
    }

    /**
    * Front Method to process the route
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return PageReference
    */
    public PageReference saveList(){
        sizeErrors=0;
        error = false;
        List <VisitControl_RetraceAndAnticipateRoute__c> objList = new List<VisitControl_RetraceAndAnticipateRoute__c>();
        List <VisitControl_RetraceAndAnticipateRoute__c> existentList = [
            SELECT Id, VisitControl_Route__c, VisitControl_DateToSkip__c, VisitControl_Route__r.Name, VisitControl_Route__r.ONTAP__SalesOfficeId__c, VisitControl_Route__r.ONTAP__RouteId__c, 
            VisitControl_Route__r.ONTAP__RouteName__c , VisitControl_Route__r.RouteManager__c  
            FROM  VisitControl_RetraceAndAnticipateRoute__c 
            WHERE VisitControl_Route__c IN :selectedIds  
            /*AND VisitControl_Deleted__c = FALSE*/]; //Removed flag "VisitControl_Deleted__c" to avoid creation of records in the same date 
        routesErrorList =new List<WrapperError>();
        DateTime dateTimeSelected;
        
        if(dateSelected != NULL){
        Integer d = dateSelected.day();
        Integer mo = dateSelected.month();
        Integer yr = dateSelected.year();
            dateTimeSelected = DateTime.newInstance(yr, mo, d);
        }
        
        /*if (dateSelected != NULL && dateTimeSelected != NULL &&typeOfSkiptSelected!=NULL && typeOfSkiptSelected=='Anticipate' &&  dateTimeSelected.format('EEEE')!= 'Monday'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'- Las rutas que desea anticipar deben comenzar el lunes de cada semana.'));
            error=true;
        }*/
        
        if(dateSelected!= NULL && dateSelected<=System.today()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,LABEL.VisitControl_RetraceInFutureValidation));
            sizeErrors=1;
            error=true;
        }
    
        for(VisitControl_RetraceAndAnticipateRoute__c  routeSkipedInDate : existentList){
           Date dateToSkip = routeSkipedInDate.VisitControl_DateToSkip__c;
           if(dateSelected!=NULL && dateToSkip.toStartOfWeek() == dateSelected.toStartOfWeek()){
               routesErrorList.add(new WrapperError(routeSkipedInDate.VisitControl_Route__r.Name, LABEL.VisitControl_RetraceRouteDuplicated  + dateSelected));
           }
        }
        
        sizeErrors = routesErrorList.size();
        if(routesErrorList.isEmpty() && !error){

            for(ONTAP__Route__c route: (ONTAP__Route__c[])setCtrl.getSelected()){

                objList.add(new VisitControl_RetraceAndAnticipateRoute__c(VisitControl_Route__c = route.Id,VisitControl_DateToSkip__c=dateSelected,VisitControl_RetraceOrAnticipate__c=typeOfSkiptSelected));
            }

            Database.SaveResult[] srList = Database.insert(objList, false);
            String failedDML ='';
            for(Integer i=0;i<srList.size();i++){
                if (srList.get(i).isSuccess()){
                    srList.get(i).getId();

                }else if (!srList.get(i).isSuccess()){
                    // DML operation failed
                    Database.Error error = srList.get(i).getErrors().get(0);
                    String routeId = objList.get(i).VisitControl_Route__c;
                    //failed record from the list
                    failedDML += 'Error '+ error.getMessage();
                    WrapperError wp = new WrapperError(routeId,failedDML);
                    routesErrorList.add(wp);

                }
            }
            sizeErrors = routesErrorList.size();
            if(sizeErrors>0){

                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,LABEL.VisitControl_RetraceRouteError));

            }else{

                PageReference orderPage = new PageReference('/' + VisitControl_RetraceAndAnticipateRoute__c.sObjectType.getDescribe().getKeyPrefix());
                orderPage.setRedirect(true);
                return orderPage;
            }
        }else{
                
            if(!error){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,LABEL.VisitControl_RetraceRouteError));}
                
        }
        return null;
    }

    /**
    * Front Getter and setter of wrapper
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public class WrapperError {

        public String route {get; set;}
        public String error {get; set;}

        public WrapperError(String route, String error){
            this.route=route;
            this.error =error;
        }
    }

}