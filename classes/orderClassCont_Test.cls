/* ----------------------------------------------------------------------------
* AB InBev :: Oncall
* ----------------------------------------------------------------------------
* Clase: orderClassCont_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 21/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class orderClassCont_Test {
    /**
    * Test method for search a text in accounts
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void Search_Test_Case1(){
        Account test = new Account();
        test.Name = 'a';
        test.Jigsaw = 'a';
        insert test;
		List<String> ids = orderClassCont.searchForIds('a');
    }
}