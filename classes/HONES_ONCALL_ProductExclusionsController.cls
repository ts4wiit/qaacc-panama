/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase:HONES_ONCALL_ProductExclusionsController.apxc
* Versión: 1.0.0.0
* 
* 
* 
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto      						Descripción
* 18/10/2019    Daniel Hernández	dhernandez@ts4.mx     			Creación de la clase  
*/
public class HONES_ONCALL_ProductExclusionsController {
    
    public static User usr = [SELECT ONTAP__Country_Alias__c from User where id =: userInfo.getUserId()];
    public static String Country = usr.ONTAP__Country_Alias__c + '%';
    
    /**
    * Method to get info displayed on individual exclusions per client
    * Contact suport By: dhernandez@ts4.mx
    * Created By: Daniel Hernández Díaz
    * @param String 10 characters with SAP code of client
    * @return Account
    */
    @AuraEnabled
    public static Account getClient(String Id_SAP){
        return [SELECT Id, Name, ONTAP__LegalName__c, V360_BASISNum__c, ONTAP__SAP_Number__c, IREP_DistributionCenter__c, ONCALL__KATR9__c, ONCALL__KATR3__c 
                FROM Account WHERE ONTAP__SAP_Number__c =:Id_SAP];
    }
    
    /**
    * Method to get a simple recount of record affected with the exclussion selected
    * Contact suport By: dhernandez@ts4.mx
    * Created By: Daniel Hernández Díaz
    * @params String 
    * @return Account
    */
    @AuraEnabled
    public static integer getCountClients(String DistributionCenter, String Subchannel, String Attr3){
        integer clientsReturned = 0;
        
        String query = 'SELECT COUNT() FROM Account ';
        if(DistributionCenter != '--Todos--' ){
            if(query.indexOf('WHERE') == -1){query = query + ' WHERE ';}
            query = query + ' IREP_DistributionCenter__c LIKE \'' + DistributionCenter.substring(0,4) + '%\'';
        }
        if(Subchannel != '--Todos--' ){
            if(query.indexOf('WHERE') == -1){query = query + ' WHERE ';}
            else{ query = query + ' AND '; }
            query = query + ' ONCALL__KATR9__c LIKE \'' + Subchannel.substring(0,3) + '%\'';
        }
        if(Attr3 != '--Todos--' ){
            if(query.indexOf('WHERE') == -1){query = query + ' WHERE ';}
            else{ query = query + ' AND '; }
            query = query + ' ONCALL__KATR3__c LIKE \'' + Attr3.substring(0,2) + '%\'';
        }
        if(query.indexOf('WHERE') == -1){query = query + ' WHERE ';}
        else{ query = query + ' AND '; }
        query = query + ' ONTAP__ExternalKey__c LIKE \'' + Country + '\'';
        system.debug('>>>>>>>>>>> query:: ' + query);
        
        return database.countQuery(query);
    }
    
    
    @AuraEnabled
    public static List<HONES_ProductPerClient__c> getProductExcludedPerClient(Id id_client){
        return [SELECT Id, HONES_RelatedClient__c, HONES_RelatedProduct__c, SAP_SKU_ID__c, SAP_Account_ID__c 
                FROM HONES_ProductPerClient__c 
                WHERE HONES_RelatedClient__c = :id_client];
    }

    /**
    * Get a client of reference with a match on the massive key and return all the products excluded for that client
    * Contact suport By: dhernandez@ts4.mx
    * Created By: Daniel Hernández Díaz
    * @params String x 3 
    * @return List<HONES_ProductPerClient__c>
    */
    @AuraEnabled
    public static List<HONES_ProductPerClient__c> getMassiveProductExcluded(String DistributionCenter, String Subchannel, String Attr3){
        Id id_recordTypeByGroup = [SELECT Id FROM RecordType WHERE DeveloperName='Grupo_Cliente'].Id;
        String massiveKey = DistributionCenter +'|'+  Subchannel +'|'+  Attr3;
        
        // trata de recuperar los productos que originalmente fueron insertados bajo una llave:
        List<HONES_ProductPerClient__c> clientReference = [SELECT HONES_RelatedClient__c FROM HONES_ProductPerClient__c 
                               WHERE RecordTypeId = :id_recordTypeByGroup AND HONES_MassiveKey__c =:massiveKey  LIMIT 1];
        
        List<HONES_ProductPerClient__c> exclusions = new List<HONES_ProductPerClient__c>();
        if(!clientReference.isEmpty()){
            exclusions = [SELECT Id, HONES_RelatedClient__c, HONES_RelatedProduct__c, SAP_SKU_ID__c, SAP_Account_ID__c, HONES_MassiveKey__c 
                          FROM HONES_ProductPerClient__c 
                          WHERE HONES_RelatedClient__c =: clientReference[0].HONES_RelatedClient__c AND RecordTypeId = :id_recordTypeByGroup 
                          AND HONES_MassiveKey__c =:massiveKey];
        }
        return exclusions;
    }

	@AuraEnabled
    public static void saveExclusionPerClient(Account client, List<Id> id_products){
        Id id_recordTypeByClient = [SELECT Id FROM RecordType WHERE DeveloperName='Wiit_ExProductClient'].Id;
        
        Map<Id, ONTAP__Product__c> selectedProducts = new Map <Id, ONTAP__Product__c>([SELECT Id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c FROM ONTAP__Product__c WHERE Id IN :id_products]);
        Map<Id, HONES_ProductPerClient__c> currentExclusions = new Map<Id, HONES_ProductPerClient__c>([SELECT Id, HONES_RelatedClient__c, HONES_RelatedProduct__c, SAP_Account_ID__c, SAP_SKU_ID__c, RecordTypeId, HONES_MassiveKey__c 
                                                                                                       FROM HONES_ProductPerClient__c WHERE HONES_RelatedClient__c = :client.Id]);
        Map<Id, HONES_ProductPerClient__c> exclusionsToDelete = new Map<Id, HONES_ProductPerClient__c>();
        Map<Id, HONES_ProductPerClient__c> exclusionsPerProduct = new Map<Id, HONES_ProductPerClient__c>(); // usa como llave el id del producto, en lugar del id de la exclusión
        Map<Id, HONES_ProductPerClient__c> exclusionsToUpsert = new Map<Id, HONES_ProductPerClient__c>();
        
        for(HONES_ProductPerClient__c currentExclusion : currentExclusions.values()){
            ONTAP__Product__c selectedProduct = selectedProducts.get(currentExclusion.HONES_RelatedProduct__c);
            if(selectedProduct == null){ 
                // si una exclusión previa no ha sido seleccionada, es eliminada
                exclusionsToDelete.put(currentExclusion.Id, currentExclusion);
            }else if(currentExclusion.RecordTypeId != id_recordTypeByClient || currentExclusion.HONES_MassiveKey__c != client.ONTAP__SAP_Number__c){ 
                // ... de  otra  forma se deberá actualizar
                currentExclusion.RecordTypeId = id_recordTypeByClient;
                currentExclusion.HONES_MassiveKey__c = client.ONTAP__SAP_Number__c;
                exclusionsToUpsert.put(currentExclusion.Id, currentExclusion);
                exclusionsPerProduct.put(selectedProduct.Id, currentExclusion);
            }
        }
        
        for(ONTAP__Product__c prod : selectedProducts.values()){ // crea un nuevo registro si no se halló coincidencia
            HONES_ProductPerClient__c exclusion = exclusionsPerProduct.get(prod.Id);
            if(exclusion == null){
                exclusion = new HONES_ProductPerClient__c();
                exclusion.RecordTypeId = id_recordTypeByClient;
                exclusion.HONES_MassiveKey__c = 'cliente ' + client.ONTAP__SAP_Number__c;
                exclusion.HONES_RelatedProduct__c = prod.Id;
                exclusion.SAP_SKU_ID__c = prod.ONTAP__ProductCode__c;
                exclusion.HONES_RelatedClient__c = client.Id;
                exclusion.SAP_Account_ID__c = client.ONTAP__SAP_Number__c;
                exclusionsToUpsert.put(prod.Id,exclusion);
            }
        }
        
        try{
            delete exclusionsToDelete.values();
            upsert exclusionsToUpsert.values();
        } catch (DmlException e) {
            System.debug('==== Se produjo un error al modificar registros: ' + e.getMessage());
        }
        
        return;
    }
    
    @AuraEnabled
    public static void saveMassiveExclusion(String searchCriteria, List<Id> id_products, Integer clientsReturned, Integer nInitialProducts){
        //ejecuta un proceso similar del método saveExclusionPerClient() pero a través de un proceso batch
        Map<Id, ONTAP__Product__c> selectedProducts = new Map <Id, ONTAP__Product__c>([SELECT Id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c FROM ONTAP__Product__c WHERE Id IN :id_products]);
        
        Integer batchSize = nInitialProducts > id_products.size()? nInitialProducts :id_products.size();
        
        HONES_ONCALL_MasiveProductExclusion exclude = new HONES_ONCALL_MasiveProductExclusion(searchCriteria, Country, selectedProducts); 
        if(batchSize > 0){
            if(batchSize * clientsReturned > 6000 ){
                batchSize = 6000 / batchSize;
            }else{
                batchSize = batchSize * clientsReturned;
            }
        }
        system.debug('>>> batchSize: ' + batchSize);
        system.debug('>>> clientsReturned: ' + clientsReturned);
        
        database.executeBatch(exclude, batchSize); 
                
        return;
    }
    

    @AuraEnabled
    public static List<ONTAP__Product__c> getProducts(String searchKey){
        List<ONTAP__Product__c> ProductList = new List<ONTAP__Product__c>();
        if(searchKey=='%%'){
            ProductList = [SELECT Id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c 
                           FROM ONTAP__Product__c 
                           WHERE ONTAP__ProductCode__c LIKE '10%' AND ONTAP__ProductCode__c!=null AND ONTAP__MaterialProduct__c!=null 
                           ORDER BY ONTAP__ProductCode__c LIMIT 50];
        }else{
            ProductList = [SELECT Id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c 
                           FROM ONTAP__Product__c 
                           WHERE (ONTAP__ProductCode__c LIKE: searchKey OR ONTAP__MaterialProduct__c LIKE: searchKey) AND ONTAP__ProductCode__c!=null AND ONTAP__MaterialProduct__c!=null 
                           ORDER BY ONTAP__ProductCode__c LIMIT 50];
        }
        return ProductList;
    }
    
    @AuraEnabled
    public static List<String> getPickListValuesDistributionCenter(){
        List<Distribution_Center_Catalog__mdt> pickListValues = [SELECT MasterLabel, Country__c FROM Distribution_Center_Catalog__mdt 
                                                                 WHERE Country__c LIKE :Country ORDER BY MasterLabel];
        List<String> pickListValuesList = new List<String>();
        for(Distribution_Center_Catalog__mdt value : pickListValues){
            pickListValuesList.add(String.valueOf(value.MasterLabel));
        }
        pickListValuesList.add(0,'--Todos--');
        return pickListValuesList;
    }
    
    @AuraEnabled
    public static List<String> getPickListValuesSubchannel(){
        List<Subchannels_Catalog__mdt > pickListValues = [SELECT MasterLabel, Country__c FROM Subchannels_Catalog__mdt 
                                                          WHERE Country__c LIKE :Country ORDER BY MasterLabel];
        List<String> pickListValuesList = new List<String>();
        for(Subchannels_Catalog__mdt  value : pickListValues){
            pickListValuesList.add(String.valueOf(value.MasterLabel));
        }
        pickListValuesList.add(0,'--Todos--');
        return pickListValuesList;
    }
    
    @AuraEnabled
    public static List<String> getPickListValuesKATR3(){
        List<KATR3_Catalog__mdt > pickListValues = [SELECT MasterLabel, Country__c FROM KATR3_Catalog__mdt 
                                                    WHERE Country__c LIKE :Country ORDER BY MasterLabel];
        List<String> pickListValuesList = new List<String>();
        for(KATR3_Catalog__mdt  value : pickListValues){
            pickListValuesList.add(String.valueOf(value.MasterLabel));
        }
        pickListValuesList.add(0,'--Todos--');
        return pickListValuesList;
    }
}