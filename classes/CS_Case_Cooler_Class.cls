/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Class: CS_COOLERS_CALLOUT_cls.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 22/01/2019           Debbie Zacarias        Creation of the class for lightning component control
*/

public class CS_Case_Cooler_Class 
{
        
    /**
	* method to find materials list for lightning.
	* @author: d.zacarias.cantillo@accenture.com
	* @param void
	* @return List of materials
	*/ 
    @AuraEnabled
    public static List<String> getPickListValues()
    {
        List<String> pickListValuesList= new List<String>();
        String prodType = Label.CS_Cooler_Filter;
        User currentUser = [select Id, username, Country__c from User where Id = :UserInfo.getUserId()];
        
        String country = '';
        String countryESV = Label.CS_Country_El_Salvador;
        String countryHN = Label.CS_Country_Honduras;
        
        if(currentUser.Country__c.equals(countryESV))
            country = Label.CS_CountryCodeSV;
        else if(currentUser.Country__c.equals(countryHN))
            country = Label.CS_CountryCodeHN;
        
        try
        {            
            List<ONTAP__Product__c> acc = [SELECT ONTAP__MaterialProduct__c, ONTAP__ProductType__c  
                                           FROM ONTAP__Product__c 
                                           WHERE ONTAP__ProductType__c =:prodType AND ONTAP__Country_Code__c =: country Order by ONTAP__MaterialProduct__c];
            
            pickListValuesList.add('');
            
            for(ONTAP__Product__c item : acc)
            {
                pickListValuesList.add(item.ONTAP__MaterialProduct__c);
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLER_cls.getPickListValues  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLER_cls.getPickListValues  Cause: '   + ex.getCause());   
            System.debug('CS_CASE_COOLER_cls.getPickListValues  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLER_cls.getPickListValues  Stack trace: ' + ex.getStackTraceString());
        } 
        return pickListValuesList;
    }
    
    /**
	* method for gets material for case 
	* @author: d.zacarias.cantillo@accenture.com
	* @param String idCase to query product code
	* @return Product with the info to display 
	*/ 
    @AuraEnabled
    public static List<Object> SetProductCase(String idCase)
    {
        List<ONTAP__Product__c> products;
        List<Object> returnList = new List<Object>();
        ONTAP__Product__c product;
        try
        {
            Case cas = ([SELECT id, CS_skuProduct__c, ISSM_Count__c
                         FROM Case 
                         WHERE id =: idCase]);
            
            if(cas.CS_skuProduct__c != null || cas.CS_skuProduct__c != '')
            {
                product = ([SELECT Id, ONTAP__MaterialProduct__c, ONTAP__ProductCode__c
                             FROM ONTAP__Product__c 
                             WHERE ONTAP__ProductCode__c =: cas.CS_skuProduct__c
                             AND ONTAP__ProductCode__c != null
                             LIMIT 1]);
                
                returnList.add(cas);
                returnList.add(product);
                                
            }
            else
            {
                returnList = null;                
            }                    
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLER_cls.SetProductCase  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLER_cls.SetProductCase  Cause: '   + ex.getCause());   
            System.debug('CS_CASE_COOLER_cls.SetProductCase  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLER_cls.SetProductCase  Stack trace: ' + ex.getStackTraceString());            
        }       
        
        return returnList;
    }
    
    /**
	* method for update material case.
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase that is updated 
	* @param String for query material 
	* @return product with information to display
	*/ 
    @AuraEnabled
    public static ONTAP__Product__c GetProductCase(String idCase, String product)
    {
        ONTAP__Product__c sku;
       
        String country = '';
        String countryESV = Label.CS_Country_El_Salvador;
        String countryHN = Label.CS_Country_Honduras;
              
        try
        {
            User currentUser = [select Id, username, Country__c from User where Id = :UserInfo.getUserId()];
           if(currentUser.Country__c.equals(countryESV))
               country = Label.CS_CountryCodeSV;
           else if(currentUser.Country__c.equals(countryHN))
               country = Label.CS_CountryCodeHN;
        
            
           sku = [SELECT id, ONTAP__ProductCode__c, ONTAP__MaterialProduct__c 
                  FROM ONTAP__Product__c 
                  WHERE ONTAP__MaterialProduct__c=:product
                  AND ONTAP__Country_Code__c =: country LIMIT 1]; 
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLER_cls.GetProductCase  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLER_cls.GetProductCase  Cause: '   + ex.getCause());   
            System.debug('CS_CASE_COOLER_cls.GetProductCase  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLER_cls.GetProductCase  Stack trace: ' + ex.getStackTraceString());            
        }
		return sku;
    }
    
    /**
	* method for update material for case 
	* @author: d.zacarias.cantillo@accenture.com
	* @param String idCase to query product code
	* @return Product with the info to display 
	*/ 
    @AuraEnabled
    public static Boolean UpdateCase(String idCase, String codeProduct, String productId, Integer quantity, Boolean onlyUpdate)
    {
        Boolean success = true;

        try
        {
            Case cas = ([SELECT id, CS_skuProduct__c, ISSM_CaseForceNumber__c, ISSM_Count__c, Equipment_model__c,CS_Id_Pedido_SAP__c
                         FROM Case 
                         WHERE id =: idCase]);
            System.debug('EL caso ' + cas);
            if(cas.CS_Id_Pedido_SAP__c == null){
                ONTAP__Product__c oDetailProduct = [SELECT ONTAP__MaterialProduct__c FROM ONTAP__Product__c WHERE ID =:productId];
                System.debug('Drtalles' + oDetailProduct);
                
                cas.CS_skuProduct__c = codeProduct;
                cas.Equipment_model__c = oDetailProduct.ONTAP__MaterialProduct__c;
                cas.ISSM_Count__c = quantity;
                
                ONTAP__Case_Force__c caseForce = ([SELECT id, CS_Material__c, ONTAP__Quantity__c 
                                                FROM ONTAP__Case_Force__c 
                                                WHERE id =:cas.ISSM_CaseForceNumber__c]);
                
                System.debug('caseForce' + caseForce);
                caseForce.CS_Material__c = productId;
                caseForce.ONTAP__Quantity__c = quantity;
                            
                ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
                update caseForce;
                ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
                update cas;
                //SM
                if(onlyUpdate != true) {
                    CS_Case_Coolers_Callout_Class.sendRequest(cas.Id,'Instalacion');
                } 
            }          
        }
        catch(Exception ex)
        {  
            success = false;
            System.debug('CS_CASE_COOLER_cls.UpdateCase  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLER_cls.UpdateCase  Cause: '   + ex.getCause());   
            System.debug('CS_CASE_COOLER_cls.UpdateCase  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLER_cls.UpdateCase  Stack trace: ' + ex.getStackTraceString());           
        }
        return success;
        
    }
}