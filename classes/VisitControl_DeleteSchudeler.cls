/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_DeleteSchudeler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
global class VisitControl_DeleteSchudeler implements schedulable
{
   /**
   * Execute a Schudeler for execute the batch for delete all the routes.
   * @author: g.martinez.cabral@accenture.com
   * @param SchedulableContext sc
   * @return void
   */
    global void execute(SchedulableContext sc)
    {
    VisitControl_DeleteRouteBatch b = new VisitControl_DeleteRouteBatch(); //ur batch class
      database.executebatch(b,200);
    }    
}