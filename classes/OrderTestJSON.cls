/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: OrderTestJSON.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 06/01/2019           Heron Zurita           Creation of methods.
*/
public class OrderTestJSON {
   	/**
    * Methods getters and setters for entity OrderTestJSON
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
	@AuraEnabled public String accountId{set;get;}

    @AuraEnabled public String paymentMethod{set;get;}

    @AuraEnabled public Decimal total{set;get;}

    @AuraEnabled public Double subtotal{set;get;}

    @AuraEnabled public Double tax{set;get;}

    @AuraEnabled public String discount{set;get;}
    
}