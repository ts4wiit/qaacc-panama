/* ----------------------------------------------------------------------------
* Clase: MockHTTPResponseCoolerCalloutSuccess.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 11/02/2019        Debbie Zacarias           Creation of mock to fake success
*											  response CS_CASE_COOLERS_CALLOUT_cls.
*/

@isTest
global class CS_MockHTTPResponseCoolerCalloutSuccess implements HttpCalloutMock{
    
    /**
    * Test method for create mock for CS_CASE_COOLERS_CALLOUT_cls call out
    * @author: d.zacarias.cantillo@accenture.com
    * @param void Request 
    * @return response
    */
    global HTTPResponse respond(HTTPRequest req) { 
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody('{"id": "12345","etReturn": {"item": {"type": "E","number": "058","message": "Entrada BS01 RCC  no existe en T691A (Verifique la entrada)"}}}');       
        
        return res;
    }
}