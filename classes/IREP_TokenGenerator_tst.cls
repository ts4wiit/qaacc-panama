@isTest
private class IREP_TokenGenerator_tst{

	@testSetup static void setup() {
        IREP_CreateObjects_tst.createFullTour(2, 1, 1);
    }

	@isTest
	static void testError(){
		Test.StartTest();
            ApexPages.currentPage().getParameters().put('tid', '');
            IREP_TokenGenerator_ctr  ctr = new IREP_TokenGenerator_ctr();
            ctr.generateToken();
        Test.StopTest();
	}

	@isTest
	static void testError1(){
		Test.StartTest();
			list<ONTAP__Tour_Visit__c> lstTour = [Select Id From ONTAP__Tour_Visit__c LIMIT 1];
			ApexPages.currentPage().getParameters().put('tid', lstTour[0].Id);
			IREP_TokenGenerator_ctr  ctr = new IREP_TokenGenerator_ctr();
			ctr.generateToken();
		Test.StopTest();		
	}

	@isTest
	static void testGenerateToken(){
		Test.StartTest();
			list<ONTAP__Tour_Visit__c> lstAcc = [Select Id From ONTAP__Tour_Visit__c LIMIT 1];
			ApexPages.currentPage().getParameters().put('tid', lstAcc[0].Id);
			IREP_TokenGenerator_ctr  ctr = new IREP_TokenGenerator_ctr();
			ctr.generateToken();
		Test.StopTest();
		
	}
}