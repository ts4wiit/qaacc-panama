global with sharing class testAzureForce {

	private testAzureForceUtil.AzureCredentials azureCredentials;

	private Map<String, String> storageURIs;

	global testAzureForce(String endpoint, String accountName, String storageKey, String containerOrTable) {
		azureCredentials = new testAzureForceUtil.AzureCredentials(endpoint, accountName, storageKey, containerOrTable);

		storageURIs = new Map<String, String> {
			'table' => testAzureForceUtil.getDefaultStorageUri(accountName, 'table', endpoint),
			'blob' => testAzureForceUtil.getDefaultStorageUri(accountName, 'blob', endpoint)
		};
	}
    
    global List<testAzureForceUtil.AzureBlob> listBlobsWithPrefix(String prefix) {
		List<testAzureForceUtil.AzureBlob> result = new List<testAzureForceUtil.AzureBlob>();

		Datetime dt = Datetime.now();
		String formattedDate = dt.addMinutes(-1).addSeconds(-1).formatGmt(testAzureForceUtil.AUTH_DATEFORMAT);
        String sharedKeyConstant = 'SharedKey ';
        String separatorConstant = ':';
        String authHeader = 'Authorization';
		String signature = testAzureForceUtil.getBase64EncodedSignature(azureCredentials.storageKey, testAzureForceUtil.getListContainerContentStringToSign(azureCredentials, formattedDate, prefix));

		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setHeader(authHeader, sharedKeyConstant + azureCredentials.accountName + separatorConstant + signature);
		req.setHeader('x-ms-date', formattedDate);
		req.setHeader('x-ms-version', testAzureForceUtil.AZURE_VERSION);

		req.setEndpoint(testAzureForceUtil.getListBlobsEndpointUrl(azureCredentials, prefix));

		Http http = new Http();

		HTTPResponse response = http.send(req);

		if (response.getStatusCode() == 200) {
			result = testAzureForceUtil.parseBlobListResponse(response.getBodyDocument());
		}
		else {
			System.debug(response.toString());
			System.debug(response.getStatus());
			System.debug(response.getStatusCode());
			System.debug(response.getBody());
		}

		return result;
	}
    
    global String getBlobSASUri(testAzureForceUtil.BlobOperationType opType, String itemName) {
		Datetime dt = Datetime.now().addHours(testAzureForceUtil.SAS_EXPIRATION_PERIOD_H);
		String expiry = dt.format(testAzureForceUtil.SAS_DATEFORMAT);

		String signature;
		String perms;
		testAzureForceUtil.UrlStorageType storageType;

		if (opType == testAzureForceUtil.BlobOperationType.Read) {
			perms = 'r';
			storageType = testAzureForceUtil.UrlStorageType.b;
			signature = testAzureForceUtil.getBase64EncodedSignature(azureCredentials.storageKey, testAzureForceUtil.getBlobSASStringToSign(azureCredentials, expiry, perms, itemName));
		}
		else if (opType == testAzureForceUtil.BlobOperationType.Upload) {
			perms = 'rw';
			storageType = testAzureForceUtil.UrlStorageType.c;
			signature = testAzureForceUtil.getBase64EncodedSignature(azureCredentials.storageKey, testAzureForceUtil.getBlobSASStringToSign(azureCredentials, expiry, perms));
		}
		else if (opType == testAzureForceUtil.BlobOperationType.Remove) {
			perms = 'rd';
			storageType = testAzureForceUtil.UrlStorageType.b;
			signature = testAzureForceUtil.getBase64EncodedSignature(azureCredentials.storageKey, testAzureForceUtil.getBlobSASStringToSign(azureCredentials, expiry, perms, itemName));
		}		

		String result = '';
			result += storageURIs.get('blob');
			result += '/' + azureCredentials.containerOrTable;
			result += '/' + itemName;
			result += '?';
			result += 'se=' + EncodingUtil.urlEncode(expiry, 'UTF-8');
			result += '&sp=' + perms;
			result += '&sv=' + testAzureForceUtil.AZURE_VERSION;
			result += '&sr=' + storageType.name();
			result += '&sig=' + EncodingUtil.urlEncode(signature, 'UTF-8');
		return result;
	}


}