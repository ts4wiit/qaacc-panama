@isTest
private class ISSM_AuthorizationToken_tst {
	@isTest static void testcontroller() {
		Account acc = new Account(name = 'test');
        insert acc;
        ONTAP__Tour__c tour = new ONTAP__Tour__c(
        	ONTAP__TourId__c ='12345',
            ONTAP__TourStatus__c = 'Not Started',
            ONTAP__IsActive__c = true
        );
        insert tour;
        
        ONTAP__Tour_Visit__c tv = new ONTAP__Tour_Visit__c(
        	ONTAP__SAPCustomerId__c = '12345',
         	ONTAP__TourId__c = '54321',
            ONTAP__Actual_Start_Time__c = System.today(),
            ONTAP__Account__c = acc.id,
            ONTAP__Tour__c = tour.id
        );
        insert tv;
        ApexPages.StandardController sc = new ApexPages.standardController(tv);
        ISSM_AuthorizationToken_ctr q = new ISSM_AuthorizationToken_ctr(sc);
    }
}