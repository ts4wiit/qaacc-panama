@isTest
public class ONCALL_SAP_OrderResponseHONES_Test {

    /**
    * Test method to parse de json and verifiy the response
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    static testMethod void testParse() {
		String json=		'{'+
		'  "orderId": "0000100011",'+
		'  "etReturn": ['+
		'    {'+
		'      "type": "S",'+
		'      "number": "233",'+
		'      "message": "SALES_HEADER_IN procesado con Éxito"'+
		'    },'+
		'    {'+
		'      "type": "S",'+
		'      "number": "233",'+
		'      "message": "SALES_ITEM_IN procesado con Éxito"'+
		'    },'+
		'    {'+
		'      "type": "S",'+
		'      "number": "311",'+
		'      "message": "PA-Pd PPT Estandar 100011 se ha grabado"'+
		'    }'+
		'  ]'+
		'}';
		ONCALL_SAP_OrderResponseHONES obj = ONCALL_SAP_OrderResponseHONES.parse(json);
		System.assert(obj != null);
        
        System.JSONParser parser = System.JSON.createParser(json);
        
        ONCALL_SAP_OrderResponseHONES.Item item = new ONCALL_SAP_OrderResponseHONES.Item(parser);
        item.message='test';
        item.type_Z='test';
        item.number_Z='test';
        
	}
}