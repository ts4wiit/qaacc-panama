public class HONES_ONCALL_MasiveProductExclusionQueue implements Queueable{
    List<Account> clients = new List<Account>();
    Map<Id, ONTAP__Product__c> selectedProducts;
    Id id_recordTypeByGroup;
	String criteria;
    
    public HONES_ONCALL_MasiveProductExclusionQueue(List<Account> input, Map<Id, ONTAP__Product__c> prods, Id recordType, String crit){
        clients = input;
        selectedProducts = prods;
        id_recordTypeByGroup = recordType;
        criteria = crit;
    }
    
    public void execute(QueueableContext context) {
        Map<Id, HONES_ProductPerClient__c> exclusionsToDelete = new Map<Id, HONES_ProductPerClient__c>();
        Map<String, HONES_ProductPerClient__c> exclusionsToUpsert = new Map<String, HONES_ProductPerClient__c>();
        // El primer id es del cliente, el segundo es de los productos:
        Map<Id, Map<id, HONES_ProductPerClient__c>> MapCurrentExclusions = new Map<Id, Map<id, HONES_ProductPerClient__c>>(); 
        List<Id> idClients = new List<Id>();
        for(Account acc: clients){
            idClients.add(acc.Id);
        }
        
        List<HONES_ProductPerClient__c> currentExclusions = [SELECT Id, HONES_RelatedClient__c, HONES_RelatedProduct__c, SAP_Account_ID__c, SAP_SKU_ID__c, RecordTypeId, HONES_MassiveKey__c 
                                                             FROM HONES_ProductPerClient__c WHERE HONES_RelatedClient__c IN : idClients];
        
        for(HONES_ProductPerClient__c exclusion : currentExclusions){
            // se crea mapa con exclusiones aplicadas actualmente
            Map<Id,HONES_ProductPerClient__c> exclusionMapPerClient = new Map<Id,HONES_ProductPerClient__c>();
            if(MapCurrentExclusions.containsKey(exclusion.HONES_RelatedClient__c)){
                exclusionMapPerClient = MapCurrentExclusions.get(exclusion.HONES_RelatedClient__c);
            }
            exclusionMapPerClient.put(exclusion.HONES_RelatedProduct__c, exclusion);
            MapCurrentExclusions.put(exclusion.HONES_RelatedClient__c, exclusionMapPerClient);
        }
        for (Account client : clients) {
            Map<Id,HONES_ProductPerClient__c> exclusionMapPerClient = new Map<Id,HONES_ProductPerClient__c>();
            if(MapCurrentExclusions.containsKey(client.Id)){
                exclusionMapPerClient = MapCurrentExclusions.get(client.Id);
            }
            if(exclusionMapPerClient.values().size() > 0 ){
                for(HONES_ProductPerClient__c currentExclusion : exclusionMapPerClient.values()){
                    ONTAP__Product__c selectedProduct = selectedProducts.get(currentExclusion.HONES_RelatedProduct__c);
                    if(selectedProduct == null && currentExclusion.HONES_MassiveKey__c == criteria){ 
                        // si una exclusión previa no ha sido seleccionada, es eliminada
                        exclusionsToDelete.put(currentExclusion.Id, currentExclusion);
                    }else{ 
                        //si ha sido seleccionada, pero tuvo cambios, se actualiza
                        //if(selectedProduct!=null && (currentExclusion.RecordTypeId != id_recordTypeByGroup || currentExclusion.HONES_MassiveKey__c != criteria)){
                        if(selectedProduct!=null && currentExclusion.RecordTypeId != id_recordTypeByGroup){
                            currentExclusion.RecordTypeId = id_recordTypeByGroup;
                            String new_key = String.valueOf(selectedProduct.Id)+String.valueOf(currentExclusion.HONES_RelatedClient__c);							
                            exclusionsToUpsert.put(new_key, currentExclusion);
                            /* Si una exclusion ya existe se deja tal cual, a menos que se cambie el tipo de registro, de individual a masivo
                            List<String> lastKeys = currentExclusion.HONES_MassiveKey__c.split('[|]');
                            String lastKey = lastKeys[0].substring(0,4) + '|' + lastKeys[1].substring(0,3) + '|' + lastKeys[2].substring(0,2);
                            currentExclusion.HONES_MassiveKey__c = criteria;
                            currentExclusion.v360_AccountGroup__c = lastKey; // almacena la llave anterior, antes de ser actualizada
							*/
                        }
                    }
                }
            }
            
            for(ONTAP__Product__c prod : selectedProducts.values()){ 
                if(exclusionMapPerClient.containsKey(prod.Id) == false){
                    HONES_ProductPerClient__c exclusion = new HONES_ProductPerClient__c();
                    exclusion.RecordTypeId = id_recordTypeByGroup;
                    exclusion.HONES_MassiveKey__c = criteria;
                    exclusion.HONES_RelatedProduct__c = prod.Id;
                    exclusion.SAP_SKU_ID__c = prod.ONTAP__ProductCode__c;
                    exclusion.HONES_RelatedClient__c = client.Id;
                    exclusion.SAP_Account_ID__c = client.ONTAP__SAP_Number__c;
                    exclusionsToUpsert.put(String.valueOf(prod.Id)+String.valueOf(client.Id),exclusion);
                }
            }
        }
        
        try{
            delete exclusionsToDelete.values();
            upsert exclusionsToUpsert.values();
        } catch (DmlException e) {
            System.debug('==== Se produjo un error al modificar registros: ' + e.getMessage());
        }
	}
}