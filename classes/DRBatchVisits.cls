global class DRBatchVisits implements 
    Database.Batchable<sObject>, Database.Stateful {
    
    global Integer processedUsers = 0;
        
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT User_Route__c,VisitsScheduledUntil__c FROM User WHERE ONTAP__Country_Alias__c = \'DO\''
        );
    }
        
    global void execute(Database.BatchableContext bc, List<User> scope){
        // process each batch of records

        for (User user : scope) {
           DRUserVisitsService visitsService = new DRUserVisitsService( user );
           visitsService.run();
           processedUsers++;
        }

    }    
        
    global void finish(Database.BatchableContext bc){
        System.debug(processedUsers + ' users processed.');
    }    
}