/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_EmptyBalanceTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
private class V360_EmptyBalanceTriggerHandler_Test{
    
    /**
   	* Method for test the before insert empty balance.
   	* @author: g.martinez.cabral@accenture.com
   	* @param void
   	* @return void
   	*/
    @isTest static void test_beforeInsert_UseCase1(){
        V360_EmptyBalanceTriggerHandler obj01 = new V360_EmptyBalanceTriggerHandler();
        Id V360_EmptyBalanceRT = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.EMPTY_BALANCE_V360_RECORDTYPE_NAME).getRecordTypeId();
        Account acc = new Account(Name = '1234567890', ONTAP__SAP_Number__c='1234567890');
        insert acc;
        ONTAP__EmptyBalance__c obj02= new ONTAP__EmptyBalance__c(RecordTypeId = V360_EmptyBalanceRT, ONTAP__SAPCustomerId__c='1234567890');
        
        insert obj02;
    }
    
    /**
   	* Method for test the before update empty balance.
   	* @author: g.martinez.cabral@accenture.com
   	* @param void
   	* @return void
   	*/
    @isTest static void test_beforeUpdate_UseCase1(){
        V360_EmptyBalanceTriggerHandler obj01 = new V360_EmptyBalanceTriggerHandler();
        ONTAP__EmptyBalance__c obj02= new ONTAP__EmptyBalance__c();
        obj02.ONTAP__SAPCustomerId__c='';
        insert obj02;
        update obj02;
    }
    
    /**
   	* Method for test relate an Account to Empty Balance.
   	* @author: g.martinez.cabral@accenture.com
   	* @param void
   	* @return void
   	*/
    @isTest static void test_relateAccountsToEmpties_UseCase1(){
        V360_EmptyBalanceTriggerHandler obj01 = new V360_EmptyBalanceTriggerHandler();
        List<ONTAP__EmptyBalance__c> newlist = [SELECT Id, ONTAP__SAPCustomerId__c, RecordTypeId, ONTAP__Account__c FROM ONTAP__EmptyBalance__c Limit 5];
        List<ONTAP__EmptyBalance__c> listEmp = PreventExecutionUtil.validateEmptyWithOutId(newlist);
        Set<Id> setIdsEmpties = new Set<Id>();
        for(ONTAP__EmptyBalance__c emp : listEmp){
            if(!setIdsEmpties.contains(emp.Id) && emp.ONTAP__Account__c == null ){setIdsEmpties.add(emp.Id);}
        }
         if(!setIdsEmpties.isEmpty()){
             //V360_EmptyBalanceTriggerHandler.relateAccountsToEmpties(setIdsEmpties);
         }
    }
}