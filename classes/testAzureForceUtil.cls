global with sharing class testAzureForceUtil {

	global static final String SAS_DATEFORMAT = 'yyyy-MM-dd\'T\'hh:mm:ss\'Z\'';
	global static final Integer SAS_EXPIRATION_PERIOD_H = 16;
	global static final String AUTH_DATEFORMAT = 'EEE, dd MMM yyyy HH:mm:ss\' GMT\'';
	global static final String AZURE_VERSION = '2015-12-11';

	global enum UrlStorageType { b, c }
	global enum BlobOperationType { Read, Upload, Remove }
	global enum TableOperationType { Query, UpsertEntity, DeleteEntity }
	
	global static String getBase64EncodedSignature(String storageKey, String stringToSign) {
		Blob keyBlob = EncodingUtil.base64Decode(storageKey);
		Blob hmac = Crypto.generateMac('HMacSHA256', Blob.valueOf(stringToSign), keyBlob);
		return EncodingUtil.base64Encode(hmac);
	}

	global static String getDefaultStorageUri(String accountName, String storageType, String endpoint) {
		return 'https://' + accountName + '.' + storageType + '.' + endpoint;
	}

	global static List<AzureBlob> parseBlobListResponse(Dom.Document doc) {
		List<AzureBlob> blobs = new List<AzureBlob>();

		Dom.XmlNode blobsNode =  doc.getRootElement().getChildElement('Blobs', null);
		for (Dom.XmlNode blobNode : blobsNode.getChildElements()) {
			String name;
			if (blobNode.getChildElement('Name', null) != null) {
				name = blobNode.getChildElement('Name', null).getText();
			}

			String etag;
			String contentType;
			String lastModifiedDate;
			if (blobNode.getChildElement('Properties', null) != null) {
				if (blobNode.getChildElement('Properties', null).getChildElement('Etag', null) != null) {
					etag = blobNode.getChildElement('Properties', null).getChildElement('Etag', null).getText();
				}

				if (blobNode.getChildElement('Properties', null).getChildElement('Content-Type', null) != null) {
					contentType = blobNode.getChildElement('Properties', null).getChildElement('Content-Type', null).getText();
				}

				if (blobNode.getChildElement('Properties', null).getChildElement('Last-Modified', null) != null) {
					lastModifiedDate = blobNode.getChildElement('Properties', null).getChildElement('Last-Modified', null).getText();
				}
			}

			AzureBlob azureBlob = new AzureBlob(name)
										.setEtag(etag)
										.setContentType(contentType)
										.setLastModifiedDate(lastModifiedDate);

			blobs.add(azureBlob);
		}
		
		return blobs;
	}

	global static String getBlobSASStringToSign(AzureCredentials creds, String expiry, String perms) {
		return getBlobSASStringToSign(creds, expiry, perms, '');
	}

	global static String getBlobSASStringToSign(AzureCredentials creds, String expiry, String perms, String itemName) {
		String itemPart = String.isNotBlank(itemName) ? '/' + itemName : '';

		String strToSign = perms + '\n';
			   strToSign += '\n';
			   strToSign += expiry + '\n';
			   strToSign += '/blob/' + creds.accountName + '/' + creds.containerOrTable + itemPart + '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += AZURE_VERSION + '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += '';

		return strToSign;
	}

	global static String getTableSASStringToSign(AzureCredentials creds, String expiry, String perms) {
		String strToSign = perms + '\n';
			   strToSign += '\n';
			   strToSign += expiry + '\n';
			   strToSign += '/table/' + creds.accountName + '/' + creds.containerOrTable + '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += AZURE_VERSION + '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += '\n';
			   strToSign += '';

		return strToSign;
	}


	global static String getListContainerContentStringToSign(AzureCredentials creds, String formattedDate, String prefix) {

		String prefixPart = String.isNotBlank(prefix) ? '\nprefix:' + prefix : '';

		String strToSign = '';
				strToSign += 'GET\n'; /*HTTP Verb*/  
				strToSign += '\n';	/*Content-Encoding*/ 
				strToSign += '\n';	/*Content-Language*/
				strToSign += '\n';	/*Content-Length (include value when zero)*/
				strToSign += '\n';	/*Content-MD5*/
				strToSign += '\n';	/*Content-Type*/ 
				strToSign += '\n';	/*Date*/  
				strToSign += '\n';	/*If-Modified-Since */
				strToSign += '\n';	/*If-Match*/
				strToSign += '\n';	/*If-None-Match*/
				strToSign += '\n';	/*If-Unmodified-Since*/
				strToSign += '\n';	/*Range*/ 
				strToSign += 'x-ms-date:' + formattedDate + '\nx-ms-version:' + testAzureForceUtil.AZURE_VERSION + '\n';	/*CanonicalizedHeaders*/
				strToSign += '/' + creds.accountName + '/' + creds.containerOrTable + '\ncomp:list' + prefixPart + '\nrestype:container';	/*CanonicalizedResource*/

		return strToSign;
	}

	global static String getListBlobsEndpointUrl(AzureCredentials creds, String prefix) {
		String result = getDefaultStorageUri(creds.accountName, 'blob', creds.endpoint) + '/' + creds.containerOrTable + '?restype=container&comp=list';
		if (String.isNotBlank(prefix)) {
			result += '&prefix=' + prefix;
		}
		return result;
	}

	global class AzureBlob {
		global String name;
		global String etag;
		global String contentType;
		global String lastModifiedDate;
		global String sasUri;

		global AzureBlob() {}

		global AzureBlob(String name) {
			this.name = name;
		}

		global AzureBlob setEtag(String etag) {
			this.etag = etag;
			return this;
		}

		global AzureBlob setContentType(String contentType) {
			this.contentType = contentType;
			return this;
		}

		global AzureBlob setLastModifiedDate(String lastModifiedDate) {
			this.lastModifiedDate = lastModifiedDate;
			return this;
		}

		global AzureBlob setSASUri(String sasUri) {
			this.sasUri = sasUri;
			return this;
		}
	}

	global class AzureCredentials {
		global String endpoint;
		global String accountName;
		global String storageKey;
		global String containerOrTable;

		global AzureCredentials(String endpoint, String accountName, String storageKey, String containerOrTable) {
			this.endpoint = endpoint;
			this.accountName = accountName;
			this.storageKey = storageKey;
			this.containerOrTable = containerOrTable;
		}
	}
}