/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_VIEW360_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 30/12/2018           Jose Luis Vargas       Crecion de la clase para consultar el id de account de un caso standard
 */

public class CS_View360_Class 
{
    public CS_View360_Class() {}
    
    /**
    * Method for consulting the id account
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id of case
    * @return Id of the account
    */
    @AuraEnabled
    public static Id GetIdAccount(string idCase)
    {
        Case oDetailCase = new Case();
        oDetailCase = [SELECT AccountID FROM Case WHERE ID =: idCase];
        
        return oDetailCase.AccountId;
    }
}