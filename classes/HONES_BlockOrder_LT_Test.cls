/*******************************************************************************
Developed by: TS4
Author: Daniel Hernández
Proyect: HONES
Description: Apex class to test methods for HONES_BlockOrder_LT functionality

------ ---------- -------------------------- -----------------------------------
No.    Date       Autor                      Description
------ ---------- -------------------------- -----------------------------------
1.0    09/12/2019 Daniel Hernández           Created Class
*******************************************************************************/

@isTest
public class HONES_BlockOrder_LT_Test {
    
    
    @testSetup static void setup(){
        Account cta = new Account(Name = 'Pupuseria', ONTAP__ExternalKey__c='SVCS0112345678911');
        insert cta;
        RecordType recordTypeOnCall= [SELECT Id FROM RecordType WHERE Name = 'ONCALL' LIMIT 1][0];
        ONCALL__Call__c llamada = new ONCALL__Call__c(Name = 'levantar pedido', RecordTypeId = recordTypeOnCall.Id, ONCALL__POC__c = cta.Id);
        insert llamada;
        event evnt = new event(WhatId=cta.Id);
        evnt.StartDateTime=datetime.now();
        evnt.EndDateTime=datetime.now();
        insert evnt;
        
        ONTAP__Order__c order_withCall = new ONTAP__Order__c(ONCALL__Call__c = llamada.Id, ONCALL__OnCall_Status__c='Closed', ONTAP__OrderAccount__c= cta.Id,
                                                             ONCALL__SAP_Order_Response__c='S - SALES_HEADER_IN procesado con éxito', 
                                                             ONTAP__SAP_Order_Number__c='6202143408');
        insert order_withCall;
        ONTAP__Order__c order_withEvent = new ONTAP__Order__c(ONTAP__Event_Id__c = evnt.Id, ONCALL__OnCall_Status__c='Closed', ONTAP__OrderAccount__c= cta.Id,
                                                             ONCALL__SAP_Order_Response__c='S - SALES_HEADER_IN procesado con éxito', 
                                                             ONTAP__SAP_Order_Number__c='6202143409');
        insert order_withEvent;
    }
    
    
    @isTest static void sendOrders(){
        List<ONTAP__Order__c> ordenes = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c FROM ONTAP__Order__c LIMIT 10];
        for(ONTAP__Order__c orden: ordenes){
            HONES_BlockOrder_LT.hasRelatedOrders(orden.Id);
            HONES_BlockOrder_LT.blockOrder(orden.Id);
        }
        HONES_BlockOrder_LT.ResponseBlockOrder rb1 = new HONES_BlockOrder_LT.ResponseBlockOrder();
        rb1.Id='123';
        rb1.sfdcId='abc';
        rb1.success=false;
        rb1.message='to test method ';
        rb1.parse('{"make":"SFDC","year":"2020"}');
        HONES_BlockOrder_LT.ResponseBlockOrderList rb2 = new HONES_BlockOrder_LT.ResponseBlockOrderList();
        rb2.resultOrder = rb1;
        rb2.parse('{"make":"SFDC","year":"2020"}');
        HONES_BlockOrder_LT.ArrayBlockOrder ab = new HONES_BlockOrder_LT.ArrayBlockOrder();
        HONES_BlockOrder_LT.CustomResponse cr = new HONES_BlockOrder_LT.CustomResponse();
        cr.success = false;
        
    }
    
    
    @isTest static void callout(){
        Test.setMock(HttpCalloutMock.class, new HONES_BlockOrder_HttpCalloutMock());
        ONTAP__Order__c orden = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c FROM ONTAP__Order__c LIMIT 1][0];
        
        Test.startTest();
        HONES_BlockOrder_LT.CustomResponse customResponse = HONES_BlockOrder_LT.blockOrder(orden.Id);
        System.debug('>>>>> HONES_BlockOrder_LT.CustomResponse: ' + customResponse);
        HONES_BlockOrder_LT.hasRelatedOrders(orden.Id);
        Test.stopTest();
    }
    
    @isTest static void nullcallout(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, mock);
        ONTAP__Order__c orden = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c FROM ONTAP__Order__c LIMIT 1][0];
        
        Test.startTest();
        HONES_BlockOrder_LT.CustomResponse customResponse = HONES_BlockOrder_LT.blockOrder(orden.Id);
        System.debug('>>>>> HONES_BlockOrder_LT.CustomResponse: ' + customResponse);
        HONES_BlockOrder_LT.hasRelatedOrders(orden.Id);
        Test.stopTest();
    }
    
    
    
}