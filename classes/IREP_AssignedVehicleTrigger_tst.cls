@isTest
private class IREP_AssignedVehicleTrigger_tst {


	@isTest static void test_method_create() {
		ONTAP__Tour__c objTour = IREP_CreateObjects_tst.createTour(null);
		insert objTour;

		ONTAP__AssignedVehicle__c objAssignedVehicle = IREP_CreateObjects_tst.createAssignedVehicle(objTour.Id);

		Test.startTest();
			insert objAssignedVehicle;
		Test.stopTest();

		objTour = [Select Id, IREP_BeginMile__c, IREP_EndMile__c From ONTAP__Tour__c Where Id = :objTour.Id];

		System.assertEquals(objAssignedVehicle.IREP_BeginMile__c, objTour.IREP_BeginMile__c);
		System.assertEquals(objAssignedVehicle.IREP_EndMile__c, objTour.IREP_EndMile__c);
	}
	
	@isTest static void test_method_update() {
		ONTAP__Tour__c objTour = IREP_CreateObjects_tst.createTour(null);
		insert objTour;

		ONTAP__AssignedVehicle__c objAssignedVehicle = IREP_CreateObjects_tst.createAssignedVehicle(objTour.Id);
		insert objAssignedVehicle;

		Test.startTest();
			objAssignedVehicle.IREP_BeginMile__c = 500;
			objAssignedVehicle.IREP_EndMile__c = 1000;
			update objAssignedVehicle;
		Test.stopTest();

		objTour = [Select Id, IREP_BeginMile__c, IREP_EndMile__c From ONTAP__Tour__c Where Id = :objTour.Id];

		System.assertEquals(objAssignedVehicle.IREP_BeginMile__c, objTour.IREP_BeginMile__c);
		System.assertEquals(objAssignedVehicle.IREP_EndMile__c, objTour.IREP_EndMile__c);
	}

	@isTest static void test_method_delete() {
		ONTAP__Tour__c objTour = IREP_CreateObjects_tst.createTour(null);
		insert objTour;

		ONTAP__AssignedVehicle__c objAssignedVehicle = IREP_CreateObjects_tst.createAssignedVehicle(objTour.Id);
		insert objAssignedVehicle;

		Test.startTest();
			delete objAssignedVehicle;
		Test.stopTest();

		objTour = [Select Id, IREP_BeginMile__c, IREP_EndMile__c From ONTAP__Tour__c Where Id = :objTour.Id];

		System.assertEquals(0, objTour.IREP_BeginMile__c);
		System.assertEquals(0, objTour.IREP_EndMile__c);
	}
	
}