/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_OrderToJson_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class HONES_ONCALL_OrderToJson_Test {
    
    /**
    * Test method for set values in the object
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void HONES_ONCALL_OrderToJson_UseCase1(){
    	HONES_ONCALL_OrderToJson objTo = new HONES_ONCALL_OrderToJson();
    	objTo.Ids = 'Test';
		objTo.Assigned_Agent = 'Test';
        objTo.CustomerSAPID = 'Test';
        objTo.DeliveryDate = 'Test';
        objTo.DocumentationType = 'Test';
        objTo.PaymentMethod = 'Test';
        objTo.SalesOgId = 'Test';
        List<ONTAP__Order_Item__c> Items = new List<ONTAP__Order_Item__c>();
        objTo.Items = Items;
        
        /*Hones_Oncall_OrderTypeWrap objWrap = new Hones_Oncall_OrderTypeWrap();
        List<List<String>> picktype = new List<List<String>>();
        objWrap.picktype = picktype;*/
        
    }
}