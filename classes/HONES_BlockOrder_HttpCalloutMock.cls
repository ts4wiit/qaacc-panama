@isTest
global class HONES_BlockOrder_HttpCalloutMock implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        //response.setBody('{"message": "callout success", "success":"true"}');
        response.setBody('{"response": "ok", "message":"'+GlobalStrings.HONES_BLOCK_STATUS+'", "success":true, "sfdcId":"sfdcId", "id":"0001"}');
        response.setStatusCode(200);
        return response; 
    }
}