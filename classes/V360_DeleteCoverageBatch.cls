/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteCoverageBatch.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteCoverageBatch implements Database.Batchable<sObject>{
    
     global Boolean hasErrors {get; private set;}
	
    /**
    * Clean the class.
    * @author: c.leal.beltran@accenture.com 
    */
    global V360_DeleteCoverageBatch()
    {  
        this.hasErrors = false;
    }
    
     /**
   * Execute the Query for the coverage objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return Database.QueryLocator
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Id V360_RT = Schema.SObjectType.V360_Coverage__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.COVERAGE_RECORDTYPE_NAME).getRecordTypeId();

        system.debug('Processing start method'); 
        // Query string for batch Apex
        String query = ''; 
        Date todayDate = System.Today();
        
        if(System.Test.isRunningTest()){
            query += 'SELECT Id FROM V360_Coverage__c';
        }else{
            query += 'SELECT Id FROM V360_Coverage__c WHERE RecordTypeId =:V360_RT AND LastModifiedDate<:todayDate';
        }

        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
     /**
   * Method for delete all the coverage objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC, List<V360_Coverage__c> objectBatch
   * @return void
   */
    global void execute(Database.BatchableContext BC, List<V360_Coverage__c> objectBatch)
    { 
        delete objectBatch;
        DataBase.emptyRecycleBin(objectBatch);
    }
    
     /**
   * Method finish for close all open process
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}