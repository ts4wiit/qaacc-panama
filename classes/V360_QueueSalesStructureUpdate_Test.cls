/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_QueueSalesStructureUpdate_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 * 19/12/2018     Carlos Leal             Creation of methods.
 */
@isTest
private class V360_QueueSalesStructureUpdate_Test{
    
   /**
    * method which test the execution of the Queue.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    @isTest
    public static void execute_test(){
        System.enqueueJob(new V360_QueueSalesStructureUpdate(New List<Account>()));
    }
}