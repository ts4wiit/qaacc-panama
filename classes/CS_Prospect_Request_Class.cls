/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_Prospect_Request_Class.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 17/04/2019      Jose Luis Vargas        Creation of the class with properties for json generation
 *                                             
*/

public class CS_Prospect_Request_Class
{
    public CS_Prospect_Request_Class() {}
    
    public string Header_Id {get; set;}
    public string Country {get; set;}
    public string Employee_Id_Create {get; set;}
    public string Employee_Name_Create {get; set;}
    public string Employee_Email_Create {get; set;}
    public string Survey_Date {get; set;}
    public string Survey_Type {get; set;}
    public string Survey_Type_Id {get; set;}
    public string Latitude {get; set;}
    public string Longitude {get; set;}
    public string Customer_referee {get; set;}
    public string Status {get; set;}
    public string AC_CambioRazonSocial {get; set;}
    public string AC_TipoIdentificacion {get; set;}
    public string AC_TipoPersona {get; set;}
    public string AC_NumeroIdentificacion {get; set;}
    public string AC_NumeroIdentificacion2 {get; set;}
    public string AC_NumeroIdentificacion3 {get; set;}
    public string AC_NombreComercial {get; set;}
    public string AC_NombreCliente_PrimerNombre {get; set;}
    public string AC_NombreCliente_SegundoNombre {get; set;}
    public string AC_NombreCliente_ApPaterno {get; set;}
    public string AC_NombreCliente_ApMaterno {get; set;}
    public string AC_TipoVia {get; set;}
    public string AC_NombreVia {get; set;}
    public string AC_Nro {get; set;}
    public string AC_Poblacion {get; set;}
    public string AC_Distrito {get; set;}
    public string AC_TipoPoblacion {get; set;}
    public string AC_TelefonoMovil {get; set;}
    public string AC_TelefonoFijo {get; set;}
    public string AC_FechaNacimiento {get; set;}
    public string AC_External_Id {get; set;}
    public string AC_Clase_Cliente {get; set;}
    public string AC_Canal_Subcanal_Local {get; set;}
}