/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap 
 * ----------------------------------------------------------------------------
 * Clase: ONCALL_SchedulerSendOrder.apxc
 * Versión: 1.0.0.0
 * 
 * Clase destinada para el envío de órdenes a SAP
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 15/05/2019    Gabriel Garcia	  gabriel.e.garcia@accenture.com        Creación de la clase  
 * 
 */
public class ONCALL_SchedulerSendOrder implements Schedulable{
    /**
    * Method that invoke a queueable class 
    * Created By: gabriel.e.garcia@accenture.com
    * @param SchedulableContext SC
    * @return void
    */
    public void execute(SchedulableContext sc){            	        
        System.enqueueJob(new ONCALL_QueueableSendOrder());                     
    }
}