/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_EventsAndCallsCtrl.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class V360_EventsAndCallsCtrl {    
	
    /**
    * Method for get events by an account 
    * Created By: g.martinez.cabral@accenture.com
    * @param String accId
    * @return List<Event>
    */
	@AuraEnabled
    public static List<Event> getEventsByAccount(String accId) {
        System.debug('ACCDID'+accId );
        List<Event> lstEvents =  [SELECT Id, Subject, ActivityDate, Owner.Alias, Type__c, ISSM_CallEffectiveness__c, ReasonNotSale__c, ReasonNotVisit__c FROM Event WHERE WhatId =:accId Order By ActivityDate DESC LIMIT 2000];
        return lstEvents;
    }
}