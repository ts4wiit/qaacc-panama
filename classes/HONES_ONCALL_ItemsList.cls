/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_ItemsList.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public class HONES_ONCALL_ItemsList {
	
	/**
    * Methods getters and setters By entity HONES_ONCALL_ItemsList
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */    
    public String sku{get; set;}
	public String quantity{get; set;}
    public String measurecode{get; set;}
	public String empties{get; set;}
    public String posex{get;set;}
    public String orderReason{get;set;}
}