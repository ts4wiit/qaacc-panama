/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_InterlocutorTools.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 12/02/2018     Gerardo Martinez        Creation of methods.
*/
@isTest public class V360_CheckRecursive_Test {
    
    
    /**
    * Testing Method recursive
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void testRecursiveBan(){
        Boolean ban = V360_CheckRecursive.runOnce();
        System.assert(ban);
        ban = V360_CheckRecursive.runOnce();
        System.assert(!ban);
    }
    

}