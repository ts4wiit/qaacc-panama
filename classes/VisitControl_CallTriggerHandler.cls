/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_CallTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class VisitControl_CallTriggerHandler extends TriggerHandlerCustom{
    
 	private Map<Id, ONCALL__Call__c > newMap;
    private Map<Id, ONCALL__Call__c > oldMap;
    private List<ONCALL__Call__c > newList;
    private List<ONCALL__Call__c > oldList;
    
    /**
    * Constructor of the class
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public VisitControl_CallTriggerHandler() {
        this.newMap = (Map<Id, ONCALL__Call__c >) Trigger.newMap;
        this.oldMap = (Map<Id, ONCALL__Call__c >) Trigger.oldMap;
        this.newList = (List<ONCALL__Call__c >) Trigger.new;
        this.oldList = (List<ONCALL__Call__c >) Trigger.old;
    }
    
    /**
    * Method wich is executed every time is inserted
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void beforeInsert(){
        List<ONCALL__Call__c > listObj = PreventExecutionUtil.validateCallWithOutId(this.newList);
        retraceCallOneDay(listObj);
        anticipateCallOneDay(listObj);
    }
   
    /**
    * Method for retrace call one day
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public static void retraceCallOneDay( List<ONCALL__Call__c> newList){
        
        Set<Id> routes = new Set<Id>();
        
        for( ONCALL__Call__c callObj: newList){
            if(callObj.VisitControl_IsRetraced__c  != NULL && callObj.VisitControl_IsRetraced__c  == TRUE ){
                routes.add(callObj.VisitControl_TourRouteId__c );
            }       
        }
        
        List<VisitControl_RetraceAndAnticipateRoute__c> routesToAnticipate = [SELECT Id, VisitControl_Route__c, VisitControl_RetraceOrAnticipate__c, VisitControl_DateToSkip__c FROM VisitControl_RetraceAndAnticipateRoute__c  WHERE 
                                                                 VisitControl_Route__c IN: routes AND 
                                                                 VisitControl_RetraceOrAnticipate__c =: GlobalStrings.VISITCONTROL_RETRACE]; 
        for( ONCALL__Call__c  callObj: newList){
            
            for (VisitControl_RetraceAndAnticipateRoute__c obj:routesToAnticipate){    
                
                Datetime startDatetime = callObj.ONCALL__Date__c;
                Date startDate = date.newinstance(StartDatetime.year(), StartDatetime.month(), StartDatetime.day());
                if (callObj.VisitControl_TourRouteId__c  != NULL && callObj.VisitControl_TourRouteId__c   == obj.VisitControl_Route__c && startDate.toStartOfWeek() == obj.VisitControl_DateToSkip__c.toStartOfWeek() && 
                    startDate >= obj.VisitControl_DateToSkip__c){
					
                    System.debug('--------------------  CALL START DATE startDate' +startDate);    
                    
                    Date startDateRetraced = startDate.addDays(1);
                    System.debug('--------------------  CALL START DATE new Date' +startDateRetraced);
                	callObj.ONCALL__Date__c = startDateRetraced ;
                }
            }
        }
    }
    
    /**
    * Method for anticipate tour one day
    * Created By: g.martinez.cabral@accenture.com
    * @param List<ONCALL__Call__c> newList
    * @return void
    */
    public static void anticipateCallOneDay( List<ONCALL__Call__c> newList){
        
        Set<Id> routes = new Set<Id>();
        
        for( ONCALL__Call__c callObj: newList){
            if(callObj.VisitControl_IsRetraced__c  != NULL && callObj.VisitControl_IsRetraced__c  == TRUE ){
                routes.add(callObj.VisitControl_TourRouteId__c );
            }
        }
        
        System.debug('Routes ID:'+ routes);
        
        List<VisitControl_RetraceAndAnticipateRoute__c> routesToAnticipate = [SELECT Id, VisitControl_Route__c, VisitControl_RetraceOrAnticipate__c, VisitControl_DateToSkip__c FROM VisitControl_RetraceAndAnticipateRoute__c  WHERE 
                                                                 VisitControl_Route__c IN: routes AND 
                                                                 VisitControl_RetraceOrAnticipate__c =: GlobalStrings.VISITCONTROL_ANTICIPATE]; 
         System.debug('Anticipations :'+ routesToAnticipate);
        for( ONCALL__Call__c  callObj: newList){
            
            for (VisitControl_RetraceAndAnticipateRoute__c obj:routesToAnticipate){    
                
                Datetime startDatetime = callObj.ONCALL__Date__c;
                Date startDate = date.newinstance(StartDatetime.year(), StartDatetime.month(), StartDatetime.day());
                
                
                System.debug(obj.VisitControl_DateToSkip__c.toStartOfWeek());
                System.debug(startDate.toStartOfWeek());
                 System.debug('--------------------  CALL START DATE startDate JL' +startDate);
                if (callObj.VisitControl_TourRouteId__c  != NULL && callObj.VisitControl_TourRouteId__c == obj.VisitControl_Route__c && startDate.toStartOfWeek() == obj.VisitControl_DateToSkip__c.toStartOfWeek() && startDate <= obj.VisitControl_DateToSkip__c){
                   
                    
                    
                    Date startDateRetraced = startDate-1;
                    
                     System.debug('--------------------  CALL START DATE new Date' +startDateRetraced);
                	callObj.ONCALL__Date__c = startDateRetraced;
                }
            }
        }
    }
}