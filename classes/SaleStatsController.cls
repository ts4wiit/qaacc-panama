/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: SaleStatsController.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class SaleStatsController {
    
    /**
    * Method to get all the Idcalls by recordId  
    * Created By: g.martinez.cabral@accenture.com
    * @param String recordId
    * @return List<Id>
    */
    @AuraEnabled
    public static List<Id> getAccountFromCall2(String recordId){
        List<Id> listcall=new List<Id>();
        ONCALL__Call__c callobj = [SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =:recordId];
        listcall.add(callobj.ONCALL__POC__c);
        return listcall; 
    }
    
    /**
    * Method to get all the coverage elements related to an account  
    * Created By: g.martinez.cabral@accenture.com
    * @param String recordId
    * @return List<List<List<String>>>
    */
    @AuraEnabled
    public static  List<List<List<String>>> getStats(String recordId) {
        
        List<v360_Coverage__c> saleStats = [SELECT Name, V360_Id_client_SAP__c, V360_Sector__c, V360_Brand__c, V360_Container__c, V360_This__c, V360_Last__c, V360_Focus_Portfolio__c FROM v360_Coverage__c  WHERE V360_Account__c =:recordId ];
        System.debug('saeStats size: ' + saleStats.size());
        Map<String, List<v360_Coverage__c>> mapList = new Map<String, List<v360_Coverage__c>>(); 
        
        for(v360_Coverage__c ss : saleStats){
            if(ss.V360_Sector__c != null){
                if(!mapList.containsKey(ss.V360_Sector__c)){
                    List<v360_Coverage__c> listObjectCreate = new List<v360_Coverage__c>();
                    listObjectCreate.add(ss);
                    mapList.put(ss.V360_Sector__c, listObjectCreate);
                    
                }else{
                    List<v360_Coverage__c> listObjectExist = new List<v360_Coverage__c>();
                    listObjectExist = mapList.get(ss.V360_Sector__c);
                    listObjectExist.add(ss);                    
                }
            }else {
            }
        }
        
        
        List<List<List<String>>> tablesList = new List<List<List<String>>>();
        for(String key : mapList.keySet()) {
            List<List<String>> tables = new List<List<String>>();
            List<String> header = new List<String>();
            
            // -> Header List
            header.add(mapList.get(key)[0].V360_Sector__c.toUpperCase());
            // ---> MapGroupingBybrand
            Map<String, List<v360_Coverage__c>> groupBybrand = new  Map<String, List<v360_Coverage__c>>();            
            for(v360_Coverage__c obj : mapList.get(key)){
                if(!header.contains(obj.V360_Container__c.toUpperCase())){
                    header.add(obj.V360_Container__c.toUpperCase());
                }
                
                
                if(!groupBybrand.containsKey(obj.V360_Brand__c)){
                    List<v360_Coverage__c> tmpbrands = new List<v360_Coverage__c>();
                    tmpbrands.add(obj);
                    groupBybrand.put(obj.V360_Brand__c, tmpbrands);
                }else {
                    List<v360_Coverage__c> tmpbrandsAreNot = new List<v360_Coverage__c>();
                    tmpbrandsAreNot = groupBybrand.get(obj.V360_Brand__c);
                    tmpbrandsAreNot.add(obj);
                    groupBybrand.put(obj.V360_Brand__c, tmpbrandsAreNot);
                }
            }
            tables.add(header);
            
            for(String ky : groupBybrand.keySet()) {
                //List<String> row = new List<String>();
                List<String> row = new List<String>();
                for(String dot : header){
                    row.add('');
                }
                row[0] = ky;
                //row.add(ky);
                for(v360_Coverage__c objBybrand : groupBybrand.get(ky)){
                    
                    Integer indexElt = header.indexOf(objBybrand.V360_Container__c.toUpperCase());
                    String field = '';
                  /*  
                    if(!field.contains('x') && objBybrand.V360_This__c){
                        field += 'x';
                    }
                    if(!field.contains('y') && objBybrand.V360_Last__c){
                        field += 'y';
                    }
                    if(!field.contains('z') && objBybrand.V360_Focus_Portfolio__c){
                        field += 'z';
                    }
*/
                    if(!field.contains('*') && objBybrand.V360_This__c){
                        field += '*';
                    }
                    if(!field.contains('@') && objBybrand.V360_Last__c){
                        field += '@';
                    }
                    if(!field.contains(':') && objBybrand.V360_Focus_Portfolio__c){
                        field += ':';
                    }
                    row[indexElt] =   field;                  
                    //row.add(indexElt, field);                 
                }
                tables.add(row);
            }
            tablesList.add(tables);            
        }
        return tablesList;
    }    
}