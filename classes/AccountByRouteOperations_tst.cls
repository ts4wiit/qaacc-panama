/****************************************************************************************************
   General Information
   -------------------
   author: Nelson Sáenz Leal
   email: nsaenz@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Test Class for AccountByRouteOperations_cls

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       11-08-2017        Nelson Sáenz Leal (NSL)      Creation Class
****************************************************************************************************/
@isTest
private class AccountByRouteOperations_tst
{
  public static ONTAP__Route__c objONTAPRoute;
  public static ONTAP__Route__c objONTAPRoute2;
  public static VisitPlan__c objISSMVisitPlan;
  public static AccountByVisitPlan__c objISSMAccByVisitPlan3;
  public static VisitPlan__c objISSMVisitPlan2;
  public static Account objAccount;
  public static Account objAccount2;
  public static Account objAccount3;
  public static   ONTAP__Vehicle__c objONTAPVehicle2;
  public static VisitPlan__c objISSMVisitPlan3;
  public static AccountByRoute__c objAccxRoute;

  private static VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();

  static void createData()
    {
        User userTest = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN].Id,
         LastName = 'Test',
         Email = 'asdasdasd@amamama.com',
         Username = 'asdasd@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST2',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
        );
        insert userTest;
        
        User u = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         managerId = userTest.Id
        );
        insert u;
        
        list<RecordType> lstRT = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' OR SobjectType = 'ONTAP__Route__c'];
        map<String, RecordType> mapRT = new map<String, RecordType>();
        for(RecordType rt : lstRT)
            mapRT.put(rt.DeveloperName, rt);
            
        objAccount3                         = new Account();
        objAccount3.RecordTypeId            = mapRT.get('SalesOrg').Id;
        objAccount3.Name                    = 'CMM Test3';
        objAccount3.StartTime__c            = '02:00';
        objAccount3.EndTime__c              = '01:00';
        objAccount3.ONTAP__SalesOgId__c     = '3117';
        insert objAccount3;

        objAccount2                      = new Account();
        objAccount2.RecordTypeId         = mapRT.get('SalesOffice').Id;
        objAccount2.Name                 = 'CMM Test';
        objAccount2.StartTime__c         = '02:00';
        objAccount2.EndTime__c           = '01:00';
        objAccount2.ONTAP__SalesOffId__c = 'FZ08';
        insert objAccount2;

        objAccount                                = new Account();
        objAccount.RecordTypeId                   = mapRT.get('Account').Id;
        objAccount.Name                           = 'CMM Dolores FZ08';
        objAccount.StartTime__c                   = '01:00';
        objAccount.EndTime__c                     = '01:00';
        objAccount.ISSM_SalesOffice__c            = objAccount2.Id;
        objAccount.ONCALL__Order_Block__c         = null;
        objAccount.ONCALL__SAP_Deleted_Flag__c    = null;
        objAccount.ONTAP__SalesOgId__c            = '3117';
        objAccount.ONTAP__SalesOffId__c           = 'FZ08';
        objAccount.ONTAP__SAP_Number__c        = '0888888888';
        insert objAccount;

        AccountTeamMember objAccountTeamMember  = new AccountTeamMember();
        objAccountTeamMember.AccountId          = objAccount.Id;
        objAccountTeamMember.TeamMemberRole     = 'Presales';
        objAccountTeamMember.UserId             = u.Id;
        insert objAccountTeamMember;

        AccountTeamMember objAccountTeamMember2     = new AccountTeamMember();
        objAccountTeamMember2.AccountId             = objAccount.Id;
        objAccountTeamMember2.TeamMemberRole        = 'Telesales';
        objAccountTeamMember2.UserId                = u.Id;
        insert objAccountTeamMember2;

        AccountTeamMember objAccountTeamMember3     = new AccountTeamMember();
        objAccountTeamMember3.AccountId             = objAccount.Id;
        objAccountTeamMember3.TeamMemberRole        = 'Supervisor de Ventas';
        objAccountTeamMember3.UserId                = u.Id;
        insert objAccountTeamMember3;


        ONTAP__Vehicle__c objONTAPVehicle       = new ONTAP__Vehicle__c();
        objONTAPVehicle.ONTAP__VehicleId__c     = '112345-23';
        objONTAPVehicle.ONTAP__VehicleName__c   = 'Zurdo Movil';
        objONTAPVehicle.ONTAP__SalesOffice__c = objAccount2.id;
        insert objONTAPVehicle;

        objONTAPVehicle2       = new ONTAP__Vehicle__c();
        objONTAPVehicle2.ONTAP__VehicleId__c     = '112345-23';
        objONTAPVehicle2.ONTAP__VehicleName__c   = 'Zurdo Movil';
        objONTAPVehicle2.ONTAP__SalesOffice__c = objAccount2.id;
        insert objONTAPVehicle2;

        objONTAPRoute                       = new ONTAP__Route__c();
        objONTAPRoute.RecordTypeId      = mapRT.get('Sales').Id;
        objONTAPRoute.ServiceModel__c       = 'Presales';
        objONTAPRoute.ONTAP__SalesOffice__c = objAccount2.Id;
        objONTAPRoute.RouteManager__c       = u.Id;
        objONTAPRoute.Supervisor__c         = u.id;
        objONTAPRoute.Vehicle__c            = objONTAPVehicle.id;
        insert objONTAPRoute;
        
        System.debug('\n--##-- '+objONTAPRoute.ONTAP__SalesOffice__c+' == '+objAccount.ISSM_SalesOffice__c);

        objAccxRoute                        = new AccountByRoute__c();
        objAccxRoute.Route__c               = objONTAPRoute.Id;
        objAccxRoute.Account__c             = objAccount.Id;
        objAccxRoute.Saturday__c            =   true;
        objAccxRoute.Wednesday__c           =   true;
        objAccxRoute.Thursday__c            =   true;
        objAccxRoute.Tuesday__c             =   true;
        objAccxRoute.Friday__c              =   true;
        objAccxRoute.Monday__c              =   true;
        objAccxRoute.Sunday__c              =   false;
        insert objAccxRoute;

        objONTAPRoute2                       = new ONTAP__Route__c();
        objONTAPRoute2.RecordTypeId       = mapRT.get('Sales').Id;
        objONTAPRoute2.ServiceModel__c       = 'Telesales';
        objONTAPRoute2.ONTAP__SalesOffice__c = objAccount2.Id;
        objONTAPRoute2.RouteManager__c       = userTest.Id;
        objONTAPRoute2.Supervisor__c         = userTest.id;
        objONTAPRoute2.Vehicle__c            = objONTAPVehicle2.id;
        insert objONTAPRoute2;

        
        objISSMVisitPlan                    =   new VisitPlan__c();
        objISSMVisitPlan.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan.Saturday__c        =   true;
        objISSMVisitPlan.Wednesday__c       =   true;
        objISSMVisitPlan.Thursday__c        =   true;
        objISSMVisitPlan.Tuesday__c         =   true;
        objISSMVisitPlan.Friday__c          =   true;
        objISSMVisitPlan.Monday__c          =   true;
        objISSMVisitPlan.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan;

        objISSMVisitPlan3                    =   new VisitPlan__c();
        objISSMVisitPlan3.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan3.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan3.Saturday__c        =   false;
        objISSMVisitPlan3.Wednesday__c       =   false;
        objISSMVisitPlan3.Thursday__c        =   false;
        objISSMVisitPlan3.Tuesday__c         =   false;
        objISSMVisitPlan3.Friday__c          =   false;
        objISSMVisitPlan3.Monday__c          =   false;
        objISSMVisitPlan3.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan3;

        objISSMVisitPlan2                    =   new VisitPlan__c();
        objISSMVisitPlan2.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan2.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan2.Saturday__c        =   false;
        objISSMVisitPlan2.Wednesday__c       =   false;
        objISSMVisitPlan2.Thursday__c        =   false;
        objISSMVisitPlan2.Tuesday__c         =   false;
        objISSMVisitPlan2.Friday__c          =   false;
        objISSMVisitPlan2.Monday__c          =   false;
        objISSMVisitPlan2.Route__c           =   objONTAPRoute2.Id;
        insert objISSMVisitPlan2;

        objISSMAccByVisitPlan3 =   new AccountByVisitPlan__c();
        objISSMAccByVisitPlan3.Account__c            =   objAccount.Id;
        objISSMAccByVisitPlan3.VisitPlan__c          =   objISSMVisitPlan.Id;
        objISSMAccByVisitPlan3.Sequence__c           =   1;
        objISSMAccByVisitPlan3.WeeklyPeriod__c       =   '1';
        objISSMAccByVisitPlan3.LastVisitDate__c      =   null;
        objISSMAccByVisitPlan3.Saturday__c           =   true;
        objISSMAccByVisitPlan3.Thursday__c           =   true;
        objISSMAccByVisitPlan3.Tuesday__c            =   true;
        objISSMAccByVisitPlan3.Friday__c             =   true;
        objISSMAccByVisitPlan3.Monday__c             =   true;
        objISSMAccByVisitPlan3.Wednesday__c          =   true;
        insert objISSMAccByVisitPlan3;

        Date dtNextVisit = Date.Today().addDays(Integer.valueOf(label.VisitPeriodConf));

        ONTAP__Tour__c objONTAPTour   = new ONTAP__Tour__c();
        objONTAPTour.ONTAP__TourId__c     = 'Test-Tour';
        objONTAPTour.ONTAP__TourStatus__c = visitPlanSettings.CreatedTourStatus__c;
        objONTAPTour.TourSubStatus__c     = visitPlanSettings.CreatedTourSubStatus__c;
        objONTAPTour.VisitPlan__c         = objISSMVisitPlan.Id;
        insert objONTAPTour;

        Event objEvent = new Event();
        objEvent.WhatId                          =   objISSMAccByVisitPlan3.Account__c;
        objEvent.Subject                         =   label.Subject;
        objEvent.StartDateTime                   =   DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(),12, 00, 0);
        objEvent.EndDateTime                     =   DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(), 13, 00, 0);
        objEvent.ONTAP__Estado_de_visita__c      =   label.Estado_de_visita;
        objEvent.EventSubestatus__c              =   label.EventSubestatus;
        objEvent.Sequence__c                     =   objISSMAccByVisitPlan3.Sequence__c;
        objEvent.VisitList__c                    =   objONTAPTour.Id;
        objEvent.CustomerId__c           =  '0888888888';
        insert objEvent;

        /*// Create Endpoint configurations
        ISSM_PriceEngineConfigWS__c config1 = TestUtils_tst.generatePriceEngineConfig('InsertHerokuEvents',
            'BASIC token_here',
            TestUtils_tst.STR_ENDPOINT_1,
            'application/json; charset=UTF-8',
            'POST');
        insert config1;

        ISSM_PriceEngineConfigWS__c config2 = TestUtils_tst.generatePriceEngineConfig('InsertHerokuTours',
            'BASIC token_here',
            TestUtils_tst.STR_ENDPOINT_2,
            'application/json; charset=UTF-8',
            'POST');
        insert config2;

        ISSM_PriceEngineConfigWS__c config3 = TestUtils_tst.generatePriceEngineConfig('DeleteHerokuEvents',
            'BASIC token_here',
            TestUtils_tst.STR_ENDPOINT_3,
            'application/json; charset=UTF-8',
            'POST');
        insert config3;*/
  }

    @isTest static void test_method_one()
  {
    insertCustomSetting();
    
    // Set mock callout class 
    Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_tst());
    TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','AfterInsertSync');
    TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterInsertSync');
    createData();
    /*SingleRequestMock_tst requestMock1 = new SingleRequestMock_tst('{"result":"ok"}');
    MultiRequestMock_tst multiRequestMock = new MultiRequestMock_tst();
    multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_1, requestMock1);
    multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_3, requestMock1);
    Test.setMock(HttpCalloutMock.class, multiRequestMock);*/

    Test.startTest();

    objAccxRoute.Monday__c              =   false;
    objAccxRoute.Saturday__c            =   false;
    objAccxRoute.Wednesday__c           =   false;
    objAccxRoute.Thursday__c            =   false;
    objAccxRoute.Tuesday__c             =   false;
    objAccxRoute.Friday__c              =   false;
    update objAccxRoute;

    delete objAccxRoute;
    Test.stopTest();
    }
  @isTest static void test_method_two()
  {
    insertCustomSetting();
    
    Test.startTest();
    TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','AfterInsertSync');
    TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterDeleteSync');  
    // Set mock callout class 
    Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_tst());
    createData();
    
    /*SingleRequestMock_tst requestMock1 = new SingleRequestMock_tst('{"result":"ok"}');
    MultiRequestMock_tst multiRequestMock = new MultiRequestMock_tst();
    multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_1, requestMock1);
    multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_3, requestMock1);
    Test.setMock(HttpCalloutMock.class, multiRequestMock);*/

    TriggerExecutionControl_cls.resetTriggerExecutionStatus('AccountByRoute_tgr','AfterUpdate');

    objAccxRoute.Monday__c              =   false;
    objAccxRoute.Saturday__c            =   false;
    objAccxRoute.Wednesday__c           =   false;
    objAccxRoute.Thursday__c            =   false;
    objAccxRoute.Tuesday__c             =   false;
    objAccxRoute.Friday__c              =   false;
    objAccxRoute.Sunday__c              =   false;
    update objAccxRoute;

    TriggerExecutionControl_cls.resetTriggerExecutionStatus('AccountByRoute_tgr','AfterUpdate');

    objAccxRoute.Monday__c              =   true;
    objAccxRoute.Saturday__c            =   true;
    objAccxRoute.Wednesday__c           =   true;
    objAccxRoute.Thursday__c            =   true;
    objAccxRoute.Tuesday__c             =   true;
    objAccxRoute.Friday__c              =   true;
    objAccxRoute.Sunday__c              =   true;
    update objAccxRoute;

    TriggerExecutionControl_cls.resetTriggerExecutionStatus('AccountByRoute_tgr','AfterUpdate');

    objAccxRoute.Sunday__c              =   false;
    update objAccxRoute;
    Test.stopTest();
  }

  @isTest static void test_method_three()
  {
    insertCustomSetting();
    TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','AfterInsertSync');
    TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterInsertSync');
    Test.startTest();
    
    // Set mock callout class 
    Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_tst());
    createData();

    /*SingleRequestMock_tst requestMock1 = new SingleRequestMock_tst('{"result":"ok"}');
    MultiRequestMock_tst multiRequestMock = new MultiRequestMock_tst();
    multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_1, requestMock1);
    multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_3, requestMock1);
    Test.setMock(HttpCalloutMock.class, multiRequestMock);*/

    TriggerExecutionControl_cls.resetTriggerExecutionStatus('AccountByRoute_tgr','AfterUpdate');

    objONTAPRoute2.ServiceModel__c       = 'Telesales';
    update objONTAPRoute2;

    TriggerExecutionControl_cls.resetTriggerExecutionStatus('AccountByRoute_tgr','AfterUpdate');
  }
  
  public static void insertCustomSetting(){
        SyncHerokuParams__c objConf = new SyncHerokuParams__c();
        objConf.Name = 'SyncToursEvents';
        objConf.StartTime__c = 0;
        objConf.EndTime__c = 24;
        objConf.IsActive__c = true;
        objConf.LastModifyDate__c = DateTime.now().addDays(-30);
        objConf.RecordTypeIds__c = 'Presales';
        objConf.RunFrequency__c = 60;
        insert objConf;
        
        list<ISSM_PriceEngineConfigWS__c> lstConf = new list<ISSM_PriceEngineConfigWS__c>();
        ISSM_PriceEngineConfigWS__c objConf1 = new ISSM_PriceEngineConfigWS__c();
        objConf1.Name = 'DeleteHerokuTours';
        objConf1.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf1.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/deletetours';
        objConf1.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf1.ISSM_Method__c = 'DELETE';
        lstConf.add(objConf1);
        
        ISSM_PriceEngineConfigWS__c objConf2 = new ISSM_PriceEngineConfigWS__c();
        objConf2.Name = 'DeleteHerokuEvents';
        objConf2.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf2.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/deleteevents';
        objConf2.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf2.ISSM_Method__c = 'DELETE';
        lstConf.add(objConf2);
        
        ISSM_PriceEngineConfigWS__c objConf3 = new ISSM_PriceEngineConfigWS__c();
        objConf3.Name = 'GetHerokuTours';
        objConf3.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf3.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/gettours';
        objConf3.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf3.ISSM_Method__c = 'POST';
        lstConf.add(objConf3);
        
        ISSM_PriceEngineConfigWS__c objConf4 = new ISSM_PriceEngineConfigWS__c();
        objConf4.Name = 'GetHerokuEvents';
        objConf4.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf4.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/getevents';
        objConf4.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf4.ISSM_Method__c = 'POST';
        lstConf.add(objConf4);
        
        ISSM_PriceEngineConfigWS__c objConf5 = new ISSM_PriceEngineConfigWS__c();
        objConf5.Name = 'InsertHerokuTours';
        objConf5.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf5.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/inserttours';
        objConf5.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf5.ISSM_Method__c = 'POST';
        lstConf.add(objConf5);
        
        ISSM_PriceEngineConfigWS__c objConf6 = new ISSM_PriceEngineConfigWS__c();
        objConf6.Name = 'InsertHerokuEvents';
        objConf6.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf6.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/insertevents';
        objConf6.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf6.ISSM_Method__c = 'POST';
        lstConf.add(objConf6);
        
        ISSM_PriceEngineConfigWS__c objConf7 = new ISSM_PriceEngineConfigWS__c();
        objConf7.Name = 'UpdateHerokuTours';
        objConf7.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf7.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/updatetours';
        objConf7.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf7.ISSM_Method__c = 'PUT';
        lstConf.add(objConf7);
        
        ISSM_PriceEngineConfigWS__c objConf8 = new ISSM_PriceEngineConfigWS__c();
        objConf8.Name = 'UpdateHerokuEvents';
        objConf8.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf8.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/updateevents';
        objConf8.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf8.ISSM_Method__c = 'PUT';
        lstConf.add(objConf8);
        
        insert lstConf;
    }
}