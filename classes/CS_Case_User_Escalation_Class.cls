/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASE_USER_ESCALATION_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 15/01/ 2019     Jose Luis Vargas            Creacion de la clase para escalacion de usuarios
 * 21/01/2019      Jose Luis Vargas            Se eliminan validaciones  para el campo de correo electrónico, ya que es un campo obligatorio
 */

public class CS_Case_User_Escalation_Class 
{
    private Map<Id, User> mapUsers;
    public CS_CASE_USER_ESCALATION_CLASS() {}
    
    public CS_CASE_USER_ESCALATION_CLASS(Set<Id> setUsers) {
        mapUsers = getMapUsers(setUsers);
    }
    
    /**
    * Method for assign the escalation for user assignation
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Case generate, Owner Id
    * @return Void
    */
    public void AssignEscalationUser(string idCase, string idUser)
    {   
        Case oUpdateCase = new Case();
        User oDetailUser = new User();
        
        string emailOwnerCase = '';
        string emailManagerN1 = '';
        string emailManagerN2 = '';
        string emailManagerN3 = '';
        string emailManagerN4 = '';
        string emailManagerN5 = '';
        string idManager = '';
        integer scalationLevel = 1;
        oUpdateCase.Id = idCase;
        
        try
        {    
            /* Se obtiene el manager del owner del caso */
            oDetailUser = GetEscalationMail(idUser);
            emailOwnerCase = oDetailUser.Email;
            
            /* Se valida si se encontro un supervisor del owner del caso */
            if(oDetailUser.Manager.id != null)
            {
                while(scalationLevel <= 5)
                {
                    switch on scalationLevel
                    {
                        when 1 
                        { 
                            oUpdateCase.ISSM_Email1CommunicationLevel3__c = oDetailUser.Manager.Email;
                            idManager = oDetailUser.Manager.Id;
                            emailManagerN1 = oDetailUser.Manager.Email;
                        }
                        when 2
                        {
                            oDetailUser = GetEscalationMail(idManager);
                            if(oDetailUser.Manager.Id != null)
                            {
                                idManager = oDetailUser.Manager.Id;
                                oUpdateCase.ISSM_Email1CommunicationLevel4__c   = oDetailUser.Manager.Email;
                                emailManagerN2 = oDetailUser.Manager.Email;
                            }
                            else
                            {
                                oUpdateCase.ISSM_Email1CommunicationLevel4__c = emailManagerN1; 
                                oUpdateCase.ISSM_Email2CommunicationLevel3__c = emailManagerN1;
                                oUpdateCase.ISSM_Email2CommunicationLevel4__c = emailManagerN1;
                                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailManagerN1;
                                break;
                            }
                        }
                        when 3
                        {
                            oDetailUser = GetEscalationMail(idManager);
                            if(oDetailUser.Manager.Id != null)
                            {
                                idManager = oDetailUser.Manager.Id;
                                oUpdateCase.ISSM_Email2CommunicationLevel3__c = oDetailUser.Manager.Email;
                                emailManagerN3 = oDetailUser.Manager.Email;
                            }
                            else
                            {
                                oUpdateCase.ISSM_Email2CommunicationLevel3__c = emailManagerN2;
                                oUpdateCase.ISSM_Email2CommunicationLevel4__c = emailManagerN2;
                                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailManagerN2;
                                break;
                            }
                        }
                        when 4
                        {
                            oDetailUser = GetEscalationMail(idManager);
                            if(oDetailUser.Manager.Id != null)
                            {
                                idManager = oDetailUser.Manager.Id;
                                oUpdateCase.ISSM_Email2CommunicationLevel4__c = oDetailUser.Manager.Email;
                                emailManagerN4 = oDetailUser.Manager.Email;
                            }
                            else
                            {
                                oUpdateCase.ISSM_Email2CommunicationLevel4__c = emailManagerN3;
                                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailManagerN3;
                                break;
                            }
                        }
                        when 5
                        {
                            oDetailUser = GetEscalationMail(idManager);
                            if(oDetailUser.Manager.Id != null)
                                    oUpdateCase.ISSM_Email3CommunicationLevel3__c = oDetailUser.Manager.Email;
                            else
                            {
                                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailManagerN4;
                                break;
                            }
                        }
                    }
                    scalationLevel++;
                }
            }
            else /* Si no se encontro Manager del Owner del caso los escalamientos se enviaran a su correo */
            {
                oUpdateCase.ISSM_Email1CommunicationLevel3__c = emailOwnerCase;
                oUpdateCase.ISSM_Email1CommunicationLevel4__c = emailOwnerCase; 
                oUpdateCase.ISSM_Email2CommunicationLevel3__c = emailOwnerCase;
                oUpdateCase.ISSM_Email2CommunicationLevel4__c = emailOwnerCase;
                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailOwnerCase;
            }
            
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            Update oUpdateCase;
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_USER_ESCALATION_CLASS.AssignEscalationUser Message: ' + ex.getMessage());   
            System.debug('CS_CASE_USER_ESCALATION_CLASS.AssignEscalationUser Cause: ' + ex.getCause());   
            System.debug('CS_CASE_USER_ESCALATION_CLASS.AssignEscalationUser Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_USER_ESCALATION_CLASS.AssignEscalationUser Stack trace: ' + ex.getStackTraceString());
        }
    }
    
    /**
    * Method for assign the escalation for user assignation
    * @author: gabriel.e.garcia@accenture.com
    * @param Id Case generate, Owner Id
    * @return Void
    */
    public Case AssignEscalationUserTrigger(string idCase, Id userId)
    {   
        Case oUpdateCase = new Case();
        User oDetailUser = new User();
        oDetailUser = mapUsers.get(userId);
        
        string emailOwnerCase = '';
        string emailManagerN1 = '';
        string emailManagerN2 = '';
        string emailManagerN3 = '';
        string emailManagerN4 = '';
        string emailManagerN5 = '';
        string idManager = '';
        integer scalationLevel = 1;
        oUpdateCase.Id = idCase;
        
        try
        {    
            /* Se obtiene el manager del owner del caso */
            emailOwnerCase = oDetailUser != null ? oDetailUser.Email : '';
            
            /* Se valida si se encontro un supervisor del owner del caso */
            if(oDetailUser != null && oDetailUser.Manager.id != null)
            {
                while(scalationLevel <= 5)
                {
                    switch on scalationLevel
                    {
                        when 1 
                        { 
                            oUpdateCase.ISSM_Email1CommunicationLevel3__c = oDetailUser.Manager.Email;
                            idManager = oDetailUser.Manager.Id;
                            emailManagerN1 = oDetailUser.Manager.Email;
                        }
                        when 2
                        {
                            if(oDetailUser.Manager.Manager.Id != null)
                            {
                                idManager = oDetailUser.Manager.Manager.Id;
                                oUpdateCase.ISSM_Email1CommunicationLevel4__c   = oDetailUser.Manager.Manager.Email;
                                emailManagerN2 = oDetailUser.Manager.Manager.Email;
                            }
                            else
                            {
                                oUpdateCase.ISSM_Email1CommunicationLevel4__c = emailManagerN1; 
                                oUpdateCase.ISSM_Email2CommunicationLevel3__c = emailManagerN1;
                                oUpdateCase.ISSM_Email2CommunicationLevel4__c = emailManagerN1;
                                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailManagerN1;
                                break;
                            }
                        }
                        when 3
                        {
                            if(oDetailUser.Manager.Manager.Manager.Id != null)
                            {
                                idManager = oDetailUser.Manager.Manager.Manager.Id;
                                oUpdateCase.ISSM_Email2CommunicationLevel3__c = oDetailUser.Manager.Manager.Manager.Email;
                                emailManagerN3 = oDetailUser.Manager.Manager.Manager.Email;
                            }
                            else
                            {
                                oUpdateCase.ISSM_Email2CommunicationLevel3__c = emailManagerN2;
                                oUpdateCase.ISSM_Email2CommunicationLevel4__c = emailManagerN2;
                                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailManagerN2;
                                break;
                            }
                        }
                        when 4
                        {
                            if(oDetailUser.Manager.Manager.Manager.Manager.Id != null)
                            {
                                idManager = oDetailUser.Manager.Manager.Manager.Manager.Id;
                                oUpdateCase.ISSM_Email2CommunicationLevel4__c = oDetailUser.Manager.Manager.Manager.Manager.Email;
                                emailManagerN4 = oDetailUser.Manager.Manager.Manager.Manager.Email;
                            }
                            else
                            {
                                oUpdateCase.ISSM_Email2CommunicationLevel4__c = emailManagerN3;
                                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailManagerN3;
                                break;
                            }
                        }
                        when 5
                        {
                            if(oDetailUser.Manager.Manager.Manager.Manager.Manager.Id != null)
                                    oUpdateCase.ISSM_Email3CommunicationLevel3__c = oDetailUser.Manager.Manager.Manager.Manager.Manager.Email;
                            else
                            {
                                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailManagerN4;
                                break;
                            }
                        }
                    }
                    scalationLevel++;
                }
            }
            else /* Si no se encontro Manager del Owner del caso los escalamientos se enviaran a su correo */
            {
                oUpdateCase.ISSM_Email1CommunicationLevel3__c = emailOwnerCase;
                oUpdateCase.ISSM_Email1CommunicationLevel4__c = emailOwnerCase; 
                oUpdateCase.ISSM_Email2CommunicationLevel3__c = emailOwnerCase;
                oUpdateCase.ISSM_Email2CommunicationLevel4__c = emailOwnerCase;
                oUpdateCase.ISSM_Email3CommunicationLevel3__c = emailOwnerCase;
            }
            
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_USER_ESCALATION_CLASS.AssignEscalationUser Message: ' + ex.getMessage());   
            System.debug('CS_CASE_USER_ESCALATION_CLASS.AssignEscalationUser Cause: ' + ex.getCause());   
            System.debug('CS_CASE_USER_ESCALATION_CLASS.AssignEscalationUser Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_USER_ESCALATION_CLASS.AssignEscalationUser Stack trace: ' + ex.getStackTraceString());
        }
        
        return oUpdateCase;
    }
    
    /**
    * Method for get the user´s manager
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id User
    * @return User Object with the Id and email of manager
    */ 
    private User GetEscalationMail(string idUser)
    {
        User oMailUser = new User();
        
        oMailUser = [SELECT Email, Manager.Id, Manager.Email
                     FROM User 
                     WHERE Id =: idUser];
        
        return oMailUser;
    }
    
    /**
    * Method for get the users manager
    * @author: gabriel.e.garciaa@accenture.com
    * @param Set<Id> setUsers
    * @return Map<Id, User> mapUsers with the Id and email of manager for 5 leves
    */ 
    private Map<Id, User> getMapUsers(Set<Id> setUsers){
        Map<Id, User> mapUsers = new Map<Id, User>([SELECT Id, Email, 
                                                    Manager.Id, Manager.Email, 
                                                    Manager.Manager.Id, Manager.Manager.Email,
                                                    Manager.Manager.Manager.Id, Manager.Manager.Manager.Email,
                                                    Manager.Manager.Manager.Manager.Id, Manager.Manager.Manager.Manager.Email,
                                                    Manager.Manager.Manager.Manager.Manager.Id, Manager.Manager.Manager.Manager.Manager.Email
                                                    FROM User 
                                                    WHERE Id IN: setUsers]);
        return mapUsers;
    }
}