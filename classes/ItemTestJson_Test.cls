/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: ItemTestJson_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class ItemTestJson_Test {
    
    /**
    * Test method for set values in the Model ItemTestJson_Test
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void ItemTestJson_UseCase1(){
		List<DealsTestJson> appliedDeals = new List<DealsTestJson>();
        ItemTestJson itemTestJson= new ItemTestJson();
        itemTestJson.sku = 'Test';
        itemTestJson.freegood = 'Test';
        itemTestJson.unit = 'Test';
        itemTestJson.price = 0.0;
        itemTestJson.unitprice = 0.0;
        itemTestJson.subtotal = 0.0;
        itemTestJson.tax = 0.0;
        itemTestJson.total = 0.0;
        itemTestJson.quantity = 0;
        itemTestJson.discount = 0;
        itemTestJson.appliedDeals = appliedDeals;
    }
}