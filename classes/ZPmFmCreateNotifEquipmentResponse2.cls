public class ZPmFmCreateNotifEquipmentResponse2 {
  
    public class Item {
    public String message {get;set;}   
    public String msg {get;set;}
      public Item(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'Message') {
                            System.debug('Message: ' + parser.getText());
                            message = parser.getText() ;
                        }
                    }
                }
            }
        }
    }

 
public EtReturn EtReturn {get;set;}    

    public ZPmFmCreateNotifEquipmentResponse2(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                System.debug('texto' + text);
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'EtReturn') {
                        EtReturn = new EtReturn(parser);
                    }
                }
            }
        }
    }

    public static ZPmFmCreateNotifEquipmentResponse2 parse(String json) {
        System.debug(json);
        System.JSONParser parser = System.JSON.createParser(json);
        return new ZPmFmCreateNotifEquipmentResponse2(parser);
    }


   /**
   * Method to consume the objects inside de Json (token iteration)
   * Created By: heron.zurita@accenture.com
   * @param JSONParser json
   * @return void
   */

    public class EtReturn {
        public Item item {get;set;}
        public EtReturn(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'item') {
                            item = new Item(parser);
                        }
                    }
                }
            }
        }
    }  

}