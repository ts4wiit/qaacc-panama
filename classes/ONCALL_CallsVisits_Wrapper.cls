/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* -----------------------------------------------------------------------------
* Clase: ONCALL_CallsVisits_Wrapper.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------
* Date                 User                   Description
* 14/02/2019     Oscar García        Creation of methods.
*
* @author: o.a.garcia.martinez@accenture.com
-------------------------------------------------------------------------------*/
	/**
    * Structure to contain calls and visits information 
    * Created By: o.a.garcia.martinez@accenture.com
    * @param void
    * @return void
    */
public class ONCALL_CallsVisits_Wrapper {    
	@AuraEnabled public String recordId{set;get;}
	@AuraEnabled public String objectName{set;get;}
    @AuraEnabled public String mean{set;get;}
    @AuraEnabled public String type{set;get;} 
    @AuraEnabled public Decimal amount{set;get;} 
    @AuraEnabled public Datetime datum{set;get;}
    
}