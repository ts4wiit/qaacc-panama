/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_TRIGGER_CASE_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 14/03/2019      Debbie Zacarias          Creacion de la clase para el trigger que se ejecuta al insertar
 *                                          o actualizar un Entitlement.
 */

public class CS_Trigger_Entitlement_Class extends TriggerHandlerCustom{
    
    private Map<Id, Entitlement> MapEntitlementNew;
    private Map<Id, Entitlement> MapEntitlementOld;
    private List<Entitlement> lstNewEntitlement;
    private List<Entitlement> lstOldEntitlement;
    
    public CS_Trigger_Entitlement_Class()
    {
        this.MapEntitlementNew = (Map<Id, Entitlement>)Trigger.newMap;
        this.MapEntitlementOld = (Map<Id, Entitlement>)Trigger.oldMap;
        this.lstNewEntitlement = (List<Entitlement>)Trigger.new;
        this.lstOldEntitlement = (List<Entitlement>)Trigger.old;
    }
    
    /**
    * Method that runs when a case is inserted
    * @author: d.zacarias.cantillo@accenture.com
    * @param Void
    * @return Void
    */
    public override void beforeInsert()
    {
        InsertEntitlementAccount(this.lstNewEntitlement);
    }    
    
    /**
    * Method for insert Entitlement
    * @author: d.zacarias.cantillo@accenture.com
    * @param List with the Entitlement
    * @return Void
    */
    private static void InsertEntitlementAccount(List<Entitlement> LstNewEntitlement)
    {
        Set<String> AccName = new Set<String>();
        Set<String> BHName = new Set<String>();
        Set<String> EPName = new Set<String>();
        
        for(Entitlement item: LstNewEntitlement)
        {
            AccName.add(item.CS_Account_Name__c);
            BHName.add(item.CS_Business_Hour_Name__c);
            EPName.add(Item.CS_Entitlement_Process_Name__c);
        }
        
        try
        {
            List<Account> lstAcc = [SELECT Id, Name FROM Account WHERE Name IN: AccName];
            List<SlaProcess> lstSLA = [SELECT Id, Name FROM SlaProcess WHERE Name IN: EPName];
            List<BusinessHours> lstBH = [SELECT Id, Name FROM BusinessHours WHERE Name IN: BHName];
                        
            for(Entitlement enti: LstNewEntitlement)
            {
                /* Asignacion de Cuenta */
                for(Account IdAccount : lstAcc)
                {
                    if(IdAccount.Name.equals(enti.CS_Account_Name__c))
                        enti.AccountId = IdAccount.Id;
                }
                
                /* Asignacion de Entitlement Process */
                for(SlaProcess Process : lstSLA)
                {
                    if(Process.Name.equals(enti.CS_Entitlement_Process_Name__c))
                        enti.SlaProcessId = Process.Id;
                }
                
                for(BusinessHours BHours : lstBH)
                {
                    if(BHours.Name.equals(enti.CS_Business_Hour_Name__c))
                        enti.BusinessHoursId = BHours.Id;
                }
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_TRIGGER_Entitlement_CLASS.InsertEntitlementAccount Message: ' + ex.getMessage());   
            System.debug('CS_TRIGGER_Entitlement_CLASS.InsertEntitlementAccount Cause: ' + ex.getCause());   
            System.debug('CS_TRIGGER_Entitlement_CLASS.InsertEntitlementAccount Line number: ' + ex.getLineNumber());   
            System.debug('CS_TRIGGER_Entitlement_CLASS.InsertEntitlementAccount Stack trace: ' + ex.getStackTraceString());
        }
    }
}