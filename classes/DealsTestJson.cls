/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: DealsTestJson.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/01/2019        Heron Zurita           Creation of methods.     
 */
public class DealsTestJson {

   		/**
        * Methods getters and setters for entity DealsTestJson
        * Created By: heron.zurita@accenture.com
        * @param void
        * @return void
        */
    	@AuraEnabled public Decimal concode{set;get;}
    
        @AuraEnabled public Double amount{set;get;}

        @AuraEnabled public Double percentage{set;get;}

        @AuraEnabled public Decimal pronr{set;get;}


}