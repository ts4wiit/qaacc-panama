@isTest
private class IREP_SharingRecordsWithCarrier_tst
{

 	@testSetup static void setup() {
        IREP_CreateObjects_tst.createFullTour(2, 2, 2);
    }


	@isTest
	static void testSharingRecordsBatch(){
		Test.startTest();
			IREP_SharingRecordsWithCarrier_bch objBatch = new IREP_SharingRecordsWithCarrier_bch();
			Database.executebatch(objBatch);
		Test.stopTest();


		list<ONTAP__Tour__c> lstTour = [Select Id, IREP_Carrier__c From ONTAP__Tour__c];


		list<ONTAP__Tour__Share> lstTourShare = [Select Id From ONTAP__Tour__Share Where ParentId = : lstTour[0].Id And UserOrGroupId = :lstTour[0].IREP_Carrier__c];
		list<ONTAP__Tour_Visit__Share> lstTourVisitShare = [Select Id From ONTAP__Tour_Visit__Share Where UserOrGroupId = :lstTour[0].IREP_Carrier__c];
		list<ONTAP__Order__Share> lstOrderShare = [Select Id From ONTAP__Order__Share Where UserOrGroupId = :lstTour[0].IREP_Carrier__c];
		list<ONTAP__Order_Item__Share> lstOrderItemShare = [Select Id From ONTAP__Order_Item__Share Where UserOrGroupId = :lstTour[0].IREP_Carrier__c];

		System.assert(lstTourShare.size()>0);
		System.assert(lstTourVisitShare.size()>0);
		System.assert(lstOrderShare.size()>0);
		System.assert(lstOrderItemShare.size()>0);
	}

	@isTest
	static void testSharingRecordsScheduler(){
		Test.startTest();
			IREP_SharingRecordsWithCarrier_sch objBatch = new IREP_SharingRecordsWithCarrier_sch();
			String sch = '0 0 23 * * ?';
			system.schedule('Share Records', sch, objBatch);
		Test.stopTest();
	}
}