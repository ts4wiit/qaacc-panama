/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_ItemsList_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Heron Zurita          Creation of methods.
*/

@isTest
private class HONES_ONCALL_ItemsList_Test{
    
    /**
    * Test method for set an Item List
    * Created By:heron.zurita@accenture.com
    * @param void
    * @return void
    */
 	@isTest static void HONES_ONCALL_ItemsList_UseCase1(){
    HONES_ONCALL_ItemsList tester=new HONES_ONCALL_ItemsList();
    	  tester.sku='tester';
          tester.quantity='tester';
          tester.measurecode='tester';
          tester.empties='tester';
      	  tester.posex='tester';
          tester.orderReason='tester';
    }
}