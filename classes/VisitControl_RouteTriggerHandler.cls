/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_RouteTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class VisitControl_RouteTriggerHandler extends TriggerHandlerCustom {

    private Map<Id, ONTAP__Route__c> newMap;
    private Map<Id, ONTAP__Route__c> oldMap;
    private List<ONTAP__Route__c> newList;
    private List<ONTAP__Route__c> oldList;
    
    /**
    * Constructor of the class
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public VisitControl_RouteTriggerHandler() {
        
        this.newMap = (Map<Id, ONTAP__Route__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__Route__c>) Trigger.oldMap;
        this.newList = (List<ONTAP__Route__c>) Trigger.new;
        this.oldList = (List<ONTAP__Route__c>) Trigger.old;
    }
    
    /**
    * Meethod for before insert
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void beforeInsert(){
        List<ONTAP__Route__c> objList = PreventExecutionUtil.validateRouteWithOutId(this.newList);
        relateHierarchyToRouteByAgent(objList);
    }
    
    /**
    * Method for before update
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void beforeUpdate(){
       List<ONTAP__Route__c> objList = PreventExecutionUtil.validaterouteWithId(this.newMap);
       relateHierarchyToRouteByAgent(objList);
    }
    
    /**
    * Method for related a hierarchy to a route by agent
    * Created By: g.martinez.cabral@accenture.com
    * @param List<ONTAP__Route__c> newList
    * @return void
    */
    public void relateHierarchyToRouteByAgent(List<ONTAP__Route__c> newList){
        
        SET<Id> usersId = New SET<Id>();
        SET<String> types = new SET<String>();
        SET<Id> salesZones = new SET<Id>();
        
         for(ONTAP__Route__c route:newList){
           
             if(route.RouteManager__c != NULL){
                 usersId.add(route.RouteManager__c);
             }
             if(route.ServiceModel__c != NULL){
                 types.add(route.ServiceModel__c);
             }
             if(route.VisitControl_SalesZone__c!=NULL){
                 salesZones.add(route.VisitControl_SalesZone__c);
             }  
        }

        for (List<V360_SalerPerZone__c > lst : [SELECT Id,V360_User__c,V360_SalesZone__c,V360_Type__c From V360_SalerPerZone__c WHERE V360_User__c IN:usersId AND V360_Type__c IN:Types AND V360_SalesZone__c IN: salesZones]){
            for(V360_SalerPerZone__c spz : lst){
                
                for(ONTAP__Route__c route:newList){
					
                    if(route.RouteManager__c!= NULL && route.ServiceModel__c!=NULL && route.VisitControl_SalesZone__c!=NULL && spz.V360_SalesZone__c != NULL  
                       && spz.V360_User__c != NULL && spz.V360_Type__c!= NULL && route.RouteManager__c == spz.V360_User__c && route.ServiceModel__c == spz.V360_Type__c 
                       && spz.V360_SalesZone__c == route.VisitControl_SalesZone__c){
                           
                            route.VisitControl_AgentByZone__c = spz.Id;
                    }
                }
            }
        }
    }
}