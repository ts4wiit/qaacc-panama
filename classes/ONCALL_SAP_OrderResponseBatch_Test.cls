/*******************************************************************************
Developed by: TS4
Author: Daniel Hernández
Proyect: HONES
Description: Apex class to test methods for ONCALL_SAP_OrderResponseBatch functionality

------ ---------- -------------------------- -----------------------------------
No.    Date       Autor                      Description
------ ---------- -------------------------- -----------------------------------
1.0    09/12/2019 Daniel Hernández           Created Class
*******************************************************************************/

@isTest
public class ONCALL_SAP_OrderResponseBatch_Test {
    @isTest static void wrapperClass() {
        ONCALL_SAP_OrderResponseBatch order = new ONCALL_SAP_OrderResponseBatch();
        order.message = 'Request Placed, an email notification will be send after update process finish.';
        order.statusCode = 200;
        order.success = true;
        ONCALL_SAP_OrderResponseBatch.parse('{"make":"SFDC","year":"2020"}');
    }
}