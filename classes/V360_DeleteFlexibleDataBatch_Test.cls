/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteFlexibleDataBatch_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Carlos Leal        Creation of methods.
 */
@isTest
public class V360_DeleteFlexibleDataBatch_Test {
   /**
   * Method test for delete all the flexible data in V360_DeleteFlexibleDataBatch.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testBatch(){
    V360_FlexibleData__c ob = new V360_FlexibleData__c(V360_Type__c='test',V360_value__c='test');
    insert ob;    
    V360_DeleteFlexibleDataBatch obj01 = new V360_DeleteFlexibleDataBatch();
    Database.executeBatch(obj01, 200);
}
}