/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONTAP_updateOrderCount.apxc
 * Versión: 1.0.0.0
 * 
 * Clase destinada a guardar el valor del ID de SAP en la orden
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 19/12/ 2018     Oscar Garcia		  o.a.garcia.martinez_accenture.com     Creación de la clase  
 */
@isTest
public class ONTAP_updateOrderCount_Test {   
  /*
   * Method that check the complete fuctionality 
   * Created By:o.a.garcia.martinez_accenture.com
   * @params void
   * @return void
	*/
  @isTest static void test_sumCounter_UseCase1(){
      
    Account acc = new Account(Name= 'Test Account');
      insert acc;
    
    ONTAP__Order__c order = new ONTAP__Order__c(ONTAP__OrderAccount__c = acc.id);
        order.ONCALL__SAP_Order_Item_Counter__c=1;
        order.ONCALL__Total_Order_Item_Quantity__c=1;
        order.ONTAP__DocumentationType__c='test';
        order.ISSM_PaymentMethod__c='Cash';
        order.ONTAP__DeliveryDate__c=System.today();
        order.ONCALL__SAP_Order_Number__c = '00121231283';
        insert order;  
        
        ONTAP__Product__c product= new ONTAP__Product__c();
        insert product;
        
      	List<ONTAP__Order_Item__c> orderItemLst = new List<ONTAP__Order_Item__c>();
        ONTAP__Order_Item__c orderItem = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id, ONTAP__Cond_Applied__c = 'Deal');
        orderItemLst.add(orderItem);
        ONTAP__Order_Item__c orderItem2 = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id, ONTAP__ItemProduct__c = product.id);
        orderItemLst.add(orderItem2);
        
		insert orderItemLst;
      
    ONTAP_updateOrderCount obj01 = new ONTAP_updateOrderCount();
    ONTAP_updateOrderCount.sumCounter(orderItemLst);
  }   

}