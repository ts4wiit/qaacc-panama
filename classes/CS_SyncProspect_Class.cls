/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_SyncProspect_Class.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 29/04/2019     Jose Luis Vargas          Creation of methods.
*/

public class CS_SyncProspect_Class 
{
    /**
    * method to execute the prospect sync
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    public void SyncProspect()
    {
        Id idRecordTypeHN = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
        Id idRecordTypeSV = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_SV).getRecordTypeId();
                       
        integer diasConsulta = integer.valueOf(Label.CS_Dias_Consulta_Prospecto);
        Date fechaInicio = Date.today().addDays(diasConsulta);
        Date fechaFin = Date.today();
        
        List<Account> lstProspect = new List<Account>();
        List<Account> lstProspectDelete = new List<Account>();
        Set<string> sIdentificationNumber = new Set<string>();
        Set<string> sIdProspect = new Set<string>();
        Map<string, Account> mpAccount = new Map<string, Account>();
        Map<Id, Case> mpProspectCase = new Map<Id, Case>();
        
        Account oAccountFinal = new Account();
        Case oCaseFinal = new Case();
        List<Case> lstCaseClose = new List<Case>();
        
        try
        {
            /* Se realiza la consulta de los prospectos que pertenescan a los record type Prospect_HN y Prospect_SV. Se Filtra tambien por la fecha de creacion, fecha actual menos 5 dias configurables */
            lstProspect = [SELECT Id, HONES_IdentificationNumber__c 
                           FROM Account 
                           WHERE (RecordTypeId =: idRecordTypeHN OR RecordTypeId =: idRecordTypeSV)
                           AND CS_Prospect_Create_Date__c <=: fechaFin
                           AND CS_Prospect_Create_Date__c >=: fechaInicio];
            
            if(lstProspect.size() > 0)
            {
                /* Se obtienen los numeros de Identificacion de los prospectos */
                for(Account oProspect : lstProspect)
                {
                    sIdentificationNumber.add(oProspect.HONES_IdentificationNumber__c);
                    sIdProspect.add(oProspect.Id);
                }
                mpAccount = getMapAccount(sIdentificationNumber);
                
                /* Se obtienen los casos de los prospectos */
                mpProspectCase = getMapCase(sIdProspect);
                
                /* Se valida si existen cuentas en el mapa */
                if(mpAccount != null && mpAccount.size() > 0)
                {
                    /* Se ejecuta el proceso por cada prospecto en la lista */
                    for(Account oProspect : lstProspect)
                    {
                        /* Se avlida si en el mapa de cuentas existe una con el numero de identificacion del prospecto */
                        if(mpAccount.get(oProspect.HONES_IdentificationNumber__c) != null)
                        {
                            lstProspectDelete.add(oProspect);
                            oAccountFinal = (Account)mpAccount.get(oProspect.HONES_IdentificationNumber__c);
                            /* Se valida si en el mapa de casos existe unos asociado al prospecto para asignar el Id de cuenta encontrado */
                            if(mpProspectCase != null && mpProspectCase.size() > 0 && mpProspectCase.get(oProspect.Id) != null)
                            {
                                oCaseFinal = (Case)mpProspectCase.get(oProspect.Id);
                                oCaseFinal.AccountId = oAccountFinal.Id;
                                lstCaseClose.add(oCaseFinal);
                                oCaseFinal = new Case();
                            }
                        }
                        oAccountFinal = new Account();
                    }
                    
                    /* Se ejecuta el proceso de cierre de casos */
                    CloseCaseProspect(lstCaseClose);
                    
                    /* Se eliminan los prospectos */
                    System.Debug('Delete Prospect: ' + lstProspectDelete);
                    Delete lstProspectDelete;
                }
            }
            
            /* Se ejecuta el proceso para borrar los prospectos con mas de 60 dias sin habere generado una cuenta */
            DeleteProspectWithoutAccount();
        }
        catch(Exception ex)
        {
            System.debug('CS_SyncProspect_Class.SyncProspect Message: ' + ex.getMessage());   
            System.debug('CS_SyncProspect_Class.SyncProspect Cause: ' + ex.getCause());   
            System.debug('CS_SyncProspect_Class.SyncProspect Line number: ' + ex.getLineNumber());   
            System.debug('CS_SyncProspect_Class.SyncProspect Stack trace: ' + ex.getStackTraceString());  
        }
    }
    
    /**
    * method to close case prospect and asign the id account
    * @author: jose.l.vargas.lara@accenture.com
    * @param lstCaseClose List with information case to close
    * @return Void
    */
    private void CloseCaseProspect(List<Case> lstCaseClose)
    {
        ONTAP__Case_Force__c oCaseForceClose = new ONTAP__Case_Force__c();
        List<ONTAP__Case_Force__c> lstCaseForceClose = new List<ONTAP__Case_Force__c>();
        List<CaseMilestone> lstMilestone = new List<CaseMilestone>();
        CS_RO_Cooler_Settings__c settings = CS_RO_Cooler_Settings__c.getOrgDefaults();
        Set<Id> sIdCase = new Set<Id>();
        Id idRecordTypeCloseCase = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(settings.CS_RT_RO_Generate_Client__c).getRecordTypeId();
        
        /* Se llena la informacion del caso y del caso force para cerrar*/
        for(Case oCaseClose : lstCaseClose)
        {
            sIdCase.add(oCaseClose.Id);
            oCaseClose.ISSM_SolutionNote__c = Label.CS_Solution_Prospect;
            oCaseClose.CS_Solution_Master__c = Label.CS_Solution_Master_Prospect;
            oCaseClose.Status =  System.Label.CS_Estatus_Caso_Cerrado;
            oCaseClose.IsStopped = true;
            oCaseClose.RecordTypeId = idRecordTypeCloseCase;
            
            oCaseForceClose.Id = oCaseClose.ISSM_CaseForceNumber__c;
            oCaseForceClose.ONTAP__Status__c = System.Label.CS_Estatus_Caso_Cerrado;
            oCaseForceClose.ONTAP__Account__c = oCaseClose.AccountId;
            lstCaseForceClose.add(oCaseForceClose);
            oCaseForceClose = new ONTAP__Case_Force__c();
        }
        
        /* Se obtienen los Milestone de los casos */
        lstMilestone = [SELECT Id, CompletionDate FROM CaseMilestone WHERE CaseId IN : sIdCase];
        if(lstMilestone.size() > 0)
        {
            /* Se actualizan los milestone */
            for(CaseMilestone oMilestone : lstMilestone)
            {
                oMilestone.CompletionDate = DateTime.now();
            }
            Update lstMilestone;
        }
        
        /* Se cierran los casos standard */
        ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
        Update lstCaseClose;
        
        /* Se cierran los casos force */
        ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
        Update lstCaseForceClose;
    }
    
    /**
    * method to delete prospect and case with 60 days being created
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    private void DeleteProspectWithoutAccount()
    {
        Id idRecordTypeHN = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
        Id idRecordTypeSV = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_SV).getRecordTypeId();
                       
        integer diasConsulta = integer.valueOf(Label.CS_Dias_Borrado_Prospecto);
        Date fechaInicio = Date.today().addDays(diasConsulta);
        Date fechaFin = Date.today();
        
        List<Account> lstProspectsDelete = new List<Account>();
        List<Case> lstCaseDelete = new List<Case>();
        
        ONTAP__Case_Force__c oCaseForceDelete = new ONTAP__Case_Force__c();
        List<ONTAP__Case_Force__c> lstCaseForceDelete = new List<ONTAP__Case_Force__c>();
        
        lstProspectsDelete  = [SELECT Id FROM Account 
                               WHERE (RecordTypeId =: idRecordTypeHN OR RecordTypeId =: idRecordTypeSV)
                               AND CS_Prospect_Create_Date__c <=: fechaFin
                               AND CS_Prospect_Create_Date__c >=: fechaInicio];
        
        if(lstProspectsDelete.size() > 0)
        {
            /* Se consultan los casos de los prospectos */
            lstCaseDelete = [SELECT Id, ISSM_CaseForceNumber__c FROM Case WHERE AccountId IN : lstProspectsDelete];
            if(lstCaseDelete.size() > 0)
            {
                /* Se obtienen los casos force a eliminar */
                for(Case oCase : lstCaseDelete)
                {
                    oCaseForceDelete.Id = oCase.ISSM_CaseForceNumber__c;
                    lstCaseForceDelete.add(oCaseForceDelete);
                    oCaseForceDelete = new ONTAP__Case_Force__c();
                }
                
                Delete lstCaseForceDelete; /* Se eliminan los casos force */
                Delete lstCaseDelete;  /* Se eliminan los casos */  
            }
                            
            /* Se eliminan los Prospectos */
            Delete lstProspectsDelete;
        }
    }
    
    /**
    * method to get a map with accounts for prospect
    * @author: jose.l.vargas.lara@accenture.com
    * @param sIdentificationNumber with identification number of prospect
    * @return Void
    */
    private Map<string, Account> getMapAccount(set<string> sIdentificationNumber)
    {
        Map<string, Account> mpAccount = new Map<string, Account>();
        List<Account> lstAccounts = new List<Account>();
        Id idRecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.ACCOUNT_RECORDTYPE_NAME).getRecordTypeId();
        
        /* Se realiza la consulta de cuentas que tengan el numero de identificacion igual a la del prospecto y con Record Type Account */
        lstAccounts = [SELECT Id, HONES_IdentificationNumber__c
                       FROM Account
                       WHERE HONES_IdentificationNumber__c IN : sIdentificationNumber
                       AND RecordTypeId =: idRecordTypeAccount];
        
        /* Se insertan los registros en el mapa*/
        for(Account oAccount : lstAccounts)
        {
            mpAccount.put(oAccount.HONES_IdentificationNumber__c, oAccount);  
        }
        
        return mpAccount;
    }
    
    /**
    * method to get a map with accounts for prospect
    * @author: jose.l.vargas.lara@accenture.com
    * @param sIdProspect with Prospect Id
    * @return Void
    */
    private Map<Id, Case> getMapCase(Set<string> sIdProspect)
    {
        Map<Id, Case> mpCaseProspect = new Map<Id, Case>();
        List<Case> lstCase = new List<Case>();
        
        /* Se realiza la consulta de casos de los prospectos */
        lstCase = [SELECT Id, AccountId, ISSM_SolutionNote__c, CS_Solution_Master__c, Status, IsStopped, RecordTypeId, ISSM_CaseForceNumber__c 
                   FROM Case 
                   WHERE AccountId IN : sIdProspect];
        
        /* Se insertan los registros en el mapa */
        for(Case oCase : lstCase)
        {
            mpCaseProspect.put(oCase.AccountId, oCase);
        }
        
        return mpCaseProspect;
    }
}