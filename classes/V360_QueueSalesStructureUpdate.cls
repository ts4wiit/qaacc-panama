/* ---------------------------------------------------------------------------- 
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_QueueSalesStructureUpdate.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
public without sharing class V360_QueueSalesStructureUpdate implements Queueable {
    private List<Account> objs;
    
    /**
    * method for update the sales structure whit accounts.
    * @author: g.martinez.cabral@accenture.com
    * @param List<Account> accounts
    * @return Void
    */
    public V360_QueueSalesStructureUpdate(List<Account> accounts) {
        this.objs = accounts;
    }
    
    /**
    * method for execute an update for accounts
    * @author: g.martinez.cabral@accenture.com
    * @param QueueableContext queCont
    * @return Void
    */
    public void execute(QueueableContext queCont) {
        Update objs;
    }
    
}