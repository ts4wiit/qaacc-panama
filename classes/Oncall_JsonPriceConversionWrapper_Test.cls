/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: Oncall_JsonPriceConversionWrapper_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class Oncall_JsonPriceConversionWrapper_Test {
	
    /**
    * Test method for create the Query for the object
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void JsonPrice_UseCase1(){
        Oncall_JsonPriceConversionWrapper jsonPrice = new Oncall_JsonPriceConversionWrapper();
        List<ItemTestJson> ItemsList = new List<ItemTestJson>();
        jsonPrice.Items = ItemsList;
        OrderTestJSON order = new OrderTestJSON();
        order.accountId = '';
        order.discount = '';
        order.paymentMethod = '';
        order.subtotal = 0.0;
        order.tax = 0.0;
        order.total = 100;
        
        jsonPrice.order = order;
    }
}