/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: Modelo - DSD
Descripción: Clase Wrapper para almacenar Planes de Visita y Cuentas por Plan de Visita

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    16/08/2017 Daniel Peñaloza            Clase creada
*******************************************************************************/

public class VisitPlanWrapper {
    public VisitPlan__c objVisitPlan { get; set; }
    public List<AccountByVisitPlan__c> lstAccounts { get; set; }
}