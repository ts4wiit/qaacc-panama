/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteAllAssetSchudeler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteAllAssetSchudeler implements schedulable
{
    /**
   * Execute a Schudeler for execute the batch for delete all the account assets.
   * @author: c.leal.beltran@accenture.com
   * @param SchedulableContext sc
   * @return void
   */
    global void execute(SchedulableContext sc)
    {
    V360_DeleteAssetsBatch a = new V360_DeleteAssetsBatch(); 
      database.executebatch(a,200);
    }
}