/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_TRIGGER_CASE_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 14/03/2019           Debbie Zacarias       Crecion de la clase para testing de la clase CS_TRIGGER_ENTITLEMENT_CLASS 
 */

@isTest
public class CS_Trigger_Entitlement_Class_Test {
    
    /**
    * Method for test the method beforeInsert
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
  
    @isTest static void test_beforeInsert(){
               
        Entitlement oNewEtl = new Entitlement();
        Account oNewAcc = new Account();
        CS_Trigger_Entitlement_Class  entitlementTrigger = new CS_Trigger_Entitlement_Class();
        
        oNewAcc.Name = 'Test Account';
        insert oNewAcc;
               
        
        oNewEtl.Name = 'Etl Test';
        oNewEtl.AccountId = oNewAcc.Id; 
        oNewEtl.CS_Account_Name__c = oNewAcc.Name;	
        oNewEtl.Type = 'Phone Support';
        oNewEtl.StartDate = Date.today();
        oNewEtl.EndDate = Date.today();
        oNewEtl.CS_Entitlement_Process_Name__c = 'SLA Test';
        oNewEtl.CS_Business_Hour_Name__c = 'BH Test';
         
        insert oNewEtl;        
    }
}