/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_CaseAssetAccount_Class.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 28/01/ 2018     Debbie Zacarias	      Creation of the class used for control Lightning component for account assets
* 11/04/2019      Jose Luis Vargas		  Change methods for new flow cooler
*/

public class CS_CaseAssetAccount_Class 
{
	/**
	* method for find brands of assets for account cases
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
    * @param serialNumber Serial Number
	* @return List<String> that controls the picklist values
	*/    
    @AuraEnabled
    public static ONTAP__Account_Asset__c getAssetInfo(String idCase, string serialNumber)
    {
        ONTAP__Account_Asset__c acc = new ONTAP__Account_Asset__c();
        
        try
        {
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            System.debug('CaseAssetAccount::getAssetInfo: '+ caso.Id);
            acc = [SELECT ONTAP__Account__c, ONTAP__Brand__c, HONES_Asset_Model__c, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c
                   FROM ONTAP__Account_Asset__c 
                   WHERE ONTAP__Account__c =: caso.Accountid
                   AND ONTAP__Serial_Number__c =: serialNumber LIMIT 1];
            
            acc.ONTAP__Brand__c = (acc.ONTAP__Brand__c != NULL && acc.ONTAP__Brand__c != '' ? acc.ONTAP__Brand__c : Label.CS_Opcion_Sin_Informacion);
            acc.HONES_Asset_Model__c = (acc.HONES_Asset_Model__c != NULL && acc.HONES_Asset_Model__c != '' ? acc.HONES_Asset_Model__c : Label.CS_Opcion_Sin_Informacion);
            acc.ONTAP__Asset_Description__c = (acc.ONTAP__Asset_Description__c != NULL && acc.ONTAP__Asset_Description__c != '' ? acc.ONTAP__Asset_Description__c : Label.CS_Opcion_Sin_Informacion);
            acc.HONES_EquipmentNumber__c = (acc.HONES_EquipmentNumber__c != NULL && acc.HONES_EquipmentNumber__c != '' ? acc.HONES_EquipmentNumber__c : Label.CS_Opcion_Sin_Informacion);
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Stack trace: ' + ex.getStackTraceString());
        }
        
        return acc;
    }
    
    /**
	* method for find Serial numbers of assets for account cases
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @return List<String> that controls the picklist values
	*/
    @AuraEnabled
    public static List<String> getSerialNumber(String idCase)
    {
        List<String> pickListValuesList = new List<String>();
        List<ONTAP__Account_Asset__c> acc = new List<ONTAP__Account_Asset__c>();
        
        try
        {
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            acc = [SELECT ONTAP__Serial_Number__c FROM ONTAP__Account_Asset__c WHERE ONTAP__Account__c =: caso.AccountId];
            
            if(acc.size() > 0)
            {
                pickListValuesList.add('');
                for(ONTAP__Account_Asset__c item : acc)
                {
                    if(!pickListValuesList.contains(item.ONTAP__Serial_Number__c))
                        pickListValuesList.add(item.ONTAP__Serial_Number__c);
                }
            }
            else
                pickListValuesList.add(Label.CS_Opcion_Sin_Informacion);
                
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Stack trace: ' + ex.getStackTraceString());
        } 
        
        return pickListValuesList;
    }
            
    /*
	* method for find Info of assets for current case
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @return Case that get the info
	*/
    @AuraEnabled
    public static List<Object> SetAssetCaseInfo(String idCase)
    {
        List<Object> returnList = new List<Object>();
        Case caso;
        ONTAP__Account_Asset__c asset;
        
        try
        {
            caso = ([SELECT id, Status, AccountId, ISSM_BrandProduct__c, ISSM_CaseReason__c, Equipment_model__c, ISSM_SerialNumber__c, CS_Serial_number_of_the_equipment__c, ISSM_Count__c, ISSM_CountRetiro__c, CS_skuProduct__c FROM Case WHERE id =:idCase]);
            system.debug('Imprime caso' + caso);
            if(caso.ISSM_SerialNumber__c != null && caso.CS_skuProduct__c != null)
            {
                system.debug('ENTRO A EL  asset') ;
                 asset = ([SELECT id, ONTAP__Account__c,ONTAP__Brand__c,ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c
                           FROM ONTAP__Account_Asset__c 
                           WHERE ONTAP__Serial_Number__c =: caso.ISSM_SerialNumber__c 
                           //AND HONES_Asset_Model__c =: caso.Equipment_model__c 
                           AND ONTAP__Brand__c =: caso.ISSM_BrandProduct__c
                           AND ONTAP__Asset_Description__c =: caso.ISSM_CaseReason__c
                           AND ONTAP__Account__c =: caso.AccountId
                          LIMIT 1]);
                system.debug('Imprime asset' + asset);
                
                returnList.add(caso);
                returnList.add(asset);    
            }
            else 
            {
                returnList = null;
            }     
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo Stack trace: ' + ex.getStackTraceString());            
        }
                
        return returnList;        
    }
    
    /**
	* method for update current case with the information passed trough GetDescription method 
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case for update
	* @param serialNumber for update
    * @param quantity asset number to retirement
	* @return Boolean if record was updated
	*/
    @AuraEnabled
    public static Boolean UpdateCase(String idCase, string serialNumber, integer quantity, Boolean onlyUpdate)
    {
        Boolean success = true;
        Case oCaseUpdate = new Case();
        ONTAP__Case_Force__c oCaseForceUpdate = new ONTAP__Case_Force__c();
        
        try
        {            
            Case caso = ([SELECT Id, ISSM_CaseForceNumber__c, AccountId, CS_Id_Retiro_SAP__c FROM Case WHERE id =: idCase]);           
            ONTAP__Account_Asset__c acc = ([SELECT Id, ONTAP__Brand__c, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c
                                            FROM ONTAP__Account_Asset__c 
                                            WHERE ONTAP__Serial_Number__c=:SerialNumber 
                                            AND ONTAP__Account__c=:caso.AccountId
                                            LIMIT 1]);
            system.debug('CS_Id_Retiro_SAP__c' + caso.CS_Id_Retiro_SAP__c);
            if(caso.CS_Id_Retiro_SAP__c == null){
                oCaseUpdate.Id = idCase;
                oCaseUpdate.ISSM_BrandProduct__c = acc.ONTAP__Brand__c;
                oCaseUpdate.Equipment_model__c = acc.HONES_Asset_Model__c;
                oCaseUpdate.ISSM_SerialNumber__c = acc.ONTAP__Serial_Number__c;
                oCaseUpdate.CS_Serial_number_of_the_equipment__c = acc.HONES_EquipmentNumber__c;
                oCaseUpdate.ISSM_CaseReason__c = acc.ONTAP__Asset_Description__c;
                oCaseUpdate.CS_skuProduct__c = acc.V360_MaterialNumber__c;
                oCaseUpdate.ISSM_CountRetiro__c = quantity;
                
                oCaseForceUpdate.Id = caso.ISSM_CaseForceNumber__c;
                oCaseForceUpdate.CS_Account_Asset__c = acc.Id;
                oCaseForceUpdate.ONTAP__Quantity__c = quantity;
                
                
                ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
                Update oCaseForceUpdate;
                ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
                Update oCaseUpdate;
                
                /* Se envia el caso a SAP */
                //SM
                if(onlyUpdate != true) {
                    CS_Case_Coolers_Callout_Class.sendRequest(caso.id,'Retiro');
                }
            } 
        }
        catch(Exception ex)
        {
            success = false;
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Stack trace: ' + ex.getStackTraceString());
        } 
        
        return success;
    }
    
    /**
	* method for update current case with the information passed trough GetDescription method 
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case for update
	* @param SerialNumber for update
	* @param EquipmentNumber for update
	* @return Boolean if record was updated
	*/
    @AuraEnabled
    public static boolean UpdateCaseAsset(string idCase, string SerialNumber)
    {
        boolean success = true;
        Case oCaseUpdate = new Case();
        ONTAP__Case_Force__c oCaseForceUpdate = new ONTAP__Case_Force__c();
        
        try
        {            
            Case caso = ([SELECT Id, ISSM_CaseForceNumber__c, AccountId FROM Case WHERE id =:idCase]);
                        
            ONTAP__Account_Asset__c acc = ([SELECT Id, ONTAP__Brand__c, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c
                                            FROM ONTAP__Account_Asset__c 
                                            WHERE ONTAP__Serial_Number__c=:SerialNumber 
                                            AND ONTAP__Account__c=:caso.AccountId
                                            LIMIT 1]);
            
            oCaseUpdate.Id = idCase;
            oCaseUpdate.ISSM_BrandProduct__c = acc.ONTAP__Brand__c;
            oCaseUpdate.Equipment_model__c = acc.HONES_Asset_Model__c;
            oCaseUpdate.ISSM_SerialNumber__c = acc.ONTAP__Serial_Number__c;
            oCaseUpdate.CS_Serial_number_of_the_equipment__c = acc.HONES_EquipmentNumber__c;
            oCaseUpdate.ISSM_CaseReason__c = acc.ONTAP__Asset_Description__c;
            oCaseUpdate.CS_skuProduct__c = acc.V360_MaterialNumber__c;
            
            oCaseForceUpdate.Id = caso.ISSM_CaseForceNumber__c;
            oCaseForceUpdate.CS_Account_Asset__c = acc.Id;           
            
            ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
            Update oCaseForceUpdate;
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            Update oCaseUpdate;
        }
        catch(Exception ex)
        {
            success = false;
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASEASSET Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASEASSET Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASEASSET Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASEASSET Stack trace: ' + ex.getStackTraceString());
        } 
        return success;
    }
    
       /**
	* method for call service Aviso de Mantenimientos
	* @author: rjimenez@ts4.mx
	* @param idCase id of current case for update
	* @param idSerial
	*
	* @return CustomResponse with Messages
	*/
    @AuraEnabled
    @future(callout = true)
    public static void sendAvisoMantenimiento(string idCase){
        System.debug(idCase);
        CustomResponse cResponse= new CustomResponse();
        MaintenanceCase maintenanceCase = new MaintenanceCase();
        
        try{

            Case caso = [SELECT Id,CaseNumber,Description,RecordTypeId, RecordType.Name,
                                AccountId, Account.ONTAP__Street__c, Account.ONTAP__Neighborhood__c,Account.HONES_Department__c,
                                Account.ONTAP__Municipality__c, Account.ONTAP__ExternalKey__c, Account.HONES_AddressReference__c,ISSM_SerialNumber__c
                        FROM Case WHERE Id =:  idCase];
            system.debug(caso);
            ONTAP__Account_Asset__c asset = [SELECT Id, ONTAP__Brand__c, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, 
                                                     ONTAP__Serial_Number__c, V360_MaterialNumber__c,ONTAP__Account__r.ONTAP__SAP_Number__c,
                                                     V360_InventoryNumber__c
                                            FROM ONTAP__Account_Asset__c 
                                            WHERE ONTAP__Serial_Number__c=:caso.ISSM_SerialNumber__c 
                                            AND ONTAP__Account__c=:caso.AccountId
                                            LIMIT 1];
            system.debug(asset);
            string serialNumber = caso.ISSM_SerialNumber__c;
            maintenanceCase.Equnr = asset.HONES_EquipmentNumber__c;
            maintenanceCase.Matnr = asset.V360_MaterialNumber__c;
            //maintenanceCase.Sernr = '61281054';
            maintenanceCase.Sernr = asset.ONTAP__Serial_Number__c;
            maintenanceCase.Kunnr = asset.ONTAP__Account__r.ONTAP__SAP_Number__c;
            maintenanceCase.Nroinvent = asset.V360_InventoryNumber__c;
            maintenanceCase.calle = caso.Account.ONTAP__Street__c;
            maintenanceCase.Colonia = caso.Account.ONTAP__Neighborhood__c;
            maintenanceCase.Depto = caso.Account.HONES_Department__c;
            maintenanceCase.Municipio = caso.Account.ONTAP__Municipality__c;
            //maintenanceCase.Referencia = '';
            maintenanceCase.Referencia = caso.Account.HONES_AddressReference__c;
            String country = caso.Account.ONTAP__ExternalKey__c;
            country = country.left(2);
            maintenanceCase.Pais = country;
            maintenanceCase.Descripcion = caso.Description;
            maintenanceCase.Tiporegis = '78981';
            //maintenanceCase.Tiporegis = caso.RecordType.Name;
            //maintenanceCase.Idcasosf = '0123456789';
            maintenanceCase.Idcasosf = caso.Id;

            JSONGenerator generator = JSON.createGenerator(true);
            generator.writeObject(maintenanceCase);
            String jsonRequest = generator.getAsString().replaceAll('\\n','');  
            cResponse = sendJsonAvisoMantenimiento (jsonRequest, idCase, serialNumber,country);

        }catch(Exception e){
            cResponse.success = false;
            cResponse.status = 'ERROR';
            cResponse.message = e.getMessage() + e.getLineNumber();
        }
    }

    @AuraEnabled
    public static CustomResponse sendAvisoMantenimientoCS(string idCase, String SNumber){
        System.debug('sendAvisoMantenimientoCSidCase: '+idCase);
        CustomResponse cResponse= new CustomResponse();
        MaintenanceCase maintenanceCase = new MaintenanceCase();
        
        try{
            Case caso = [SELECT Id,CaseNumber,Description,RecordTypeId, RecordType.Name,
                                AccountId, Account.ONTAP__Street__c, Account.ONTAP__Neighborhood__c,Account.HONES_Department__c,
                                Account.ONTAP__Municipality__c, Account.ONTAP__ExternalKey__c, Account.HONES_AddressReference__c,ISSM_SerialNumber__c
                        FROM Case WHERE Id =:  idCase];

            
            ONTAP__Account_Asset__c asset = [SELECT Id, ONTAP__Brand__c, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, 
                                                     ONTAP__Serial_Number__c, V360_MaterialNumber__c,ONTAP__Account__r.ONTAP__SAP_Number__c,
                                                     V360_InventoryNumber__c
                                            FROM ONTAP__Account_Asset__c 
                                            WHERE ONTAP__Serial_Number__c=:SNumber
                                            AND ONTAP__Account__c=:caso.AccountId
                                            LIMIT 1];

            system.debug('QueryAsset: '+asset);
            string serialNumber = SNumber;
            maintenanceCase.Equnr = asset.HONES_EquipmentNumber__c;
            maintenanceCase.Matnr = asset.V360_MaterialNumber__c;
            //maintenanceCase.Sernr = '61281054';
            maintenanceCase.Sernr = asset.ONTAP__Serial_Number__c;
            maintenanceCase.Kunnr = asset.ONTAP__Account__r.ONTAP__SAP_Number__c;
            maintenanceCase.Nroinvent = asset.V360_InventoryNumber__c;
            maintenanceCase.calle = caso.Account.ONTAP__Street__c;
            maintenanceCase.Colonia = caso.Account.ONTAP__Neighborhood__c;
            maintenanceCase.Depto = caso.Account.HONES_Department__c;
            maintenanceCase.Municipio = caso.Account.ONTAP__Municipality__c;
            //maintenanceCase.Referencia = '';
            maintenanceCase.Referencia = caso.Account.HONES_AddressReference__c;
            String country = caso.Account.ONTAP__ExternalKey__c;
            country = country.left(2);
            maintenanceCase.Pais = country;
            maintenanceCase.Descripcion = caso.Description;
            maintenanceCase.Tiporegis = '78981';
            //maintenanceCase.Tiporegis = caso.RecordType.Name;
            //maintenanceCase.Idcasosf = '0123456789';
            maintenanceCase.Idcasosf = caso.Id;

            JSONGenerator generator = JSON.createGenerator(true);
            generator.writeObject(maintenanceCase);
            String jsonRequest = generator.getAsString().replaceAll('\\n','');  
            cResponse = sendJsonAvisoMantenimiento (jsonRequest, idCase, serialNumber,country);

        }catch(Exception e){
            cResponse.success = false;
            cResponse.status = 'ERROR';
            cResponse.message = e.getMessage() + e.getLineNumber();
        }
        return cResponse;

    }

    public static CustomResponse sendJsonAvisoMantenimiento(String jsonRequest, String idCase, String serialNumber, String country){
       
        List<End_Point__mdt> endpoint = [SELECT DeveloperName,MasterLabel,Token__c,URL__c 
                                        FROM End_Point__mdt 
                                        WHERE MasterLabel = 'Notify_Maintenance_Case' AND Country__c =: country];
        CustomResponse cResponse= new CustomResponse();


        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint(endpoint[0].URL__c);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json;charset=utf-8');
        httpReq.setHeader('bearer', endpoint[0].Token__c);
        httpReq.setBody(jsonRequest);
        System.debug('sendJsonAvisoMantenimiento::jsonRequest::' + jsonRequest);
        HTTPResponse authresp = new HTTPResponse();
         try{
            authresp = h.send(httpReq);
            if(authresp.getStatusCode() == 200 && authresp.getBody() != null){
                String responceItem = authresp.getBody();
                String responseChanItem = responceItem.replace('Id' , 'Iditem');
                String responseNumber = responseChanItem.replace('Number', 'NumberItem');
                String responseItem = responseNumber.replace('System', 'SystemItem');
                ZPmFmCreateNotifEquipmentResponse listToSave = ZPmFmCreateNotifEquipmentResponse.parse(authresp.getBody());
                string respuesta = String.valueOf(listToSave);
                List<String> resultEquipment= respuesta.split('Equipment=');
                List<String> resultadoEquipment= resultEquipment[1].split(',');
                string Equipment = resultadoEquipment[0];
                if(resultadoEquipment[0] != 'null'){
                    List<case> lstCaseUpdate = new List<case>();
                    List<String> resultNotifNo= resultadoEquipment[1].split('NotifNo=');
                    List<String> resultadoNotifNo= resultNotifNo[1].split(']]');
                    string NotifNo = resultadoNotifNo[0];
                    case upcase = new case();
                    upcase.Id = idCase;
                    upcase.CS_Id_Retiro_SAP__c = NotifNo;
                    upcase.CS_Coolers_Logs__c  = Equipment;
                    upcase.status = 'Open';
                    upcase.CS_Send_To_Mule__c = true;
                    lstCaseUpdate.add(upcase);
                    ISSM_TriggerManager_cls.inactivate('Case');
                    try{
                        Database.update(lstCaseUpdate);
                    }catch(exception e){
                        System.debug('ERROR : '+e);
                    }

                }else{
                    ZPmFmCreateNotifEquipmentResponse2 listToSave2 = ZPmFmCreateNotifEquipmentResponse2.parse(authresp.getBody());
                    string message = String.valueOf(listToSave2);
                    List<case> lstCaseUpdate = new List<case>();
                    List<String> resultmessage= message.split('message=');
                    List<String> resultadomessage= resultmessage[1].split(',');
                    string messages = resultadomessage[0];
                    case upcase = new case();
                    upcase.Id = idCase;
                    upcase.CS_Id_Retiro_SAP__c = messages;
                    upcase.CS_Coolers_Logs__c  = messages ;
                    lstCaseUpdate.add(upcase);
                    ISSM_TriggerManager_cls.inactivate('Case');
                    try{
                        Database.update(lstCaseUpdate);
                    }catch(exception e){
                        System.debug('ERROR : '+e);
                    }
                }
                
                cResponse.message = 'CASE SEND SUCCESSFILLY: ' + listToSave;
                //RJP:dependiendo del mensaje es que se deberia mandar a actulizar o no el caso
                UpdateCaseAsset(idCase, serialNumber);
            }else{
                cResponse.success = false;
                cResponse.status = 'ERROR';
                cResponse.message = 'CONSULT ADMIN 1:' + authresp.getBody() + ':: StatusCode:: ' + authresp.getStatusCode();
            }

         }catch(Exception e){
            cResponse.success = false;
            cResponse.status = 'ERROR';
            cResponse.message = 'CONSULT ADMIN 2:' + e.getMessage();
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
        }finally{
    
        }

        return cResponse;
    }

     public class CustomResponse{
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String message {get;set;}
        @AuraEnabled public boolean success {get;set;}
        public CustomResponse(){
            this.success = true;
        }
    }

    public class MaintenanceCase{
        String Equnr {get; set;}
        String Matnr {get;set;}  
        String Sernr {get;set;}
        String Kunnr {get;set;}
        String Nroinvent {get;set;}
        String Calle {get;set;}
        String Colonia {get; set;}
        String Depto {get;set;}
        String Municipio {get;set;}
        String Pais {get;set;}
        String Referencia{get;set;}
        String Descripcion {get;set;}  
        String Tiporegis {get;set;}
        String Idcasosf {get;set;}

        public MaintenanceCase(){

        }

        public  MaintenanceCase parse(String json){
		    return (MaintenanceCase) System.JSON.deserialize(json, MaintenanceCase.class);
	    }
    }

    //este Response quizás cambie
  /*  public class ZPmFmCreateNotifEquipmentResponse{
        String EtAvisos {get;set;}
        public EtReturn EtReturn {get;set;}
        public ZPmFmCreateNotifEquipmentResponse parse(String json){
            return (ZPmFmCreateNotifEquipmentResponse) System.JSON.deserialize(json, ZPmFmCreateNotifEquipmentResponse.class);
        }
    }
    public class EtReturn {
        public List<Item> items {get;set;}
    }
    public class Item {
        public item(){}
        public String  type {get;set;}
        public String  IdItem {get;set;}
        public Integer NumberItem {get;set;}
        public String  Message {get;set;}
        public String  LogNo {get;set;}
        public Integer LogMsgNo {get;set;}
        public String  MessageV1 {get;set;}
        public String  MessageV2 {get;set;}
        public String  MessageV3 {get;set;}
        public String  MessageV4 {get;set;}
        public String  Parameter {get;set;}
        public String  Row {get;set;}
        public String  Field {get;set;}
        public String  SystemItem {get;set;}
    }
*/
}