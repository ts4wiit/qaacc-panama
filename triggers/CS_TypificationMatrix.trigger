trigger CS_TypificationMatrix on ISSM_TypificationMatrix__c (after delete, after insert, after update, before delete, before insert, before update) {
    New CS_CaseTypification_Trigger_Class().run();
}