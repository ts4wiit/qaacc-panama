/* ----------------------------------------------------------------------------
* AB InBev :: Visit Control
* ----------------------------------------------------------------------------
* Clase: VisitControl_Call.apxt
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                  				 Description
* 18/12/2018     g.martinez.cabral@accenture.com        Creation of Trigger which executes after delete, after insert, after update, 
														before delete, before insert, before update on the object ONCALL__Call__c
*/


/**
* Constructor which call the VisitControl_CallTriggerHandler in the following cases: after delete, after insert, after update, before delete, before insert, before update
* @author: g.martinez.cabral@accenture.com
* @param void
* @return Void
*/
trigger VisitControl_Call on ONCALL__Call__c  (after delete, after insert, after update, before delete, before insert, before update) { 
	New VisitControl_CallTriggerHandler().run();
}