/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASE_TRIGGER.apxt
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 03/12/2018      José Luis Vargas        Creacion del trigger que se ejecutara despues de una insercion
 *                                         o actualización en le objeto Case
 */

/**
* Constructor which call the CS_TRIGGER_CASE_CLASS TriggerHandler in the following cases: after insert, after update, before insert, before update
* @author: José Luis Vargas
* @param void
* @return Void
*/
trigger CS_Case_Trigger on Case (after insert, after update, before insert, before update) 
{
    new CS_Trigger_Case_Class().run();
}