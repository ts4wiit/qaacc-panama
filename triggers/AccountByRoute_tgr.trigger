/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger to control RouteXAccount__c events

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       14-06-2017        Andrés Garrido               Creation Class
    1.1       01-09-2017        Daniel Peñaloza              Add route manager to Account Team
    1.2       19-04-2018        Rodrigo Resendiz             When route is for BDR: set BDR as the Account owner of Accounts in the route
    1.3       30-04-2018        Rodrigo Resendiz             When route is for Telesales add to account team
****************************************************************************************************/

trigger AccountByRoute_tgr on AccountByRoute__c (after delete, after insert, after update, before delete, before insert, before update) {
    if(trigger.isBefore) {
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','BeforeInsert')){
            // Validar que solo se aplique para Cuentas con Ruta de Preventa y Autoventa
            AccountByRoute__c[] lstAccountsToProcess = AccountByRoute_cls.filterAccounts(Trigger.new);
            AccountByRouteOperations_cls.validateServiceModelConfig(trigger.new);

            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','BeforeInsert'); 
        }
        else if(trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','BeforeUpdate')){
            // Validar que solo se aplique para Cuentas con Ruta de Preventa y Autoventa
            AccountByRoute__c[] lstAccountsToProcess = AccountByRoute_cls.filterAccounts(Trigger.new);
            AccountByRouteOperations_cls.validateServiceModelConfigUpdate(trigger.old, trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','BeforeUpdate');
        }
        else if(trigger.isDelete){
            // Remove Supervisor and Sales Agent from Account team
            ////>>>>>>AccountByRoute_cls.removeUsersFromAccountTeam(Trigger.old, null);
             ////>>>>>>AccountByRoute_cls.removeTelesalesFromAccountTeam(Trigger.old, null);//1.3
            //[19-04-2018 -START-]
            AccountByRoute__c[] lstAccountsToProcessBDR = AccountByRoute_cls.filterAccountsBDR(Trigger.old);
            // (1.2) make BDR the Owner of the Account RR
             ////>>>>>>if(!lstAccountsToProcessBDR.isEmpty())
             ////>>>>>>AccountByRoute_cls.removeBDRAsAccountOwner(lstAccountsToProcessBDR);

            //[19-04-2018 -END-]
        }
    }
    else if(trigger.isAfter){
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','AfterInsert')){
            AccountByRouteOperations_cls.afterInsertAccountByRoute(trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','AfterInsert');
            // Add Supervisor and Sales Agent to Account team
             ////>>>>>>AccountByRoute_cls.addUsersToAccountTeam(Trigger.new);
             ////>>>>>>AccountByRoute_cls.addTelesaleToAccountTeam(Trigger.new); //1.3
            //[19-04-2018 -START-]
            // (1.2) Filter accounts that belong to the correct business
            AccountByRoute__c[] lstAccountsToProcessBDR = AccountByRoute_cls.filterAccountsBDR(Trigger.new);
            // (1.2) Make BDR the Owner of the Account RR
             ////>>>>>>AccountByRoute_cls.setBDRAsAccountOwner(lstAccountsToProcessBDR, null);
            //[19-04-2018 -END-]
        }
        else if(trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','AfterUpdate')){
            AccountByRouteOperations_cls.afterUpdateAccountByRoute(trigger.old, trigger.new);
            //[19-04-2018 -START-] 
            //Filter accounts that belong to the correct business
            AccountByRoute__c[] lstAccountsToProcessBDR = AccountByRoute_cls.filterAccountsBDR(Trigger.new);
            // (1.2) Remove BDR of being owner when the account field is changed
            AccountByRoute__c[] lstAccountsToProcessWithChanges = AccountByRoute_cls.filterAccountsByChange(lstAccountsToProcessBDR, trigger.oldMap, new String[]{'Account__c'});
            if(!lstAccountsToProcessWithChanges.isEmpty())  ////>>>>>>AccountByRoute_cls.removeBDRAsAccountOwner(lstAccountsToProcessWithChanges);
            // (1.2) Make BDR the Owner of the Account RR
            ////>>>>>> AccountByRoute_cls.setBDRAsAccountOwner(lstAccountsToProcessBDR, null);
            //[19-04-2018 -END-]
            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','AfterUpdate');
        }
        else if(trigger.isDelete && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','AfterDelete')){
            AccountByRouteOperations_cls.afterDeleteAccountByRoute(trigger.old);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','AfterDelete');
        }
    }
}